#include "ioutilities.hpp"

#include <fstream>
#include <string>
#include "petscksp.h"
#include "array_types.hpp"

namespace IoUtilities
{

  void writePetscVectorToBin(Vec &vector,
                             std::string filename,
                             uint32_t nEntries)
  {
    PetscReal *array;
    VecGetArray(vector, &array);

    std::fstream fout;
    fout.open(filename.c_str(), std::ios::out | std::ios::binary);

    // Write number of entries as header
    fout.write((char *) &nEntries, sizeof(uint32_t));

    if (sizeof(PetscReal) == sizeof(double))
    {
      fout.write((char *) array, nEntries * sizeof(double));
      fout.close();
    }
    else // Need to convert to doubles before writing
    {
      Array1d<double> arrayOut(nEntries);
      for (PetscInt i = 0; i < nEntries; i++)
      {
        arrayOut(i) = *(array + i);
      }
      fout.write((char *) arrayOut.front(), nEntries * sizeof(double));
    }
    fout.close();
  }

}
