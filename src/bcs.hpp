#ifndef BCS_HPP
#define BCS_HPP

#ifndef DIM
#define DIM 3
#endif

#include <vector>

#include "petscksp.h"

#include "mesh.hpp"
#include "array_types.hpp"
#include "gauss_legendre.hpp"


class DirichletBC
{
public:
  DirichletBC(Mesh &mesh, int nSpecies=1) :
    m_mesh(&mesh), m_nSpecies(nSpecies), m_nCornerNodes(0), m_nTotalNodes(0)
  {
    m_nodeValues.resize(m_nSpecies);
    m_speciesGetsBC.resize(m_nSpecies);
    for (int i = 0; i < m_nSpecies; i++)
    {
      m_speciesGetsBC[i] = false;
    }
  }
  void loadNodes(std::string nodeListFilename);
  void setValues(PetscReal uniformValue, int species=0);
  void make2ndOrder();


  /** Size: m_nTotalNodes <br>
    * m_nodeIds(i) contains the local index of the ith Dirichlet node owned by
    * this process (includes ghost nodes). The first m_nCornerNodes entries are
    * order 1 nodes. */
  Array1d<PetscInt> m_nodeIds;

  /** Size: m_nTotalNodes <br>
    * m_nodeIdsGlobal(i) contains the global index of the ith Dirichlet node
    * owned by this process (includes ghost nodes). The first m_nCornerNodes
    * entries are order 1 nodes. This array isn't totally necessary and can be
    * eliminated if memory becomes an issue, but makes the implementation of
    * applyBCs easier. */
  Array1d<PetscInt> m_nodeIdsGlobal;

  /** Size: m_nTotalNodes <br>
    * m_nodeValues(i) contains the values for the ith Dirichlet node owned by
    * this process (includes ghost nodes). The first m_nCornerNodes entries are
    * order 1 nodes. */
  std::vector<Array1d<PetscReal> > m_nodeValues;

  /** Size: (m_nTotalNodes - m_nCornerNodes) x 2 <br>
    * Contains the local indices of the two neighbors for each order 2
    * Dirichlet node. */
  Array2d<PetscInt> m_order2Neighbors;

  /** Size: m_nSpecies <br>
    * m_speciesGetsBC[i] determines whether BC is applied to species i. */
  std::vector<bool> m_speciesGetsBC;

  PetscInt m_nCornerNodes; ///< Number of order 1 Dirichlet nodes (local).
  PetscInt m_nTotalNodes; ///< Total number of Dirichlet nodes (local).
  int m_nSpecies;
  Mesh *m_mesh; ///< Mesh associated with this BC.
};

class OutletBC : public DirichletBC
{
public:
  OutletBC(Mesh &mesh, int nSpecies=1);

  /** Sets all normals in this BC to a uniform value (i.e. assumes face is
    * flat). */
  void setNormals(PetscReal normal[DIM]);

  /** Size: m_nTotalNodes x DIM <br>
    * m_normals(i, j) contains the jth component of the unit normal vector at
    * node i. */
  Array2d<PetscReal> m_normals;
};

class NeumannBC
{
public:
  // NeumannBC(const NeumannBC & other);
  NeumannBC(Mesh &mesh, int basisOrder, int integrationOrder, int nSpecies=1);
  void loadNodes(std::string nodeListFilename);
  void setValues(PetscReal uniformValue, int species=0);
  void calculateJacobians();

  /** Size: m_nTotalNodes <br>
    * m_nodeIds(i) contains the local mesh node index for node i. */
  Array1d<PetscInt> m_nodeIds;

  /** Size: m_nFaces x m_enon <br>
    * m_connectivity(i,j) contains the index (in m_nodeIds, not the mesh
    * ordering) for node j of face i. Corner (order 1) nodes are listed first
    * for each element followed by order 2 nodes, and ordering follows the same
    * format as m_connectivity in the Mesh class. */
  Array2d<PetscInt> m_connectivity;

  /** Size: m_nFaces x m_enon <br>
    * m_connectivityGlobal(i,j) contains the global mesh index for node j of
    * face i. Same ordering as m_connectivity. */
  Array2d<PetscInt> m_connectivityGlobal;

  /** Size: m_nFaces <br>
    * m_nodeValues[i](j) contains the value of the Neumann BC at node j (in
    * m_nodeIds) for species[i] */
  std::vector<Array1d<PetscReal> > m_nodeValues;

  /** Size: m_nFaces <br>
    * m_jacobians(i) contains the Jacobian determinant for face i. */
  Array1d<PetscReal> m_jacobians;

  /** Size: m_intScheme.m_nIntegrationPoints x m_enon <br>
    * m_phi(i, j) contains the value of phi_j at integration point i. */
  Array2d<PetscReal> m_phi;

  /** Size: m_nSpecies <br>
    * m_speciesGetsBC[i] determines whether BC is applied to species i. */
  std::vector<bool> m_speciesGetsBC;

  PetscInt m_nFaces; ///> Number of faces in the boundary condition.
  PetscInt m_nCornerNodes; ///> Number of order 1 nodes.
  PetscInt m_nTotalNodes; ///> Number of order 1 nodes.
  int m_enon; ///> Number of nodes on each face.
  int m_order;
  IntegrationScheme m_intScheme; ///> Gauss-Legendre integration scheme.
  Mesh * m_mesh; ///> Mesh associated with this BC.
  int m_nSpecies;
};

#endif
