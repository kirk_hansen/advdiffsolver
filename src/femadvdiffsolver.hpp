#ifndef FEMADVDIFFSOLVER_HPP
#define FEMADVDIFFSOLVER_HPP

#include "petscksp.h"

#include <string>
#include <vector>
#include "mesh.hpp"
#include "vectorND.hpp"
#include "bcs.hpp"
#include "basis.hpp"

class FemAdvDiffSolver
{
public:

  FemAdvDiffSolver(Mesh &mesh, std::vector<Vec> &solution);

  FemAdvDiffSolver(Mesh &mesh,
                   std::vector<Vec> &solution,
                   int basisOrder,
                   int velocityBasisOrder,
                   bool stabilized=true,
                   bool store_dPhidX=false,
                   bool verbose=false);

  ~FemAdvDiffSolver();

  void initialize(int basisOrder,
                  int velocityBasisOrder,
                  bool stabilized=true,
                  bool store_dPhidX=false,
                  bool verbose=false);

  void buildDiffusiveStiffnessMatrix(Mat &Kdiff,
                                     PetscReal diffusivity=1.,
                                     bool zeroFirst=true);

  void buildAdvectiveStiffnessMatrix(Mat &Kadv, bool zeroFirst=true);

  void buildMassMatrix(Mat &M, bool zeroFirst);

  void applyNeumannBCs(Vec &rhs, bool zeroFirst, int species=0);

  void applyDirichletBCs(int species=0);

  void applyOutletBCs(int species=0);

  void loadVelocityFromBin(Array2d<PetscReal> &velocity, std::string filename);

  void setUniformVelocity(Array2d<PetscReal> &velocity, PetscReal value[DIM]);

  Array2d<PetscReal> m_currentVelocity;

  Array2d<PetscReal> m_phi;

  Array1d<Array3d<PetscReal> > m_dPhidX;

  Array1d<Array1d<PetscReal> > m_laplacePhi;

  Array2d<PetscReal> m_velocityPhi;

  std::vector<DirichletBC> m_dirichletBCs;
  std::vector<OutletBC> m_outletBCs;
  std::vector<NeumannBC> m_neumannBCs;

  Mesh &m_mesh;
  Basis m_basis; ///< The FEM basis.
  Basis m_velocityBasis; ///< Basis for the velocity field.
  IntegrationScheme m_integrationScheme; ///< Gauss-Legendre scheme.
  std::vector<Vec> &m_solution; ///< The solution vector.
  Vec m_rhs; ///< The RHS vector.
  Mat m_LHS; ///< Overall LHS matrix.
  KSP m_ksp; ///< Petsc solver.
  PC m_pc; ///< Preconditioner.
  PetscInt m_localDOF; ///< DOFs owned by this process.
  PetscInt m_globalDOF; ///< Total DOFs across all processes.
  PetscInt m_enon; ///< Number of nodes per element.
  bool m_store_dPhidX; ///< Whether to precompute dPhidX and laplacePhi.
  bool m_stabilized;
  PetscMPIInt m_MPIrank;
  PetscMPIInt m_MPIsize;
  PetscReal m_diffusivity;
  bool m_verbose;
  int m_nSpecies;
  std::vector<bool> m_writeSpeciesToFile;
  std::vector<std::string> m_speciesNames;
};


class FemAdvDiffSolverSteady : public FemAdvDiffSolver
{
public:
  FemAdvDiffSolverSteady(Mesh &mesh, std::vector<Vec> &solution);

  FemAdvDiffSolverSteady(Mesh &mesh,
                         std::vector<Vec> &solution,
                         int basisOrder,
                         int velocityBasisOrder,
                         bool stabilized=true,
                         bool store_dPhidX=false,
                         bool verbose=false);

  void buildLHS();

  void buildRhs();

  void solve();
};


class FemAdvDiffSolverUnsteady : public FemAdvDiffSolver
{
public:
  FemAdvDiffSolverUnsteady(Mesh &mesh, std::vector<Vec> &solution);

  FemAdvDiffSolverUnsteady(Mesh &mesh,
                           std::vector<Vec> &solution,
                           int basisOrder,
                           int velocityBasisOrder,
                           bool stabilized=true,
                           bool store_dPhidX=false,
                           bool verbose=false);

  void updateVelocity(PetscReal time);

  PetscReal m_dt;

  Array2d<PetscReal> m_leftVel;
  Array2d<PetscReal> m_rightVel;

  std::string m_velFilenameRoot;
  PetscInt m_velFileStart;
  PetscInt m_nVelFiles;
  PetscInt m_velFileInterval;
  PetscReal m_velFileDt;
  PetscInt m_leftVelIdx;
  PetscInt m_rightVelIdx;
};


class FemAdvDiffSolverGenAlpha : public FemAdvDiffSolverUnsteady
{
public:
  /** Initialize the solver with a mesh and a solution only. Note that if this
    * constructor is used the user is responsible for calling initialize before
    * using the solver. */
  FemAdvDiffSolverGenAlpha(Mesh &mesh, std::vector<Vec> &solution);

  FemAdvDiffSolverGenAlpha(Mesh &mesh,
                           std::vector<Vec> &solution,
                           int basisOrder,
                           int velocityBasisOrder,
                           bool stabilized=true,
                           bool store_dPhidX=false,
                           bool verbose=false);

  ~FemAdvDiffSolverGenAlpha();

  /** Initialize the solver and prepare for use. */
  void initialize(int basisOrder,
                  int velocityBasisOrder,
                  bool stabilized=true,
                  bool store_dPhidX=false,
                  bool verbose=false);

  /** Solves adv-diff equation for one timestep for one species. User is
    * responsible for calling updateVelocity and buildLHS before the individual
    * species are timestepped. */
  PetscInt takeTimeStep(int species);

  void applyIC(int species);

  void buildLHS();

  void buildRHS(int species=0);

  void applyBCs(int species=0);

  PetscInt solve(int species=0);

  void buildMatrices(Mat &M_total, Mat &K_total);

  Mat m_M;
  Mat m_Kdiff;
  Mat m_M_total;
  Mat m_K_total;
  std::vector<Vec> m_cdot;
  std::vector<Vec> m_cprev;

  PetscReal m_rhoInfinity;
};


#endif
