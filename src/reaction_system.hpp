#ifndef REACTION_SYSTEM_HPP
#define REACTION_SYSTEM_HPP

#include <vector>
#include <string>
#include "petscksp.h"

class ReactionTerm
{
public:
  /** Set reaction type to invalid number by default. */
  ReactionTerm() : m_reactionType(-1) {}

  /** Reaction types:
        0 - 0th order
        1 - 1st order
        2 - 2nd order
        3 - Michaelis-Menten: R = k1 * c1 * c2 / (k2 + c2), where k1 and k2
            are m_rateConstant[0] and m_rateConstant[1], and c1 and c2 are
            the entries of m_speciesIdx
        4 - 3 species Michaelis-Menten: R = k1 * c1 * c2 * c3 / (k2 + c3)
  */
  int m_reactionType;

  /** m_speciesIdx[i] gives the species index of the ith species in the
   *  reaction.
   */
  int m_speciesIdx[4];

  /** m_rateConstant[i] gives the ith rate constant. */
  PetscReal m_rateConstant[2];
};


class ReactionEquation
{
public:
  /** Computes reaction rate based on species concentrations in c, and places
   *  it in rate. Assumes the entries of c are compatible with rate.
   */
  void computeRate(const std::vector<Vec> &c, Vec &rate);

  /** m_reactions[i] contains the ith reaction term. */
  std::vector<ReactionTerm> m_reactions;
};

#endif
