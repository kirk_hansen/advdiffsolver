#include <string>
#include <iostream>
#include <fstream>
#include <cstring>
#include <stdlib.h>
#include <cmath>

#include "petscsys.h"

#include "vectorND.hpp"
#include "array_types.hpp"

#ifndef DIM
#define DIM 3
#endif

Vector& Vector::operator=(const Vector &rhs)
{
  for (int i = 0; i < DIM; i++)
  {
    m_coords[i] = rhs[i];
  }
  return *this;
}



const Vector Vector::operator-() const
{
  Vector result;
  for (int i = 0; i < DIM; i++)
  {
    result[i] = -m_coords[i];
  }
  return result;
}

Vector & Vector::operator+=(const Vector &rhs)
{
  for (int i = 0; i < DIM; i++)
  {
    m_coords[i] += rhs[i];
  }
  return *this;
}

Vector & Vector::operator-=(const Vector &rhs)
{
  return (*this) += (-rhs);
}

const Vector Vector::operator+(const Vector &rhs) const
{
  Vector result = *this;
  return result += rhs;
}

const Vector Vector::operator-(const Vector &rhs) const
{
  Vector result = *this;
  return result += (-rhs);
}

const Vector Vector::operator*(double scalar) const
{
  Vector result = *this;
  for (int i = 0; i < DIM; i++)
  {
    result[i] *= scalar;
  }
  return result;
}

const PetscReal Vector::dot(const Vector &rhs) const
{
  PetscReal result = 0.;
  for (int i = 0; i < DIM; i++)
  {
    result += m_coords[i] * rhs[i];
  }
  return result;
}

const Vector Vector::cross(const Vector &rhs) const
{
  if (DIM == 3)
  {
    Vector result;
    result[0] = m_coords[1] * rhs[2] - m_coords[2] * rhs[1];
    result[1] = m_coords[2] * rhs[0] - m_coords[0] * rhs[2];
    result[2] = m_coords[0] * rhs[1] - m_coords[1] * rhs[0];

    return result;
  }
  else
  {
    std::cout << "Cross product invalid in " << DIM << "D." << std::endl;
    exit(0);
  }
}

const PetscReal Vector::magnitude() const
{
  return std::sqrt(this->dot(*this));
}
