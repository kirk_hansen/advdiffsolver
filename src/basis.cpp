#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>
#include <string>

#include "petscksp.h"

#include "mesh.hpp"
#include "vectorND.hpp"
#include "FEMSystem.hpp"
#include "array_types.hpp"

#ifndef DIM
#define DIM 3
#endif


Basis::Basis(const Mesh &mesh, int dim)
  : m_mesh(mesh), m_dim(dim), m_order(-1)
{
}

Basis::Basis(const Mesh &mesh, int basisOrder, int dim) :
  m_mesh(mesh), m_dim(dim)
{
  setOrder(basisOrder);
}


void Basis::setOrder(int basisOrder)
{
  m_order = basisOrder;
  if (m_dim == 1)
  {
    m_nLocalCoordinates = 1;
  }
  else if (m_dim > 1 && m_dim < 4)
  {
    m_nLocalCoordinates = m_mesh.m_order1Enon;
  }
  else
  {
    PetscMPIInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    if (rank == 0)
    {
      std::cout << "dim must be either 1, 2, or 3. "
                << m_dim << " is invalid." << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }

  if (basisOrder == 1)
  {
    m_nBasisFunctions = m_mesh.m_order1Enon;
  }
  else if (basisOrder == 2)
  {
    if (m_dim == 1)
    {
      m_nBasisFunctions = 3;
    }
    else if (m_dim == 2)
    {
      m_nBasisFunctions = 6;
    }
    else if (m_dim == 3)
    {
      m_nBasisFunctions = 10;
    }
  }
  else
  {
    PetscMPIInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    if (rank == 0)
    {
      std::cout << "Order " << basisOrder << " not implemented." << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
}


void Basis::calc_dXidX(PetscInt elementId, Array2d<PetscReal> & dXidX)
{
  if (m_order < 1)
  {
    PetscMPIInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    if (rank == 0)
    {
      std::cout << "Must set basis order before using." << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  std::vector<Vector> x(m_mesh.m_order1Enon);

  if (dXidX.m_dim1 != m_nLocalCoordinates || dXidX.m_dim2 != m_dim)
  {
    dXidX.resize(m_nLocalCoordinates, m_dim);
  }

  for (int i = 0; i < m_mesh.m_order1Enon; i++)
  {
    PetscInt globalNodeId = m_mesh.m_connectivity(elementId,i);
    x[i] = m_mesh.m_coordinates(globalNodeId);
  }

  if (m_dim == 1)
  {
    Vector & p1 = x[0];
    Vector & p2 = x[1];

    dXidX(0,0) = 2. / (p2[0] - p1[0]);
  }
  else if (m_dim == 2)
  {
    Vector & p1 = x[0];
    Vector & p2 = x[1];
    Vector & p3 = x[2];

    PetscReal area = getArea(p1, p2, p3);
    PetscReal mult = .5 / area;

    dXidX(0,0) = mult * (p2[1] - p3[1]);
    dXidX(0,1) = mult * (p3[0] - p2[0]);
    dXidX(1,0) = mult * (p3[1] - p1[1]);
    dXidX(1,1) = mult * (p1[0] - p3[0]);
    dXidX(2,0) = mult * (p1[1] - p2[1]);
    dXidX(2,1) = mult * (p2[0] - p1[0]);
  }
  else if (m_dim == 3)
  {
    Vector & p1 = x[0];
    Vector & p2 = x[1];
    Vector & p3 = x[2];
    Vector & p4 = x[3];

    PetscReal volume = getVolume(p1, p2, p3, p4);
    PetscReal mult = 1. / volume / 6.;

    dXidX(0,0) = mult * ((p4[1] - p2[1]) * (p3[2] - p2[2])
                           - (p3[1] - p2[1]) * (p4[2] - p2[2]));

    dXidX(0,1) = mult * ((p3[0] - p2[0]) * (p4[2] - p2[2])
                           - (p4[0] - p2[0]) * (p3[2] - p2[2]));

    dXidX(0,2) = mult * ((p4[0] - p2[0]) * (p3[1] - p2[1])
                           - (p3[0] - p2[0]) * (p4[1] - p2[1]));

    dXidX(1,0) = mult * ((p3[1] - p1[1]) * (p4[2] - p3[2])
                           - (p3[1] - p4[1]) * (p1[2] - p3[2]));

    dXidX(1,1) = mult * ((p4[0] - p3[0]) * (p3[2] - p1[2])
                           - (p1[0] - p3[0]) * (p3[2] - p4[2]));

    dXidX(1,2) = mult * ((p3[0] - p1[0]) * (p4[1] - p3[1])
                           - (p3[0] - p4[0]) * (p1[1] - p3[1]));

    dXidX(2,0) = mult * ((p2[1] - p4[1]) * (p1[2] - p4[2])
                           - (p1[1] - p4[1]) * (p2[2] - p4[2]));

    dXidX(2,1) = mult * ((p1[0] - p4[0]) * (p2[2] - p4[2])
                           - (p2[0] - p4[0]) * (p1[2] - p4[2]));

    dXidX(2,2) = mult * ((p2[0] - p4[0]) * (p1[1] - p4[1])
                           - (p1[0] - p4[0]) * (p2[1] - p4[1]));

    dXidX(3,0) = mult * ((p1[1] - p3[1]) * (p2[2] - p1[2])
                           - (p1[1] - p2[1]) * (p3[2] - p1[2]));

    dXidX(3,1) = mult * ((p2[0] - p1[0]) * (p1[2] - p3[2])
                           - (p3[0] - p1[0]) * (p1[2] - p2[2]));

    dXidX(3,2) = mult * ((p1[0] - p3[0]) * (p2[1] - p1[1])
                           - (p1[0] - p2[0]) * (p3[1] - p1[1]));
  }
}


void Basis::print_dXidX(PetscInt elementId)
{
  if (m_order < 1)
  {
    PetscMPIInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    if (rank == 0)
    {
      std::cout << "Must set basis order before using." << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  Array2d<PetscReal> dXidX(m_nLocalCoordinates, m_dim);
  calc_dXidX(elementId, dXidX);
  std::cout << std::setiosflags(std::ios::fixed)
            << std::setprecision(3)
            << std::left;
  for (int i = 0; i < m_nLocalCoordinates; i++)
  {
    for (int j = 0; j < m_dim; j++)
    {
      std::cout << std::setw(8) << dXidX(i,j) << " ";
    }
    std::cout << std::endl;
  }
}


void Basis::calc_phi(const IntegrationScheme & intScheme,
                     Array2d<PetscReal> & phi)
{
  if (m_order < 1)
  {
    PetscMPIInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    if (rank == 0)
    {
      std::cout << "Must set basis order before using." << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  if (phi.m_dim1 != intScheme.m_nIntegrationPoints
      || phi.m_dim2 != m_nBasisFunctions)
  {
    phi.resize(intScheme.m_nIntegrationPoints, m_nBasisFunctions);
  }
  const Array2d<PetscReal> & xi = intScheme.m_gaussPoints;

  for (int i = 0; i < intScheme.m_nIntegrationPoints; i++)
  {
    if (m_dim == 1)
    {
      if (m_order == 1)
      {
        phi(i,0) = .5 * (1. - xi(i,0));
        phi(i,1) = .5 * (1. + xi(i,0));
      }
      if (m_order == 2)
      {
        phi(i,0) = -.5 * (1. - xi(i,0)) * xi(i,0);
        phi(i,1) = .5 * (1. + xi(i,0)) * xi(i,0);
        phi(i,2) = (1. - xi(i,0)) * (1. + xi(i,0));
      }
    }
    if (m_dim == 2)
    {
      if (m_order == 1)
      {
        phi(i,0) = xi(i,0);
        phi(i,1) = xi(i,1);
        phi(i,2) = xi(i,2);
      }
      if (m_order == 2)
      {
        phi(i,0) = xi(i,0) * (2. * xi(i,0) - 1.);
        phi(i,1) = xi(i,1) * (2. * xi(i,1) - 1.);
        phi(i,2) = xi(i,2) * (2. * xi(i,2) - 1.);
        phi(i,3) = 4. * xi(i,0) * xi(i,1);
        phi(i,4) = 4. * xi(i,1) * xi(i,2);
        phi(i,5) = 4. * xi(i,0) * xi(i,2);
      }
    }
    if (m_dim == 3)
    {
      if (m_order == 1)
      {
        phi(i,0) = xi(i,0);
        phi(i,1) = xi(i,1);
        phi(i,2) = xi(i,2);
        phi(i,3) = xi(i,3);
      }
      if (m_order == 2)
      {
        phi(i,0) = xi(i,0) * (2. * xi(i,0) - 1.);
        phi(i,1) = xi(i,1) * (2. * xi(i,1) - 1.);
        phi(i,2) = xi(i,2) * (2. * xi(i,2) - 1.);
        phi(i,3) = xi(i,3) * (2. * xi(i,3) - 1.);
        phi(i,4) = 4. * xi(i,0) * xi(i,1);
        phi(i,5) = 4. * xi(i,1) * xi(i,2);
        phi(i,6) = 4. * xi(i,2) * xi(i,0);
        phi(i,7) = 4. * xi(i,0) * xi(i,3);
        phi(i,8) = 4. * xi(i,1) * xi(i,3);
        phi(i,9) = 4. * xi(i,2) * xi(i,3);
      }
    }
  }
}


void Basis::calc_dPhidX(PetscInt elementId,
                        const IntegrationScheme & intScheme,
                        Array3d<PetscReal> & dPhidX)
{
  if (m_order < 1)
  {
    PetscMPIInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    if (rank == 0)
    {
      std::cout << "Must set basis order before using." << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  if (dPhidX.m_dim1 != intScheme.m_nIntegrationPoints
      || dPhidX.m_dim2 != m_nBasisFunctions || dPhidX.m_dim3 != m_dim)
  {
    dPhidX.resize(intScheme.m_nIntegrationPoints,
                  m_nBasisFunctions,
                  m_dim);
  }
  Array2d<PetscReal> dXidX(m_nLocalCoordinates, m_dim);
  calc_dXidX(elementId, dXidX);

  const Array2d<PetscReal> & xi = intScheme.m_gaussPoints;

  for (int i = 0; i < intScheme.m_nIntegrationPoints; i++)
  {
    if (m_dim == 1)
    {
      if (m_order == 1)
      {
        dPhidX(i,0,0) = -.5 * dXidX(0,0);
        dPhidX(i,1,0) = .5 * dXidX(0,0);
      }
      if (m_order == 2)
      {
        dPhidX(i,0,0) = (-.5 + xi(i,0)) * dXidX(0,0);
        dPhidX(i,1,0) = (.5 + xi(i,0)) * dXidX(0,0);
        dPhidX(i,2,0) = (-2. * xi(i,0)) * dXidX(0,0);
      }
    }
    if (m_dim == 2)
    {
      if (m_order == 1)
      {
        dPhidX(i,0,0) = dXidX(0,0);
        dPhidX(i,0,1) = dXidX(0,1);
        dPhidX(i,1,0) = dXidX(1,0);
        dPhidX(i,1,1) = dXidX(1,1);
        dPhidX(i,2,0) = dXidX(2,0);
        dPhidX(i,2,1) = dXidX(2,1);
      }
      if (m_order == 2)
      {
        dPhidX(i,0,0) = dXidX(0,0) * (4. * xi(i,0) - 1.);
        dPhidX(i,0,1) = dXidX(0,1) * (4. * xi(i,0) - 1.);
        dPhidX(i,1,0) = dXidX(1,0) * (4. * xi(i,1) - 1.);
        dPhidX(i,1,1) = dXidX(1,1) * (4. * xi(i,1) - 1.);
        dPhidX(i,2,0) = dXidX(2,0) * (4. * xi(i,2) - 1.);
        dPhidX(i,2,1) = dXidX(2,1) * (4. * xi(i,2) - 1.);
        dPhidX(i,3,0) = 4. * (xi(i,1) * dXidX(0,0) + xi(i,0) * dXidX(1,0));
        dPhidX(i,3,1) = 4. * (xi(i,1) * dXidX(0,1) + xi(i,0) * dXidX(1,1));
        dPhidX(i,4,0) = 4. * (xi(i,2) * dXidX(1,0) + xi(i,1) * dXidX(2,0));
        dPhidX(i,4,1) = 4. * (xi(i,2) * dXidX(1,1) + xi(i,1) * dXidX(2,1));
        dPhidX(i,5,0) = 4. * (xi(i,2) * dXidX(0,0) + xi(i,0) * dXidX(2,0));
        dPhidX(i,5,1) = 4. * (xi(i,2) * dXidX(0,1) + xi(i,0) * dXidX(2,1));
      }
    }
    if (m_dim == 3)
    {
      if (m_order == 1)
      {
        dPhidX(i,0,0) = dXidX(0,0);
        dPhidX(i,0,1) = dXidX(0,1);
        dPhidX(i,0,2) = dXidX(0,2);
        dPhidX(i,1,0) = dXidX(1,0);
        dPhidX(i,1,1) = dXidX(1,1);
        dPhidX(i,1,2) = dXidX(1,2);
        dPhidX(i,2,0) = dXidX(2,0);
        dPhidX(i,2,1) = dXidX(2,1);
        dPhidX(i,2,2) = dXidX(2,2);
        dPhidX(i,3,0) = dXidX(3,0);
        dPhidX(i,3,1) = dXidX(3,1);
        dPhidX(i,3,2) = dXidX(3,2);
      }
      if (m_order == 2)
      {
        dPhidX(i,0,0) = dXidX(0,0) * (4. * xi(i,0) - 1.);
        dPhidX(i,0,1) = dXidX(0,1) * (4. * xi(i,0) - 1.);
        dPhidX(i,0,2) = dXidX(0,2) * (4. * xi(i,0) - 1.);
        dPhidX(i,1,0) = dXidX(1,0) * (4. * xi(i,1) - 1.);
        dPhidX(i,1,1) = dXidX(1,1) * (4. * xi(i,1) - 1.);
        dPhidX(i,1,2) = dXidX(1,2) * (4. * xi(i,1) - 1.);
        dPhidX(i,2,0) = dXidX(2,0) * (4. * xi(i,2) - 1.);
        dPhidX(i,2,1) = dXidX(2,1) * (4. * xi(i,2) - 1.);
        dPhidX(i,2,2) = dXidX(2,2) * (4. * xi(i,2) - 1.);
        dPhidX(i,3,0) = dXidX(3,0) * (4. * xi(i,3) - 1.);
        dPhidX(i,3,1) = dXidX(3,1) * (4. * xi(i,3) - 1.);
        dPhidX(i,3,2) = dXidX(3,2) * (4. * xi(i,3) - 1.);
        dPhidX(i,4,0) = 4. * (xi(i,1) * dXidX(0,0) + xi(i,0) * dXidX(1,0));
        dPhidX(i,4,1) = 4. * (xi(i,1) * dXidX(0,1) + xi(i,0) * dXidX(1,1));
        dPhidX(i,4,2) = 4. * (xi(i,1) * dXidX(0,2) + xi(i,0) * dXidX(1,2));
        dPhidX(i,5,0) = 4. * (xi(i,2) * dXidX(1,0) + xi(i,1) * dXidX(2,0));
        dPhidX(i,5,1) = 4. * (xi(i,2) * dXidX(1,1) + xi(i,1) * dXidX(2,1));
        dPhidX(i,5,2) = 4. * (xi(i,2) * dXidX(1,2) + xi(i,1) * dXidX(2,2));
        dPhidX(i,6,0) = 4. * (xi(i,2) * dXidX(0,0) + xi(i,0) * dXidX(2,0));
        dPhidX(i,6,1) = 4. * (xi(i,2) * dXidX(0,1) + xi(i,0) * dXidX(2,1));
        dPhidX(i,6,2) = 4. * (xi(i,2) * dXidX(0,2) + xi(i,0) * dXidX(2,2));
        dPhidX(i,7,0) = 4. * (xi(i,3) * dXidX(0,0) + xi(i,0) * dXidX(3,0));
        dPhidX(i,7,1) = 4. * (xi(i,3) * dXidX(0,1) + xi(i,0) * dXidX(3,1));
        dPhidX(i,7,2) = 4. * (xi(i,3) * dXidX(0,2) + xi(i,0) * dXidX(3,2));
        dPhidX(i,8,0) = 4. * (xi(i,3) * dXidX(1,0) + xi(i,1) * dXidX(3,0));
        dPhidX(i,8,1) = 4. * (xi(i,3) * dXidX(1,1) + xi(i,1) * dXidX(3,1));
        dPhidX(i,8,2) = 4. * (xi(i,3) * dXidX(1,2) + xi(i,1) * dXidX(3,2));
        dPhidX(i,9,0) = 4. * (xi(i,3) * dXidX(2,0) + xi(i,2) * dXidX(3,0));
        dPhidX(i,9,1) = 4. * (xi(i,3) * dXidX(2,1) + xi(i,2) * dXidX(3,1));
        dPhidX(i,9,2) = 4. * (xi(i,3) * dXidX(2,2) + xi(i,2) * dXidX(3,2));
      }
    }
  }
}


void Basis::calc_laplacePhi(PetscInt elementId, Array1d<PetscReal> &laplacePhi)
{
  if (m_order < 1)
  {
    PetscMPIInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    if (rank == 0)
    {
      std::cout << "Must set basis order before using." << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  if (laplacePhi.m_size != m_nBasisFunctions)
  {
    laplacePhi.resize(m_nBasisFunctions);
  }
  Array2d<PetscReal> dXidX(m_nLocalCoordinates, m_dim);
  calc_dXidX(elementId, dXidX);

  if (m_dim == 1)
  {
    laplacePhi(0) = dXidX(0,0) * dXidX(0,0);
    laplacePhi(1) = dXidX(0,0) * dXidX(0,0);
    laplacePhi(1) = -2. * dXidX(0,0) * dXidX(0,0);
  }
  if (m_dim == 2)
  {
    laplacePhi(0) = 4. * (dXidX(0,0) * dXidX(0,0) + dXidX(0,1) * dXidX(0,1));
    laplacePhi(1) = 4. * (dXidX(1,0) * dXidX(1,0) + dXidX(1,1) * dXidX(1,1));
    laplacePhi(2) = 4. * (dXidX(2,0) * dXidX(2,0) + dXidX(2,1) * dXidX(2,1));
    laplacePhi(3) = 8. * (dXidX(0,0) * dXidX(1,0) + dXidX(0,1) * dXidX(1,1));
    laplacePhi(4) = 8. * (dXidX(1,0) * dXidX(2,0) + dXidX(1,1) * dXidX(2,1));
    laplacePhi(5) = 8. * (dXidX(0,0) * dXidX(2,0) + dXidX(0,1) * dXidX(2,1));
  }
  if (m_dim == 3)
  {
    laplacePhi(0) = 4. * (dXidX(0,0) * dXidX(0,0) + dXidX(0,1) * dXidX(0,1)
                          + dXidX(0,2) * dXidX(0,2));
    laplacePhi(1) = 4. * (dXidX(1,0) * dXidX(1,0) + dXidX(1,1) * dXidX(1,1)
                          + dXidX(1,2) * dXidX(1,2));
    laplacePhi(2) = 4. * (dXidX(2,0) * dXidX(2,0) + dXidX(2,1) * dXidX(2,1)
                          + dXidX(2,2) * dXidX(2,2));
    laplacePhi(3) = 4. * (dXidX(3,0) * dXidX(3,0) + dXidX(3,1) * dXidX(3,1)
                          + dXidX(3,2) * dXidX(3,2));
    laplacePhi(4) = 8. * (dXidX(0,0) * dXidX(1,0) + dXidX(0,1) * dXidX(1,1)
                          + dXidX(0,2) * dXidX(1,2));
    laplacePhi(5) = 8. * (dXidX(1,0) * dXidX(2,0) + dXidX(1,1) * dXidX(2,1)
                          + dXidX(1,2) * dXidX(2,2));
    laplacePhi(6) = 8. * (dXidX(0,0) * dXidX(2,0) + dXidX(0,1) * dXidX(2,1)
                          + dXidX(0,2) * dXidX(2,2));
    laplacePhi(7) = 8. * (dXidX(0,0) * dXidX(3,0) + dXidX(0,1) * dXidX(3,1)
                          + dXidX(0,2) * dXidX(3,2));
    laplacePhi(8) = 8. * (dXidX(1,0) * dXidX(3,0) + dXidX(1,1) * dXidX(3,1)
                          + dXidX(1,2) * dXidX(3,2));
    laplacePhi(9) = 8. * (dXidX(2,0) * dXidX(3,0) + dXidX(2,1) * dXidX(3,1)
                          + dXidX(2,2) * dXidX(3,2));
  }
}

