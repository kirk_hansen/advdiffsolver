#ifndef IOUTILITIES_HPP
#define IOUTILITIES_HPP

#include <string>
#include "petscksp.h"

namespace IoUtilities
{
  /** Writes a PetscReal Petsc vector to a bin file. First entry of the file is
    * a 4-byte integer containing the number of entries, the remainder are
    * doubles.
    * \param vector The Petsc vector to write to file.
    * \param filename The output filename root.
    * \param nEntries The number of entries of vector to write to file. */
  void writePetscVectorToBin(Vec & vector,
                             std::string filename,
                             uint32_t nEntries);


}

#endif
