#ifndef BASIS_HPP
#define BASIS_HPP

#include "petscksp.h"

#include "mesh.hpp"
#include "array_types.hpp"

#ifndef DIM
#define DIM 3
#endif


class Basis
{
public:
  /** Constructs a basis with given mesh and dimensionality. If this
   *  constructor is used then setOrder must be called before using basis. */
  Basis(const Mesh &mesh, int dim=DIM);

  Basis(const Mesh &mesh, int basisOrder, int dim=DIM);

  void setOrder(int basisOrder);

  void calc_dXidX(PetscInt elementId, Array2d<PetscReal> & dXidX);

  void print_dXidX(PetscInt elementId);

  void calc_phi(const IntegrationScheme & intScheme,
                Array2d<PetscReal> & phi);

  void calc_dPhidX(PetscInt elementId,
                   const IntegrationScheme & intScheme,
                   Array3d<PetscReal> & dPhidX);

  void calc_laplacePhi(PetscInt elementId, Array1d<PetscReal> & laplacePhi);

  int m_nBasisFunctions;
  int m_nLocalCoordinates;
  const int m_dim;
  const Mesh &m_mesh;
  int m_order;
};

#endif
