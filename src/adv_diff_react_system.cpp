#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include "adv_diff_react_system.hpp"
#include "ioutilities.hpp"

std::string AdvDiffReactSystem::readNextLine(std::istream &fin)
{
  std::string line;
  while (getline(fin, line))
  {
    if (line.length() > 0 && line[0] != '#')
    {
      break;
    }
  }
  return line;
}

template <typename T>
std::string toString(T rhs)
{
  std::stringstream ss;
  ss << rhs;
  return ss.str();
}


AdvDiffReactSystem::AdvDiffReactSystem()
     : m_femSolver(m_mesh, m_solution),
       m_reactionSolver(m_solution)
{
  // Set defaults
  m_t = 0.;
  m_eps = 1e-9;

  MPI_Comm_rank(PETSC_COMM_WORLD,&m_MPIrank);
  MPI_Comm_size(PETSC_COMM_WORLD,&m_MPIsize);
}


AdvDiffReactSystem::AdvDiffReactSystem(std::string filename)
     : m_femSolver(m_mesh, m_solution),
       m_reactionSolver(m_solution)
{
  // Set defaults
  m_t = 0.;
  m_eps = 1e-9;

  MPI_Comm_rank(PETSC_COMM_WORLD,&m_MPIrank);
  MPI_Comm_size(PETSC_COMM_WORLD,&m_MPIsize);
  initFromFile(filename);
}


AdvDiffReactSystem::~AdvDiffReactSystem()
{
  for (int i = 0; i < m_solution.size(); i++)
  {
    VecDestroy(&m_solution[i]);
  }
}


void AdvDiffReactSystem::initFromFile(std::string filename)
{
  if (m_MPIrank == 0)
  {
    std::cout << "Reading parameters from " << filename << "."
              << std::endl << std::endl;
  }

  // Read params from file
  std::ifstream fin(filename.c_str());

  std::string::size_type sz;

  bool isReactiveSystem = std::atoi(readNextLine(fin).c_str()) != 0;
  if (isReactiveSystem)
  {
    std::string reactionFilename;
    reactionFilename = readNextLine(fin);
    if (m_MPIrank == 0)
    {
      std::cout << "Reading reactive system from "
                << reactionFilename << std::endl;
    }
    m_chemicalSystem.loadFromFile(reactionFilename);
    m_chemicalSystem.outputSystem();
  }
  else
  {
    if (m_MPIrank == 0)
    {
      std::cout << "Not a reactive system." << std::endl;
    }
    m_chemicalSystem.initSingleSpecies();

    m_chemicalSystem.m_isMobile[0] = true;
    m_chemicalSystem.m_diffusivities[0] = std::atof(readNextLine(fin).c_str());
    m_chemicalSystem.m_initialConditions[0] =
                                          std::atof(readNextLine(fin).c_str());
  }

  std::string connectivityFilename = readNextLine(fin);
  if (m_MPIrank == 0)
  {
    std::cout << "Connectivity filename: " << connectivityFilename << std::endl;
  }

  std::string coordinatesFilename = readNextLine(fin);
  if (m_MPIrank == 0)
  {
    std::cout << "Coordinates filename: " << coordinatesFilename << std::endl;
  }

  std::string mapppingFilename = readNextLine(fin);
  if (m_MPIrank == 0)
  {
    std::cout << "Mapping filename: " << mapppingFilename << std::endl;
  }

  int basisOrder = std::atoi(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "FEM basis function order: " << basisOrder << std::endl;
  }

  if (m_MPIrank == 0)
  {
    std::cout << "Initializing mesh..." << std::endl;;
  }
  m_mesh.initialize(connectivityFilename,
                    coordinatesFilename,
                    basisOrder);
  m_mesh.writeMappingToFile(mapppingFilename, m_MPIrank);
  if (m_MPIrank == 0)
  {
    std::cout << "Mesh initialized." << std::endl;
  }

  m_solution.resize(m_chemicalSystem.m_nSpecies);
  for (int i = 0; i < m_chemicalSystem.m_nSpecies; i++)
  {
    VecCreate(PETSC_COMM_WORLD, &m_solution[i]);
    VecSetSizes(m_solution[i],
                m_mesh.m_nOwnedNodes,
                m_mesh.m_nTotalNodesGlobal);
    if (m_MPIsize == 1)
    {
      VecSetType(m_solution[i], VECSEQ);
    }
    else
    {
      VecSetType(m_solution[i], VECMPI);
    }
    VecSetFromOptions(m_solution[i]);
  }

  bool useInitialCondition = !(std::atoi(readNextLine(fin).c_str()) == 0);
  if (useInitialCondition)
  {
    std::string icFilenameRoot = readNextLine(fin);
    for (int i = 0; i < m_chemicalSystem.m_nSpecies; i++)
    {
      loadInitialConditionFromBin(icFilenameRoot
                                  + "."
                                  + m_chemicalSystem.m_speciesNames[i]
                                  + ".bin",
                                  i);
    }
  }
  else
  {
    for (int i = 0; i < m_chemicalSystem.m_nSpecies; i++)
    {
      VecSet(m_solution[i], m_chemicalSystem.m_initialConditions[i]);
    }
  }

  m_reactionSolver.initialize(m_chemicalSystem.m_volumeReactions);

  PetscReal rhoInfinity = std::atof(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Rho infinity: " << rhoInfinity << std::endl;
  }

  bool stabilized = (std::atoi(readNextLine(fin).c_str()) != 0);
  if (m_MPIrank == 0)
  {
    std::cout << "Use SUPG stabilization?: " << stabilized << std::endl;
  }

  bool store_dPhidX = (std::atoi(readNextLine(fin).c_str()) != 0);
  if (m_MPIrank == 0)
  {
    std::cout << "Precompute dPhidX?: " << store_dPhidX << std::endl;
  }

  int velocityBasisOrder = std::atoi(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Velocity field basis order: " << velocityBasisOrder << std::endl;
  }

  m_femSolver.initialize(basisOrder,
                         velocityBasisOrder,
                         stabilized,
                         store_dPhidX,
                         false);

  m_femSolver.m_rhoInfinity = rhoInfinity;

  m_femSolver.m_velFilenameRoot = readNextLine(fin);
  if (m_MPIrank == 0)
  {
    std::cout << "Velocity field filename root: "
              << m_femSolver.m_velFilenameRoot << std::endl;
  }

  m_femSolver.m_velFileStart = std::atoi(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Velocity field starting time step: "
            << m_femSolver.m_velFileStart << std::endl;
  }

  m_femSolver.m_nVelFiles = std::atoi(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Number of velocity files: " << m_femSolver.m_nVelFiles
              << std::endl;
  }

  m_femSolver.m_velFileInterval = std::atoi(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Velocity file interval: " << m_femSolver.m_velFileInterval
              << std::endl;
  }

  m_femSolver.m_velFileDt = std::atof(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Time between velocity files: " << m_femSolver.m_velFileDt
              << std::endl;
  }

  m_currentTstep = std::atoi(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Initial time step: " << m_currentTstep << std::endl;
  }

  PetscInt nTsteps = std::atoi(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Number of time steps: " << nTsteps << std::endl;
  }
  m_finalTstep = m_currentTstep + nTsteps;

  m_dt = std::atof(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "dt: " << m_dt << std::endl;
  }

  m_t = std::atof(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Simulation start time: " << m_t << std::endl;
  }

  m_outputFilenameRoot = readNextLine(fin);
  if (m_MPIrank == 0)
  {
    std::cout << "Output filename root: " << m_outputFilenameRoot << std::endl;
  }

  m_saveInterval = std::atoi(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Save interval: " << m_saveInterval << std::endl;
  }

  m_restartInterval = std::atoi(readNextLine(fin).c_str());
  if (m_MPIrank == 0)
  {
    std::cout << "Restart interval: " << m_restartInterval << std::endl;
  }

  int nDirichletBCs = std::atoi(readNextLine(fin).c_str());
  for (int i = 0; i < nDirichletBCs; i++)
  {
    std::string bcFilename = readNextLine(fin);
    if (m_MPIrank == 0)
    {
      std::cout << "Loading Dirichlet BC from " << bcFilename << std::endl;
    }
    m_femSolver.m_dirichletBCs.push_back(DirichletBC(m_mesh,
                                                m_chemicalSystem.m_nSpecies));
    m_femSolver.m_dirichletBCs[i].loadNodes(bcFilename);
    bool isInlet = !(std::atoi(readNextLine(fin).c_str()) == 0);
    if (isInlet)
    {
      if (m_MPIrank == 0)
      {
        std::cout << "Inlet BC. Setting values to those in ChemicalSystem."
                  << std::endl;
      }
      for (int j = 0; j < m_chemicalSystem.m_nSpecies; j++)
      {
        m_femSolver.m_dirichletBCs[i].m_speciesGetsBC[j] = true;
        m_femSolver.m_dirichletBCs[i].setValues(
                                       m_chemicalSystem.m_initialConditions[j],
                                       j);
      }
    }
    else
    {
      int speciesID = std::atoi(readNextLine(fin).c_str());
      PetscReal val = std::atof(readNextLine(fin).c_str());
      if (m_MPIrank == 0)
      {
        std::cout << "Value of " << val << " applied to species " << speciesID
                  << std::endl;
      }
      m_femSolver.m_dirichletBCs[i].m_speciesGetsBC[speciesID] = true;
      m_femSolver.m_dirichletBCs[i].setValues(val, speciesID);
    }
  }

  int nOutletBCs = std::atoi(readNextLine(fin).c_str());
  for (int i = 0; i < nOutletBCs; i++)
  {
    std::string bcFilename = readNextLine(fin);
    if (m_MPIrank == 0)
    {
      std::cout << "Loading outlet BC from " << bcFilename << std::endl;
    }
    m_femSolver.m_outletBCs.push_back(OutletBC(m_mesh,
                                               m_chemicalSystem.m_nSpecies));
    m_femSolver.m_outletBCs[i].loadNodes(bcFilename);
    std::string normalString = readNextLine(fin);
    std::stringstream stream(normalString);
    PetscReal normal[DIM];
    for (int j = 0; j < DIM; j++)
    {
      stream >> normal[j];
    }
    m_femSolver.m_outletBCs[i].setNormals(normal);
  }

  int nNeumannBCs = std::atoi(readNextLine(fin).c_str());
  for (int i = 0; i < nNeumannBCs; i++)
  {
    std::string bcFilename = readNextLine(fin);
    if (m_MPIrank == 0)
    {
      std::cout << "Loading Neumann BC from " << bcFilename << std::endl;
    }
    m_femSolver.m_neumannBCs.push_back(NeumannBC(m_mesh,
                                                 basisOrder, 2 * basisOrder));
    m_femSolver.m_neumannBCs[i].loadNodes(bcFilename);
    bool isReactive = !(std::atoi(readNextLine(fin).c_str()) == 0);
    if (isReactive)
    {
      if (m_MPIrank == 0)
      {
        std::cout << "Reactive BC. "
                  << "Linking to surface reactions in ChemicalSystem."
                  << std::endl;
      }
      for (int j = 0; j < m_chemicalSystem.m_nSpecies; j++)
      {
        m_femSolver.m_neumannBCs[i].m_speciesGetsBC[j] = true;
      }
      m_reactiveBCs.push_back(ReactiveBC());
      m_reactiveBCs[m_reactiveBCs.size()-1].initialize(
                                         &m_solution,
                                         &m_femSolver.m_neumannBCs[i],
                                         &m_chemicalSystem.m_surfaceReactions);
    }
    else
    {
      int speciesID = std::atoi(readNextLine(fin).c_str());
      PetscReal val = std::atof(readNextLine(fin).c_str());
      if (m_MPIrank == 0)
      {
        std::cout << "Flux of " << val << " applied to species " << speciesID
                  << std::endl;
      }
      m_femSolver.m_neumannBCs[i].m_speciesGetsBC[speciesID] = true;
      m_femSolver.m_neumannBCs[i].setValues(val, speciesID);
    }
  }
  if (m_MPIrank == 0)
  {
    std::cout << "Boundary conditions loaded.\n\n" << std::endl;
  }

  fin.close();

  initNeedsLHSRebuild();
}


void AdvDiffReactSystem::solveSystem()
{

  writeHeader();
  while (m_currentTstep < m_finalTstep)
  {
    takeTimeStep();
  }

  if (m_saveInterval == 0)
  {
    writeSolutionToBin(m_outputFilenameRoot, m_currentTstep, true);
  }

  if (m_restartInterval == 0)
  {
    writeSolutionToBin("restart." + m_outputFilenameRoot,
                       m_currentTstep,
                       false);
  }
}


void AdvDiffReactSystem::takeTimeStep()
{

  // Solve reactions
  m_reactionSolver.computeReactions(m_dt);

  // Solve adv-diff (only for mobile species).

  // Update reactive BCs
  for (int i = 0; i < m_reactiveBCs.size(); i++)
  {
    m_reactiveBCs[i].updateValues();
  }

  // Update velocity
  m_femSolver.updateVelocity(m_t);
  m_femSolver.m_dt = m_dt;

  for (int i = 0; i < m_solution.size(); i++)
  {
    // Build LHS (if necessary)
    if (m_needsLHSRebuild[i])
    {
      m_femSolver.m_diffusivity = m_chemicalSystem.m_diffusivities[i];
      m_femSolver.buildLHS();
    }
    // Build RHS
    m_femSolver.buildRHS(i);

    // Apply BCs
    m_femSolver.applyBCs(i);

    // Take time step
    PetscInt nIterations = m_femSolver.solve(i);

    // Output results
    PetscReal maxval;
    PetscReal minval;
    VecMax(m_solution[i], NULL, &maxval);
    VecMin(m_solution[i], NULL, &minval);

    if (m_MPIrank == 0)
    {
      std::cout << m_currentTstep << " "
                << m_t << " "
                << i << " "
                << nIterations << " "
                << maxval << " "
                << minval << " "
                << std::endl;
    }
  }
  m_t += m_dt;
  m_currentTstep++;

  if (m_saveInterval > 0 && m_currentTstep % m_saveInterval == 0)
  {
    writeSolutionToBin(m_outputFilenameRoot, m_currentTstep, true);
  }

  if (m_restartInterval > 0 && m_currentTstep % m_restartInterval == 0)
  {
    writeSolutionToBin("restart." + m_outputFilenameRoot,
                       m_currentTstep,
                       false);
  }
}


void AdvDiffReactSystem::writeSolutionToBin(std::string filenameRoot,
                                          PetscInt tstep,
                                          bool order1Only)
{
  uint32_t nEntries;
  if (order1Only == true)
  {
    nEntries = m_mesh.m_nOwnedCornerNodes;
  }
  else
  {
    nEntries = m_mesh.m_nOwnedNodes;
  }

  for (int i = 0; i < m_chemicalSystem.m_nSpecies; i++)
  {
    std::string speciesName = m_chemicalSystem.m_speciesNames.size() >= i ?
                              m_chemicalSystem.m_speciesNames[i] : toString(i);
    std::string filename = filenameRoot + "." + speciesName
                           + "." + toString(tstep) + ".bin"
                           + "." + toString(m_MPIrank);
    IoUtilities::writePetscVectorToBin(m_solution[i], filename, nEntries);
  }
}


void AdvDiffReactSystem::loadInitialConditionFromBin(std::string filename,
                                                     int species)
{
  if (m_MPIrank == 0)
  {
    std::cout << "Loading initial condition for species " << species
              << " from " << filename << std::endl;
  }

  uint32_t nEntries;
  PetscInt nEntriesMPI; // To ensure types are right
  PetscInt icOrder;
  Array1d<PetscReal> serialSolution;

  if (m_MPIrank == 0)
  {
    std::fstream fin;
    fin.open(filename.c_str(), std::ios::in | std::ios::binary);

    // Read number of entries from header
    fin.read((char *) &nEntries, sizeof(uint32_t));

    if (nEntries == m_mesh.m_nCornerNodesGlobal)
    {
      icOrder = 1;
    }
    else if (nEntries == m_mesh.m_nTotalNodesGlobal)
    {
      icOrder = 2;
    }
    else
    {
      std::cout << "Mesh has " << m_mesh.m_nCornerNodesGlobal
                << " corner nodes and " << m_mesh.m_nTotalNodesGlobal
                << " total nodes, but initial condition file has "
                << nEntries << " entries. Aborting." << std::endl;
      MPI_Abort(PETSC_COMM_WORLD, 1);
    }
    nEntriesMPI = nEntries;

    serialSolution.resize(nEntries);
    if (sizeof(PetscReal) == sizeof(double))
    {
      fin.read((char *) serialSolution.front(),
               nEntries * sizeof(double));
      fin.close();
    }
    else // Need to convert to doubles before writing
    {
      Array1d<double> arrayDoubles(nEntries);
      fin.read((char *) arrayDoubles.front(), nEntries * sizeof(double));
      for (PetscInt i = 0; i < nEntries; i++)
      {
        serialSolution(i) = arrayDoubles(i);
      }
    }
    fin.close();
  }
  MPI_Bcast(&nEntriesMPI, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  MPI_Bcast(&icOrder, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  nEntries = nEntriesMPI;
  if (m_MPIrank > 0)
  {
    serialSolution.resize(nEntries);
  }
  MPI_Bcast(serialSolution.front(), nEntriesMPI, MPIU_REAL, 0,
            PETSC_COMM_WORLD);

  // Map serial values to local
  Array1d<PetscReal> localSolution(m_mesh.m_nOwnedNodes);
  for (PetscInt i = 0; i < m_mesh.m_nOwnedCornerNodes; i++)
  {
    localSolution(i) = serialSolution(m_mesh.m_parallelToSerialMap(i));
  }
  for (PetscInt i = m_mesh.m_nOwnedCornerNodes; i < m_mesh.m_nOwnedNodes; i++)
  {
    if (icOrder == 2)
    {
      localSolution(i) = serialSolution(m_mesh.m_parallelToSerialMap(i));
    }
    else // Linearly interpolate between two neighbors
    {
      PetscInt order2Index = i - m_mesh.m_nOwnedCornerNodes;
      localSolution(i) =
          .5 * (serialSolution(m_mesh.m_parallelToSerialMap(
                               m_mesh.m_order2nodeNeighbors(order2Index, 0)))
                + serialSolution(m_mesh.m_parallelToSerialMap(
                               m_mesh.m_order2nodeNeighbors(order2Index, 1))));
    }
  }

  VecSetValues(m_solution[species],
               m_mesh.m_nOwnedNodes,
               m_mesh.m_localToGlobalMap.front(),
               localSolution.front(),
               INSERT_VALUES);
  VecAssemblyBegin(m_solution[species]);
  VecAssemblyEnd(m_solution[species]);
}


void AdvDiffReactSystem::initNeedsLHSRebuild()
{
  m_needsLHSRebuild.resize(m_chemicalSystem.m_nSpecies);

  // First species always needs rebuild (new tstep)
  m_needsLHSRebuild[0] = true;

  // Check if diffusivities are different than previous species
  for (int i = 1; i < m_chemicalSystem.m_nSpecies; i++)
  {
    PetscReal err = std::abs(m_chemicalSystem.m_diffusivities[i]
                             - m_chemicalSystem.m_diffusivities[i - 1])
                    / m_chemicalSystem.m_diffusivities[i];
    if (err > m_eps)
    {
      m_needsLHSRebuild[i] = true;
    }
    else
    {
      m_needsLHSRebuild[i] = false;
    }
  }

  // Check for unique Dirichlet BCs
  for (int i = 0; i < m_femSolver.m_dirichletBCs.size(); i++)
  {
    for (int j = 1; j < m_chemicalSystem.m_nSpecies; j++)
    {
      if (m_femSolver.m_dirichletBCs[i].m_speciesGetsBC[j] !=
          m_femSolver.m_dirichletBCs[i].m_speciesGetsBC[j - 1])
      {
        m_needsLHSRebuild[j] = true;
      }
    }
  }
}


void AdvDiffReactSystem::writeHeader()
{
  if (m_MPIrank == 0)
  {
    std::cout << "Tstep Time SpeciesID SolverIterations MaxVal MinVal"
              << std::endl;
  }
}


