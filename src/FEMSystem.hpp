#ifndef FEMSOLVER_HPP
#define FEMSOLVER_HPP

#include <vector>
#include <string>

#include "petscksp.h"

#include "mesh.hpp"
#include "array_types.hpp"
#include "gauss_legendre.hpp"
#include "basis.hpp"
#include "bcs.hpp"


class FEMSystem
{
public:
  FEMSystem(Mesh &mesh);

  // ~FEMSystem();

  Mesh &m_mesh;
  std::vector<DirichletBC> m_dirichletBCs;
  std::vector<NeumannBC> m_neumannBCs;
  std::vector<IntegrationScheme> m_integrationSchemes;


  PetscMPIInt m_MPIsize;
  PetscMPIInt m_MPIrank;

};


class PoissonSolverSteady: public FEMSystem
// Solves div(grad(f)) = g
{
public:
  PoissonSolverSteady(Mesh &mesh, int order);
  ~PoissonSolverSteady();

  void setUpSolver(); // Creates PETSc matrices and vectors

  void buildLHS();

  void setRHS(PetscReal source);

  void applyDirichletBCs();

  void applyNeumannBCs();

  void solve();

  void writeSolutionToBin(std::string filename, bool order1Only=true);

  // void getNodeConnectivity(Array1d<PetscInt> &nConnectedLocalNodes,
  //                          Array1d<PetscInt> &nConnectedNonLocalNodes);
//protected:

  Basis m_basis;
  IntegrationScheme m_integrationScheme;
  Vec m_x, m_b;
  Mat m_A;
  KSP m_ksp;
  PC m_pc;
  PetscInt m_localDOF;
  PetscInt m_globalDOF;
  PetscInt m_enon;

};

#endif
