#ifndef REACTIVE_BC_HPP
#define REACTIVE_BC_HPP

#include <vector>
#include "petscksp.h"
#include "bcs.hpp"
#include "reaction_system.hpp"

class ReactiveBC
{
public:
  ReactiveBC();

  ReactiveBC(std::vector<Vec> *solution,
             NeumannBC *bc,
             std::vector<ReactionEquation> *surfaceReactions);

  ~ReactiveBC();

  void initialize(std::vector<Vec> *solution,
                  NeumannBC *bc,
                  std::vector<ReactionEquation> *surfaceReactions);

  /** Updates the boundary condition nodal values in m_bc based on the current
   *  values in m_solution. */
  void updateValues();

  /** Solution used to calculate the surface reactions. */
  std::vector<Vec> *m_solution;

  /** Boundary condition to which reaction should be applied. */
  NeumannBC *m_bc;

  /** System of reactions used to evaluate boundary condition values. */
  std::vector<ReactionEquation> *m_surfaceReactions;

  /** Temporary vector used to evaluate the surface reaction rates. It is only
    * preallocated and stored as a member variable for efficiency. */
  Vec m_rate;

  bool m_initialized;
};

#endif
