#include <string>
#include <iostream>
#include <fstream>
#include <stdint.h>
#include <cstring>
#include <vector>
#include <stdlib.h>
#include <cmath>

#include "petscsys.h"
#include "metis.h"

#include "mesh.hpp"
#include "vectorND.hpp"
#include "array_types.hpp"

#ifdef DEBUG
#define DEBUG_MSG(str) do { std::cout << str << std::endl; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif


Mesh::Mesh()
    : m_order1Enon(DIM + 1),
      m_nElements(0),
      m_nCornerNodes(0),
      m_nTotalNodes(0),
      m_enon(DIM + 1)
{
}


Mesh::Mesh(std::string connectivityFilename,
           std::string coordinatesFilename,
           int order=1)
    : m_order1Enon(DIM + 1), m_enon(DIM + 1)
{
  initialize(connectivityFilename,
             coordinatesFilename,
             order);
}

void Mesh::initialize(std::string connectivityFilename,
                      std::string coordinatesFilename,
                      int order)
{
  PetscErrorCode ierr;

  PetscMPIInt size;
  PetscMPIInt rank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);


  Array1d<PetscInt> elementPartition;
  Array1d<PetscInt> nodePartition;

  if (rank == 0)
  {
    loadFromFile(connectivityFilename, coordinatesFilename);
    if (order == 2)
    {
      make2ndOrder();
    }

    m_nCornerNodesGlobal = m_nCornerNodes;
    m_nTotalNodesGlobal = m_nTotalNodes;
    // std::cout << "Mesh loaded. " << m_nTotalNodes << " DOF." << std::endl;

    elementPartition.resize(m_nElements);
    nodePartition.resize(m_nTotalNodes);
    if (size == 1)
    {
      elementPartition = 0;
      nodePartition = 0;
    }
    else
    {
    partition(elementPartition, nodePartition);
    }
  }

  // Note: Less memory would be used if the whole coordinates and connectivity
  // was not Bcast to each node. The following should be redone if memory
  // becomes an issue. Also, if the first set of Bcasts is slow all of the Mesh
  // parameters can be put into a struct and Bcast all at once (probably fine
  // though).

  MPI_Bcast(&m_nElements, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  MPI_Bcast(&m_nCornerNodes, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  MPI_Bcast(&m_nTotalNodes, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  MPI_Bcast(&m_nCornerNodesGlobal, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  MPI_Bcast(&m_nTotalNodesGlobal, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  MPI_Bcast(&m_order1Enon, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  MPI_Bcast(&m_enon, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  if (rank > 0)
  {
    elementPartition.resize(m_nElements);
    nodePartition.resize(m_nTotalNodes);
    m_connectivity.resize(m_nElements, m_enon);
    m_coordinates.resize(m_nCornerNodes);
    m_order2nodeNeighbors.resize(m_nTotalNodes - m_nCornerNodes, 3);
  }

  MPI_Bcast(elementPartition.front(), m_nElements, MPIU_INT, 0,
            PETSC_COMM_WORLD);
  MPI_Bcast(nodePartition.front(), m_nTotalNodes, MPIU_INT, 0,
            PETSC_COMM_WORLD);
  MPI_Bcast(m_connectivity.front(), m_nElements * m_enon, MPIU_INT, 0,
            PETSC_COMM_WORLD);
  MPI_Bcast(m_coordinates.front(), m_nCornerNodes * DIM, MPIU_REAL, 0,
            PETSC_COMM_WORLD);
  if (order == 2)
  {
    MPI_Bcast(m_order2nodeNeighbors.front(),
              (m_nTotalNodes - m_nCornerNodes) * 3, MPIU_INT, 0,
              PETSC_COMM_WORLD);
  }

  convertGlobalMeshToLocal(elementPartition, nodePartition, size, rank);

  calculateJacobian();

  computeMetricTensor();
}


void Mesh::loadFromFile(std::string connectivityFilename,
                        std::string coordinatesFilename)
{
  // Assumes coordinates and connectivity files use doubles and 32 bit ints.
  std::fstream fin;
  fin.open(connectivityFilename.c_str(), std::ios::in | std::ios::binary);
  // Use uint32_t to ensure that read is the right size.
  uint32_t nElements;
  fin.read((char *) &nElements, sizeof(uint32_t));
  m_nElements = (PetscInt) nElements;
  DEBUG_MSG("Reading connectivity from " << connectivityFilename << ", "
            << m_nElements << " elements.");
  m_connectivity.resize(m_nElements, m_order1Enon);
  std::vector<uint32_t> tmpConnectivity(m_nElements * m_order1Enon);
  fin.read(reinterpret_cast<char*>(tmpConnectivity.data()),
           m_nElements * m_order1Enon * sizeof(uint32_t));
  fin.close();
  for (PetscInt i = 0; i < m_nElements; i++)
  {
    for (int j = 0; j < m_order1Enon; j++)
    {
      m_connectivity(i,j) = tmpConnectivity[m_order1Enon*i + j];
    }
  }

  fin.open(coordinatesFilename.c_str(), std::ios::in | std::ios::binary);
  // Use uint32_t to ensure that read is the right size.
  uint32_t nNodes;
  fin.read((char *) &nNodes, sizeof(uint32_t));
  m_nCornerNodes = (PetscInt) nNodes;
  m_nTotalNodes = m_nCornerNodes;
  DEBUG_MSG("Reading coordinates from " << coordinatesFilename << ", "
            << m_nCornerNodes << " nodes.");
  m_coordinates.resize(m_nCornerNodes);
  std::vector<double> tmpCoordinates(m_nCornerNodes * DIM);
  fin.read(reinterpret_cast<char*>(tmpCoordinates.data()),
           m_nCornerNodes * DIM * sizeof(double));
  fin.close();
  for (PetscInt i = 0; i < m_nCornerNodes; i++)
  {
    for (int j = 0; j < DIM; j++)
    {
      m_coordinates(i)[j] = tmpCoordinates[DIM*i + j];
    }
  }
}


void Mesh::partition(Array1d<PetscInt> &elementPartition,
                     Array1d<PetscInt> &nodePartition,
                     std::string partitionType)
{
  PetscErrorCode ierr;

  PetscMPIInt size;
  PetscMPIInt rank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  if (rank == 0)
  {
    std::cout << "Partitioning mesh among " << size << " nodes." << std::endl;

    // Set up METIS arrays to ensure consistent types
    idx_t ne = m_nElements;
    idx_t nn = m_nTotalNodes;
    idx_t ncommon = DIM;
    idx_t nparts = size;
    idx_t objval;
    Array1d<idx_t> eptr(ne + 1);
    Array2d<idx_t> eind(ne, m_enon);
    Array1d<idx_t> epart(ne);
    Array1d<idx_t> npart(nn);

    for (idx_t i = 0; i < ne + 1; i++)
    {
      eptr(i) = i * m_enon;
    }

    for (idx_t i = 0; i < ne; i++)
    {
      for (int j = 0; j < m_enon; j++)
      {
        eind(i, j) = m_connectivity(i, j);
      }
    }

    if (partitionType == "dual")
    {
      METIS_PartMeshDual(&ne, &nn, eptr.front(), eind.front(), NULL, NULL,
                         &ncommon, &nparts, NULL, NULL, &objval, epart.front(),
                         npart.front());
    }
    else if (partitionType == "nodal")
    {
      METIS_PartMeshNodal(&ne, &nn, eptr.front(), eind.front(),
                          NULL, NULL, &nparts, NULL, NULL, &objval,
                          epart.front(), npart.front());
    }
    else
    {
      if (rank == 0)
      {
        std::cout << "Invalid partition type: " << partitionType
                  << ". Valid types are 'dual' and 'nodal'" << std::endl;
        MPI_Abort(PETSC_COMM_WORLD, 1);
      }
    }

    // Convert epart and npart to PetscInt type
    for (PetscInt i = 0; i < ne; i++)
    {
      elementPartition(i) = epart(i);
    }
    for (PetscInt i = 0; i < nn; i++)
    {
      nodePartition(i) = npart(i);
    }
  }
}


void Mesh::convertGlobalMeshToLocal(const Array1d<PetscInt> &elementPartition,
                                    const Array1d<PetscInt> &nodePartition,
                                    const PetscMPIInt size,
                                    const PetscMPIInt rank)
{
  // Find number of owned nodes and owned corner nodes on each process
  Array1d<PetscInt> ownedNodes(size);
  Array1d<PetscInt> ownedCornerNodes(size);
  ownedNodes = 0;
  ownedCornerNodes = 0;

  for (PetscInt i = 0; i < m_nTotalNodes; i++)
  {
    ownedNodes(nodePartition(i))++;
    if (i < m_nCornerNodes)
    {
      ownedCornerNodes(nodePartition(i))++;
    }
  }
  m_nOwnedNodes = ownedNodes(rank);
  m_nOwnedCornerNodes = ownedCornerNodes(rank);

  // Find start index for each process
  Array1d<PetscInt> currentIndex(size);
  currentIndex = 0;
  for (PetscInt i = 0; i < size; i++)
  {
    for (PetscInt j = i + 1; j < size; j++)
    {
      currentIndex(j) += ownedNodes(i);
    }
  }
  PetscInt startIdx = currentIndex(rank);
  m_globalStartIndex = startIdx;

  // Find serial to parallel and parallel to serial mappings
  // Since order 2 node indices are at the end in the original mesh, they
  // also end up at the end of each local mesh.
  Array1d<PetscInt> parallelToSerial(m_nTotalNodes);
  Array1d<PetscInt> serialToParallel(m_nTotalNodes);
  for (PetscInt i = 0; i < m_nTotalNodes; i++)
  {
    serialToParallel(i) = currentIndex(nodePartition(i));
    parallelToSerial(currentIndex(nodePartition(i))) = i;
    currentIndex(nodePartition(i))++;
  }

  // Find nodes needed by this process
  Array1d<PetscInt> neededNodes(m_nTotalNodes);
  neededNodes = 0;
  PetscInt nElementsNew = 0;
  for (PetscInt i = 0; i < m_nElements; i++)
  {
    if (elementPartition(i) == rank)
    {
      nElementsNew++;
      for (PetscInt j = 0; j < m_enon; j++)
      {
        neededNodes(m_connectivity(i, j)) = 1;
      }
    }
  }

  // Count needed nodes
  PetscInt nTotalNodesNew = 0;
  PetscInt nCornerNodesNew = 0;
  for (PetscInt i = 0; i < m_nTotalNodes; i++)
  {
    if (neededNodes(i) == 1)
    {
      nTotalNodesNew++;
      if (i < m_nCornerNodes)
      {
        nCornerNodesNew++;
      }
    }
  }

  // Create global to local and local to global mappings. Since order 2 nodes
  // are at the end of the original mesh, they should also end up at the end of
  // the "owned" and "ghost" regions of the local mesh, i.e. the order of the
  // local mesh should be 1)owned order 1, 2)owned order 2, 3)ghost order 1,
  // 4)ghost order 2.
  m_localToGlobalMap.resize(nTotalNodesNew);
  Array1d<PetscInt> globalToLocal(m_nTotalNodes);
  globalToLocal = -1;
  PetscInt currentOwnedIdx = 0;
  PetscInt currentGhostIdx = m_nOwnedNodes;
  for (PetscInt i = 0; i < m_nTotalNodes; i++)
  {
    if (neededNodes(i) == 1)
    {
      if (nodePartition(i) == rank)
      {
        m_localToGlobalMap(currentOwnedIdx) = serialToParallel(i);
        globalToLocal(serialToParallel(i)) = currentOwnedIdx;
        currentOwnedIdx++;
      }
      else
      {
        m_localToGlobalMap(currentGhostIdx) = serialToParallel(i);
        globalToLocal(serialToParallel(i)) = currentGhostIdx;
        currentGhostIdx++;
      }
    }
  }


  // Find number of entries in each row of FEM matrix
  std::vector<std::vector<PetscInt> > connections(m_nOwnedNodes);
  for (int i = 0; i < m_nElements; i++)
  {
    for (int j = 0; j < m_enon; j++)
    {
      PetscInt n1 = m_connectivity(i, j);
      PetscInt n1Local = globalToLocal(serialToParallel(n1));
      if (nodePartition(n1) == rank)
      {
        for (int k = 0; k < m_enon; k++)
        {
          if (j != k)
          {
            PetscInt n2 = m_connectivity(i, k);
            bool isDuplicate = false;
            for (int m = 0; m < connections[n1Local].size(); m++)
            {
              if (n2 == connections[n1Local][m])
              {
                isDuplicate = true;
                break;
              }
            }
            if (isDuplicate == false)
            {
              connections[n1Local].push_back(n2);
            }
          }
        }
      }
    }
  }

  m_nFemDiagonalEntries.resize(m_nOwnedNodes);
  m_nFemOffDiagonalEntries.resize(m_nOwnedNodes);
  m_nFemDiagonalEntries = 1;  // Actual diagonal entry

  for (PetscInt i = 0; i < m_nOwnedNodes; i++)
  {
    for (int j = 0; j < connections[i].size(); j++)
    {
      if (nodePartition(connections[i][j]) == rank)
      {
        m_nFemDiagonalEntries(i)++;
      }
      else
      {
        m_nFemOffDiagonalEntries(i)++;
      }
    }
  }



  // Create new connectivity and connectivityGlobal
  Array2d<PetscInt> connectivityNew(nElementsNew, m_enon);
  m_connectivityGlobal.resize(nElementsNew, m_enon);
  PetscInt idx = 0;
  for (PetscInt i = 0; i < m_nElements; i++)
  {
    if (elementPartition(i) == rank)
    {
      for (PetscInt j = 0; j < m_enon; j++)
      {
        connectivityNew(idx, j) =
                         globalToLocal(serialToParallel(m_connectivity(i, j)));
        m_connectivityGlobal(idx, j) = serialToParallel(m_connectivity(i, j));
      }
      idx++;
    }
  }

  // Create new coordinates
  Array1d<Vector> coordinatesNew(nTotalNodesNew);
  for (PetscInt i = 0; i < nTotalNodesNew; i++)
  {
    if (parallelToSerial(m_localToGlobalMap(i)) < m_nCornerNodes)
    {
      coordinatesNew(i) = m_coordinates(parallelToSerial(m_localToGlobalMap(i)));
    }
    else
    {
      PetscInt order2idx = parallelToSerial(m_localToGlobalMap(i))
                           - m_nCornerNodes;
      PetscInt neighbor1 = m_order2nodeNeighbors(order2idx, 0);
      PetscInt neighbor2 = m_order2nodeNeighbors(order2idx, 1);
      coordinatesNew(i) = (m_coordinates(neighbor1)
                           + m_coordinates(neighbor2)) * .5;
    }
  }


  // Create new order2nodeNeighbors
  currentOwnedIdx = 0;
  currentGhostIdx = m_nOwnedNodes - m_nOwnedCornerNodes;
  PetscInt idx1;
  Array2d<PetscInt> order2nodeNeighborsNew(nTotalNodesNew - nCornerNodesNew, 3);
  for (PetscInt i = m_nCornerNodes; i < m_nTotalNodes; i++)
  {
    if (neededNodes(i) == 1)
    {
      if (nodePartition(i) == rank)
      {
        idx1 = currentOwnedIdx;
        currentOwnedIdx++;
      }
      else
      {
        idx1 = currentGhostIdx;
        currentGhostIdx++;
      }

      for (PetscInt j = 0; j < 2; j++)
      {
        order2nodeNeighborsNew(idx1, j) =
          globalToLocal(serialToParallel(
                                m_order2nodeNeighbors(i - m_nCornerNodes, j)));
      }
      order2nodeNeighborsNew(idx1, 2) =
          globalToLocal(serialToParallel(
                                m_order2nodeNeighbors(i - m_nCornerNodes, 2)));
    }
  }

  // Create new parallelToSerialMap
  m_parallelToSerialMap.resize(nTotalNodesNew);
  for (PetscInt i = 0; i < nTotalNodesNew; i++)
  {
    m_parallelToSerialMap(i) = parallelToSerial(m_localToGlobalMap(i));
  }

  // Copy everything
  // connectivity
  m_connectivity.resize(nElementsNew, m_enon);
  for (PetscInt i = 0; i < nElementsNew; i++)
  {
    for (PetscInt j = 0; j < m_enon; j++)
    {
      m_connectivity(i, j) = connectivityNew(i, j);
    }
  }
  // coordinates
  m_coordinates.resize(nTotalNodesNew);
  for (PetscInt i = 0; i < nTotalNodesNew; i++)
  {
    m_coordinates(i) = coordinatesNew(i);
  }
  // order2nodeNeighbors
  m_order2nodeNeighbors.resize(nTotalNodesNew - nCornerNodesNew, 3);
  for (PetscInt i = 0; i < nTotalNodesNew - nCornerNodesNew; i++)
  {
    for (PetscInt j = 0; j < 2; j++)
    {
      m_order2nodeNeighbors(i, j) = order2nodeNeighborsNew(i, j);
    }
    m_order2nodeNeighbors(i, 2) = order2nodeNeighborsNew(i, 2);
  }


  // Update variables
  m_nElements = nElementsNew;
  m_nCornerNodes = nCornerNodesNew;
  m_nTotalNodes = nTotalNodesNew;

}


void Mesh::printOrder2Connectivity() const
{
  for (PetscInt i = 0; i < m_nElements; i++)
  {
    for (int j = 0; j < m_enon; j++)
    {
      std::cout << m_connectivity(i,j) << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

void Mesh::printConnectivity() const
{
  for (PetscInt i = 0; i < m_nElements; i++)
  {
    for (int j = 0; j < m_order1Enon; j++)
    {
      std::cout << m_connectivity(i,j) << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

void Mesh::printCoordinates() const
{
  for (PetscInt i = 0; i < m_nCornerNodes; i++)
  {
    for (int j = 0; j < DIM; j++)
    {
      std::cout << m_coordinates(i)[j] << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

void Mesh::make2ndOrder()
{
  // Perturbations and inverse are based on basis order
  int perturbations2d[3][2] = {{0, 1}, {1, 2}, {0, 2}};
  int inverse2d[3][3] = {{-1, 0, 2}, {0, -1, 1}, {2, 1, -1}};
  int perturbations3d[6][2] = {{0, 1}, {1, 2}, {0, 2},
                             {0, 3}, {1, 3}, {2, 3}};
  int inverse3d[4][4] = {{-1, 0, 2, 3},
                       {0, -1, 1, 4},
                       {2, 1, -1, 5},
                       {3, 4, 5, -1}};
  if (DIM == 1)
  {
    m_enon = 3;
  }
  else if (DIM == 2)
  {
    m_enon = 6;
  }
  else if (DIM == 3)
  {
    m_enon = 10;
  }

  Array2d<PetscInt> oldConnectivity = m_connectivity;
  m_connectivity.resize(m_nElements, m_enon);

  // Copy connectivity back
  for (PetscInt i = 0; i < m_nElements; i++)
  {
    for (int j = 0; j < m_order1Enon; j++)
    {
      m_connectivity(i,j) = oldConnectivity(i,j);
    }
  }

  m_nTotalNodes = m_nCornerNodes;

  // Set order 2 connectivity to -1 (kind of wonky with uint, but should work)
  for (PetscInt i = 0; i < m_nElements; i++)
  {
    for (int j = m_order1Enon; j < m_enon; j++)
    {
      m_connectivity(i,j) = -1;
    }
  }

  std::vector<std::vector<PetscInt> > neighbors;

  Array1d<PetscInt> elsPerNode;
  Array2d<PetscInt> mapping;
  nodeToElementMapping(elsPerNode, mapping);

  for (PetscInt i = 0; i < m_nElements; i++)
  {
    for (int j = m_order1Enon; j < m_enon; j++)
    {
      if (m_connectivity(i,j) == -1)  // Node not assigned yet
      {
        m_nTotalNodes++;
        m_connectivity(i,j) = m_nTotalNodes - 1;
        PetscInt node1, node2;
        // new node is on edge between node 1 and node 2
        if (DIM == 1)
        {
          node1 = m_connectivity(i, 0);
          node2 = m_connectivity(i, 1);
        }
        if (DIM == 2)
        {
          node1 = m_connectivity(i, perturbations2d[j - m_order1Enon][0]);
          node2 = m_connectivity(i, perturbations2d[j - m_order1Enon][1]);
        }
        else if (DIM == 3)
        {
          node1 = m_connectivity(i, perturbations3d[j - m_order1Enon][0]);
          node2 = m_connectivity(i, perturbations3d[j - m_order1Enon][1]);
        }
        std::vector<PetscInt> newNeighbor(3);
        newNeighbor[0] = node1;
        newNeighbor[1] = node2;
        newNeighbor[2] = m_nTotalNodes - 1;
        neighbors.push_back(newNeighbor);

        if (DIM > 1)
        {
          // Find repeated nodes
          for (PetscInt a = 0; a < elsPerNode(node1); a++)
          {
            PetscInt k = mapping(node1, a);
            int newIndex1 = -1;
            int newIndex2 = -1;
            for (int m = 0; m < m_order1Enon; m++)
            {
              if (m_connectivity(k,m) == node1)
              {
                newIndex1 = m;
              }
              if (m_connectivity(k,m) == node2)
              {
                newIndex2 = m;
              }
            }
            if (newIndex1 > -1 && newIndex2 > -1)
            {
              if (DIM == 2)
              {
                m_connectivity(k,m_order1Enon + inverse2d[newIndex1][newIndex2]) =
                                                               m_nTotalNodes - 1;
              }
              else if (DIM == 3)
              {
                m_connectivity(k,m_order1Enon + inverse3d[newIndex1][newIndex2]) =
                                                               m_nTotalNodes - 1;
              }
            }
          }
        }
      }
    }
  }

  m_order2nodeNeighbors.resize(m_nTotalNodes - m_nCornerNodes, 3);
  for (PetscInt i = 0; i < m_nTotalNodes - m_nCornerNodes; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      m_order2nodeNeighbors(i, j) = neighbors[i][j];
    }
    m_order2nodeNeighbors(i, 2) = i + m_nCornerNodes;
  }
}


void Mesh::calculateJacobian()
{
  m_jacobian.resize(m_nElements);

  for (int i = 0; i < m_nElements; i++)
  {
    if (DIM == 1)
    {
      m_jacobian(i) = .5 * getLength(m_coordinates(m_connectivity(i,0)),
                                     m_coordinates(m_connectivity(i,1)));
      if (m_jacobian(i) < 0.)
      {
        std::cout << "Mesh has element with negative Jacobian. Aborting."
                  << std::endl;
        MPI_Abort(PETSC_COMM_WORLD, 1);
      }
    }
    if (DIM == 2)
    {
      m_jacobian(i) = getArea(m_coordinates(m_connectivity(i,0)),
                              m_coordinates(m_connectivity(i,1)),
                              m_coordinates(m_connectivity(i,2)));
      if (m_jacobian(i) < 0.)
      {
        std::cout << "Mesh has element with negative Jacobian. Aborting."
                  << std::endl;
        MPI_Abort(PETSC_COMM_WORLD, 1);
      }
    }
    if (DIM == 3)
    {
      m_jacobian(i) = getVolume(m_coordinates(m_connectivity(i,0)),
                                m_coordinates(m_connectivity(i,1)),
                                m_coordinates(m_connectivity(i,2)),
                                m_coordinates(m_connectivity(i,3)));
      if (m_jacobian(i) < 0.)
      {
        std::cout << "Mesh has element with negative Jacobian. Aborting."
                  << std::endl;
        MPI_Abort(PETSC_COMM_WORLD, 1);
      }
    }
  }
}


void Mesh::computeMetricTensor()
{
  m_metricTensor.resize(m_nElements, DIM, DIM);
  m_metricTensor = 0.;

  Array2d<PetscReal> J(DIM, DIM);
  Array2d<PetscReal> Jinv(DIM, DIM);
  for (PetscInt el = 0; el < m_nElements; el++)
  {
    J = 0.;
    Jinv = 0.;

    // Calculate J (and zero out
    for (PetscInt i = 0; i < DIM; i++)
    {
      for (PetscInt j = 0; j < DIM; j++)
      {
        J(i, j) = m_coordinates(m_connectivity(el, j + 1))[i]
                  - m_coordinates(m_connectivity(el, 0))[i];
      }
    }


    // Calculate Jinv
    PetscReal det = 0.;

    if (DIM == 1)
    {
      det = J(0, 0);
      Jinv(0, 0) = 1.;
    }
    else if (DIM == 2)
    {
      det = J(0, 0) * J(1, 1) - J(0, 1) * J(1, 0);
      Jinv(0, 0) = J(1, 1);
      Jinv(0, 1) = -J(0, 1);
      Jinv(1, 0) = -J(1, 0);
      Jinv(1, 1) = J(0, 0);
    }
    else
    {
      det = J(0, 0) * (J(1, 1) * J(2, 2) - J(1, 2) * J(2, 1))
            - J(0, 1) * (J(1, 0) * J(2, 2) - J(1, 2) * J(2, 0))
            + J(0, 2) * (J(1, 0) * J(2, 1) - J(1, 1) * J(2, 1));

      Jinv(0, 0) = J(1, 1) * J(2, 2) - J(1, 2) * J(2, 1);
      Jinv(0, 1) = J(0, 2) * J(2, 1) - J(0, 1) * J(2, 2);
      Jinv(0, 2) = J(0, 1) * J(1, 2) - J(0, 2) * J(1, 1);
      Jinv(1, 0) = J(1, 2) * J(2, 0) - J(1, 0) * J(2, 2);
      Jinv(1, 1) = J(0, 0) * J(2, 2) - J(0, 2) * J(2, 0);
      Jinv(1, 2) = J(0, 2) * J(1, 0) - J(0, 0) * J(1, 2);
      Jinv(2, 0) = J(1, 0) * J(2, 1) - J(1, 1) * J(2, 0);
      Jinv(2, 1) = J(0, 1) * J(2, 0) - J(0, 0) * J(2, 1);
      Jinv(2, 2) = J(0, 0) * J(1, 1) - J(0, 1) * J(1, 0);
    }

    // Divide by determinant as well as check that determinant is not near 0
    PetscReal eps = 1e-12;
    for (PetscInt i = 0; i < DIM; i++)
    {
      for (PetscInt j = 0; j < DIM; j++)
      {
        if (std::abs(det / Jinv(i, j)) < eps)
        {
          std::cout << "Error: element " << el << " Jacobian determinant of "
                    << std::abs(det / Jinv(i, j))
                    << " is less than threshold of " << eps << std::endl;
          for (int k = 0; k < DIM + 1; k++)
          {
            std::cout << "Node "
                      << m_parallelToSerialMap(m_connectivity(el, k)) << ": ";
            for (int l = 0; l < DIM; l++)
            {
              std::cout << m_coordinates(m_connectivity(el, k))[l] << " ";
            }
            std::cout << std::endl;
          }
          MPI_Abort(PETSC_COMM_WORLD, 1);
        }
        else
        {
          Jinv(i, j) /= std::abs(det);
        }
      }
    }


    // Calculate Ginv
    for (PetscInt i = 0; i < DIM; i++)
    {
      for (PetscInt j = 0; j < DIM; j++)
      {
        for (PetscInt k = 0; k < DIM; k++)
        {
          m_metricTensor(el, i, j) += Jinv(k, i) * Jinv(k, j);
        }
      }
    }

  }
}

PetscReal getVolume(const Vector & p1,
                    const Vector & p2,
                    const Vector & p3,
                    const Vector & p4)
{
  if (DIM == 3)
  {
    return (p4 - p1).dot((p2 - p4).cross(p3 - p4)) / 6.;
  }
  else
  {
    std::cout << "Volume invalid in " << DIM << "D." << std::endl;
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
}

PetscReal getArea(const Vector & p1,
                  const Vector & p2,
                  const Vector & p3)
{
  if (DIM == 3)
  {
    return .5 * ((p2 - p1).cross(p1 - p3)).magnitude();
  }
  else if (DIM == 2)
  {
    return .5 * (-p2[0] * p1[1] + p3[0] * p1[1] + p1[0] * p2[1]
                 - p3[0] * p2[1] - p1[0] * p3[1] + p2[0] * p3[1]);
  }
  else
  {
    std::cout << "Area invalid in " << DIM << "D." << std::endl;
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
}

PetscReal getLength(const Vector & p1,
                    const Vector & p2)
{
  if (DIM == 2 || DIM == 3)
  {
    return (p2 - p1).magnitude();
  }
  else if (DIM == 1)
  {
    return p2[0] - p1[0];
  }
  else
  {
    std::cout << "Length invalid in " << DIM << "D." << std::endl;
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
}


void Mesh::nodeToElementMapping(Array1d<PetscInt> &elsPerNode,
                                Array2d<PetscInt> &mapping)
{
  elsPerNode.resize(m_nCornerNodes);
  for (int i = 0; i < m_nElements; i++)
  {
    for (int j = 0; j < m_order1Enon; j++)
    {
      elsPerNode(m_connectivity(i, j))++;
    }
  }

  int maxNumberOfConnections = 0;
  Array1d<PetscInt> currentIdx(m_nCornerNodes);
  for (int i = 0; i < m_nCornerNodes; i++)
  {
    currentIdx(i) = 0;
    if (elsPerNode(i) > maxNumberOfConnections)
    {
      maxNumberOfConnections = elsPerNode(i);
    }
  }
  mapping.resize(m_nCornerNodes, maxNumberOfConnections);
  for (int i = 0; i < m_nElements; i++)
  {
    for (int j = 0; j < m_order1Enon; j++)
    {
      PetscInt n = m_connectivity(i, j);
      mapping(n, currentIdx(n)++) = i;
    }
  }
}

void Mesh::writeMappingToFile(std::string filenameRoot,
                              int rank,
                              bool order1Only)
{
  std::stringstream ss;
  ss << rank;

  std::string filename = filenameRoot + "." + ss.str();

  uint32_t nEntries;
  if (order1Only)
  {
    nEntries = m_nOwnedCornerNodes;
  }
  else
  {
    nEntries = m_nOwnedNodes;
  }

  // Calculate mapping
  Array1d<uint32_t> mapping(nEntries);
  for (PetscInt i = 0; i < nEntries; i++)
  {
    mapping(i) = m_parallelToSerialMap(i);
  }

  std::fstream fout;
  fout.open(filename.c_str(), std::ios::out | std::ios::binary);
  fout.write((char *) &nEntries, sizeof(uint32_t));
  fout.write((char *) mapping.front(), nEntries * sizeof(uint32_t));
  fout.close();
}
