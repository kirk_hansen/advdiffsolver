#ifndef REACTION_SOLVER_HPP
#define REACTION_SOLVER_HPP

#include <vector>
#include "petscksp.h"
#include "reaction_system.hpp"

class ReactionSolver
{
public:
  ReactionSolver(std::vector<Vec> &solution);

  ~ReactionSolver();

  /** Allocates m_stage1 and m_stage2. Must be called after allocating
   *  m_solution but before using the ReactionSolver. */
  void initialize(std::vector<ReactionEquation> &system);

  /** Updates m_solution using 2nd order Runge-Kutta scheme. */
  void computeReactions(PetscReal dt);

  /** ReactionSystem associated with this solver. */
  std::vector<ReactionEquation> *m_system;

  /** Vector of concentrations for each species. */
  std::vector<Vec> &m_solution;

private:
  /** These temporary vectors are pre-allocated and stored premanently for
   *  efficiency.
   */
  std::vector<Vec> m_stage1, m_stage2;

  /** Whether or not ReactionSolver is initialized. */
  bool m_initialized;
};

#endif
