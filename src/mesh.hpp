#ifndef MESH_HPP
#define MESH_HPP

#include <string>
#include <vector>
#include "petscsys.h"

#include "vectorND.hpp"

#ifndef DIM
#define DIM 3
#endif


// Utilities

// Computes volume of tetrahedron with these 4 corners
// Signed according to sign of ((p2-p1)x(p3-p1)).p4
PetscReal getVolume(const Vector & p1,
                    const Vector & p2,
                    const Vector & p3,
                    const Vector & p4);

// Computes area of triangle with these 3 corners
// Signed for 2D, unsigned for 3D
// Sign is positive if counterclockwise, negative if clockwise
PetscReal getArea(const Vector & p1,
                  const Vector & p2,
                  const Vector & p3);

// Computes length of line with these 2 corners
// Signed for 1D, unsigned for 2D and 3D
// Sign is positive if p2 > p1
PetscReal getLength(const Vector & p1,
                    const Vector & p2);


class Mesh
{
public:
  Mesh();

  Mesh(std::string connectivityFilename,
       std::string coordinatesFilename,
       int order);

  /** Completely initializes parallel mesh. */
  void initialize(std::string connectivityFilename,
                  std::string coordinatesFilename,
                  int order);


  void printConnectivity() const;

  void printOrder2Connectivity() const;

  void printCoordinates() const;

  /** TODO */
  void writeMeshToFile(std::string filename);

  /** Size: m_nElements x m_enon
    * m_connectivity(i, j) contains local index of jth node in local element i.
    * Corner nodes are listed before order 2 nodes for each element. Corner
    * nodes are listed left to right in 1D, counterclockwise in 2D, and
    * according to the right-hand rule in 3D. 2nd order nodes are listed in the
    * following order, where e.g. 13 is the node between corner nodes 1 and 3:
    *   2D: 01 12 02
    *   3D: 01 12 02 03 13 23 */
  Array2d<PetscInt> m_connectivity;

  /** Size: m_nElements x m_enon
    * m_connectivityGlobal(i, j) contains global index of jth node in element
    * i. Follows same order as m_connectivity. */
  Array2d<PetscInt> m_connectivityGlobal;

  /** Size: m_nTotalNodes x DIM
    * m_coordinates(i)[j] contains jth coordinate of local node i. Owned corner
    * nodes are listed first, followed by owned order 2 nodes, ghost corner
    * nodes, and ghost order 2 nodes respectively. */
  Array1d<Vector> m_coordinates;

  /** Size: (m_nTotalNodes - m_nCornerNodes) x 3
    * m_order2nodeNeighbors(i, j) contains local index of jth adjacent corner
    * node for the ith order 2 (edge) node for j = 0, 1.
    * m_order2nodeNeighbors(i,2) contains the index of the ith order 2 node */
  Array2d<PetscInt> m_order2nodeNeighbors;

  // /** Size: m_nOwnedNodes
  //   * m_nfemRowDiagonalSize(i) contains the number of local nodes (including i)
  //   * sharing an element with local node i, i.e. the number of "diagonal"
  //   * entries in row i in an FEM matrix. m_femRowOffDiagonalSize(i) contains
  //   * the number of nodes sharing an element with local node i which are owned
  //   * by other processes (i.e. the number of "off-diagonal" entries in row i of
  //   * an FEM matrix. */
  // Array1d<PetscInt> m_femRowDiagonalSize;
  // Array1d<PetscInt> m_femRowOffDiagonalSize;

  /** Size: m_nTotalNodes
    * m_parallelToSerialMap(i) contains the original global index (before
    * partitioning) of local node i. */
  Array1d<PetscInt> m_parallelToSerialMap;

  /** Size: m_nTotalNodes
    * m_localToGlobalMap(i) contains the global index of local node i. */
  Array1d<PetscInt> m_localToGlobalMap;

  /** Size: m_nTotalNodes
    * m_jacobian(i) contains the Jacobian determinant for local element i. */
  Array1d<PetscReal> m_jacobian;

  /** Size: m_nElements * DIM * DIM
    * m_metricTensor(i, j, k) contains entry(j, k) of the metric tensor G
    * defined on page 602 of Bazilevs et al. (2007), YZB discontinuity
    * capturing for advection-dominated processes with application to arterial
    * drug delivery. */
  Array3d<PetscReal> m_metricTensor;

  /** Size: m_nOwnedNodes */
  Array1d<PetscInt> m_nFemDiagonalEntries;

  Array1d<PetscInt> m_nFemOffDiagonalEntries;


  PetscInt m_nElements; ///< Number of elements (local).
  PetscInt m_nCornerNodes; ///< Number of corner (geometric) nodes (local).
  PetscInt m_nTotalNodes; ///< Total number of nodes (local).
  PetscInt m_nOwnedNodes; ///< Total number of nodes owned by this process.
  PetscInt m_nOwnedCornerNodes; ///< Total number of owned corner nodes.
  PetscInt m_nCornerNodesGlobal; ///< Number of corner nodes in global mesh.
  PetscInt m_nTotalNodesGlobal; ///< Total number of nodes in global mesh.
  PetscInt m_globalStartIndex; ///< First row owned by this process
  int m_order1Enon; ///< Number of corner (order 1) nodes per element.
  int m_enon; ///< Total number of nodes per element.

  /** Write parallel to serial mapping to file. */
  void writeMappingToFile(std::string filenameRoot,
                          int rank,
                          bool order1Only=true);

protected:
  /** Loads connectivity and coordinates from binary files. Connectivity file
    * should be a 32-bit int (nElements) followed by nElements*order1Enon
    * 32-bit ints containing node indices for each element. Coordinates file
    * should be a 32-bit int (nNodes) followed by nNodes*DIM doubles containing
    * x (y (z)) coordinates for each node. */
  void loadFromFile(std::string connectivityFilename,
                    std::string coordinatesFilename);

  /** Makes mesh 2nd order by adding a new node at the midpoint of each mesh
    * edge. Changes m_connectivity (putting new nodes at the end of each
    * element's connectivity) and m_coordinates (adding new nodes at end)
    * accordingly. Also populates m_order2neighbors and changes m_nTotalNodes
    * and m_enon. */
  void make2ndOrder();

  /** Calculates Jacobian determinant for each element and stores in
    * m_jacobian. */
  void calculateJacobian();

  /** Computes the metric tensor for the mapping between the parent element and
    * the physical domain. Needed to apply SUPG stabilization in Bazilevs et
    * al. 2007. */
  void computeMetricTensor();

  /** Partitions mesh using METIS. Options for partitionType are "dual" and
    * "nodal" */
  void partition(Array1d<PetscInt> &elementPartition,
                 Array1d<PetscInt> &nodePartition,
                 std::string partitionType="dual");

  /** Converts all mesh data from global mesh to local mesh on each process.
    * Also populates m_parallelToSerialMap and m_localToGlobalMap. */
  void convertGlobalMeshToLocal(const Array1d<PetscInt> &elementPartition,
                                const Array1d<PetscInt> &nodePartition,
                                const PetscMPIInt size,
                                const PetscMPIInt rank);

  /** Helper fcn for make2ndOrder()
    * elsPerNode(i): the number of elements node i is a part of
    * mapping(i,j): the jth element containing node i
    * This function resizes elsPerNode and mapping. */
  void nodeToElementMapping(Array1d<PetscInt> &elsPerNode,
                            Array2d<PetscInt> &mapping);
};

#endif
