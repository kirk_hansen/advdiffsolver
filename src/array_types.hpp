#ifndef ARRAY_TYPES_HPP
#define ARRAY_TYPES_HPP

#include <iostream>
#include <vector>
#include <string>
#include <fstream>

#include "petscsys.h"

// typedef PetscReal Real;


template <typename T>
class Array1d
{
public:
  Array1d() : m_size(0) {}

  void resize(PetscInt newSize)
  {
    if (m_size != newSize)
    {
      m_data.resize(newSize);
      m_size = newSize;
    }
  }

  Array1d(const Array1d<T> &rhs) : m_size(0)
  {
    if (this != &rhs)
    {
      resize(rhs.m_size);
      for (PetscInt i = 0; i < m_size; i++)
      {
        m_data[i] = rhs.m_data[i];
      }
    }
  }

  Array1d<T> & operator=(const T &rhs)
  {
    for (PetscInt i = 0; i < m_size; i++)
    {
      m_data[i] = rhs;
    }
    return *this;
  }

  Array1d<T> & operator*=(const T &rhs)
  {
    for (PetscInt i = 0; i < m_size; i++)
    {
      m_data[i] *= rhs;
    }
    return *this;
  }

  Array1d<T> & operator/=(const T &rhs)
  {
    for (PetscInt i = 0; i < m_size; i++)
    {
      m_data[i] /= rhs;
    }
    return *this;
  }

  Array1d<T> & operator+=(const T &rhs)
  {
    for (PetscInt i = 0; i < m_size; i++)
    {
      m_data[i] += rhs;
    }
    return *this;
  }

  Array1d<T> & operator-=(const T &rhs)
  {
    for (PetscInt i = 0; i < m_size; i++)
    {
      m_data[i] -= rhs;
    }
    return *this;
  }

  Array1d<T> & operator*=(const Array1d<T> &rhs)
  {
    for (PetscInt i = 0; i < m_size; i++)
    {
      m_data[i] *= rhs.m_data[i];
    }
    return *this;
  }

  Array1d<T> & operator/=(const Array1d<T> &rhs)
  {
    for (PetscInt i = 0; i < m_size; i++)
    {
      m_data[i] /= rhs.m_data[i];
    }
    return *this;
  }

  Array1d<T> & operator+=(const Array1d<T> &rhs)
  {
    for (PetscInt i = 0; i < m_size; i++)
    {
      m_data[i] += rhs.m_data[i];
    }
    return *this;
  }

  Array1d<T> & operator-=(const Array1d<T> &rhs)
  {
    for (PetscInt i = 0; i < m_size; i++)
    {
      m_data[i] -= rhs.m_data[i];
    }
    return *this;
  }

  const Array1d<T> operator*(const Array1d<T> &rhs)
  {
    Array1d<T> result = *this;
    result *= rhs;
    return result;
  }

  const Array1d<T> operator/(const Array1d<T> &rhs)
  {
    Array1d<T> result = *this;
    result /= rhs;
    return result;
  }

  const Array1d<T> operator+(const Array1d<T> &rhs)
  {
    Array1d<T> result = *this;
    result += rhs;
    return result;
  }

  const Array1d<T> operator-(const Array1d<T> &rhs)
  {
    Array1d<T> result = *this;
    result -= rhs;
    return result;
  }

  Array1d<T> & operator=(const Array1d<T> &rhs)
  {
    if (this != &rhs)
    {
      resize(rhs.m_size);
      for (PetscInt i = 0; i < m_size; i++)
      {
        m_data[i] = rhs.m_data[i];
      }
    }
    return *this;
  }

  Array1d(PetscInt dim1)
  {
    m_size = dim1;
    m_data.resize(dim1);
  }

  T& operator()(PetscInt index)
  {
    return m_data[index];
  }

  const T& operator()(PetscInt index) const
  {
    return m_data[index];
  }

  const T* front() const
  {
    return m_data.data();
  }

  T* front()
  {
    return m_data.data();
  }

  void swap(Array1d<T> rhs)
  {
    m_data.swap(rhs.m_data);
  }

  // void writeToFile(std::string filename, std::string type)
  // {
  //   std::fstream fout;
  //   fout.open(filename, std::ios::out | std::ios::binary);
  //   uint32_t size = m_size;
  //   fout.write((char *) &size, sizeof(uint32_t));
  //   if (type == "integer")
  //   {
  //     std::vector<uint32_t> data(size);
  //     for (int i = 0; i < size; i++)
  //     {
  //       data[i] = m_data[i];
  //     }
  //     fout.write((char *) &data[0], size * sizeof(uint32_t));
  //   }
  //   else if (type == "floating point")
  //   {
  //     std::vector<double> data(size);
  //     for (int i = 0; i < size; i++)
  //     {
  //       data[i] = m_data[i];
  //     }
  //     fout.write((char *) &data[0], size * sizeof(double));
  //   }
  //   fout.close();
  // }

  PetscInt m_size;

protected:
  std::vector<T> m_data;
};


template <typename T>
class Array2d : public Array1d<T>
{
public:
  Array2d() : m_dim1(0), m_dim2(0) {}

  void resize(PetscInt dim1, PetscInt dim2)
  {
    m_dim1 = dim1;
    m_dim2 = dim2;
    this->m_size = dim1 * dim2;
    this->m_data.resize(this->m_size);
  }

  Array2d(const Array2d<T> &rhs)
  {
    if (this != &rhs)
    {
      resize(rhs.m_dim1, rhs.m_dim2);
      for (PetscInt i = 0; i < this->m_size; i++)
      {
        this->m_data[i] = rhs.m_data[i];
      }
    }
  }

  Array2d(PetscInt dim1, PetscInt dim2)
  {
    m_dim1 = dim1;
    m_dim2 = dim2;
    this->m_size = dim1 * dim2;
    this->m_data.resize(this->m_size);
  }

  Array2d<T> & operator=(const Array2d<T> &rhs)
  {
    if (this != &rhs)
    {
      resize(rhs.m_dim1, rhs.m_dim2);
      for (PetscInt i = 0; i < this->m_size; i++)
      {
        this->m_data[i] = rhs.m_data[i];
      }
    }
    return *this;
  }

  Array2d<T> & operator=(const T &rhs)
  {
    for (PetscInt i = 0; i < this->m_size; i++)
    {
      this->m_data[i] = rhs;
    }
    return *this;
  }

  T& operator()(PetscInt index1, PetscInt index2)
  {
    return this->m_data[index1 * m_dim2 + index2];
  }

  const T& operator()(PetscInt index1, PetscInt index2) const
  {
    return this->m_data[index1 * m_dim2 + index2];
  }

  PetscInt m_dim1;
  PetscInt m_dim2;
};


template <typename T>
class Array3d : public Array2d<T>
{
public:
  Array3d() : m_dim3(0) {}

  void resize(PetscInt dim1, PetscInt dim2, PetscInt dim3)
  {
    this->m_size = dim1 * dim2 * dim3;
    this->m_data.resize(this->m_size);
    this->m_dim1 = dim1;
    this->m_dim2 = dim2;
    this->m_dim3 = dim3;
  }

  Array3d(const Array3d<T> &rhs)
  {
    if (this != &rhs)
    {
      resize(rhs.m_dim1, rhs.m_dim2, rhs.m_dim3);
      for (PetscInt i = 0; i < this->m_size; i++)
      {
        this->m_data[i] = rhs.m_data[i];
      }
    }
  }

  Array3d(PetscInt dim1, PetscInt dim2, PetscInt dim3)
  {
    this->m_dim1 = dim1;
    this->m_dim2 = dim2;
    m_dim3 = dim3;
    this->m_size = dim1 * dim2 * dim3;
    this->m_data.resize(this->m_size);
  }

  Array3d<T> & operator=(const Array3d<T> &rhs)
  {
    if (this != &rhs)
    {
      resize(rhs.m_dim1, rhs.m_dim2, rhs.m_dim3);
      for (PetscInt i = 0; i < this->m_size; i++)
      {
        this->m_data[i] = rhs.m_data[i];
      }
    }
    return *this;
  }

  Array3d<T> & operator=(const T &rhs)
  {
    for (PetscInt i = 0; i < this->m_size; i++)
    {
      this->m_data[i] = rhs;
    }
    return *this;
  }

  T& operator()(PetscInt index1, PetscInt index2, PetscInt index3)
  {
    return
      this->m_data[index1 * this->m_dim2 * m_dim3 + index2 * m_dim3 + index3];
  }

  const T& operator()(PetscInt index1, PetscInt index2, PetscInt index3) const
  {
    return
      this->m_data[index1 * this->m_dim2 * m_dim3 + index2 * m_dim3 + index3];
  }

  PetscInt m_dim3;
};

#endif
