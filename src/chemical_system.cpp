#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include "chemical_system.hpp"


ChemicalSystem::ChemicalSystem(std::string filename)
{
  loadFromFile(filename);
}


void ChemicalSystem::initSingleSpecies()
{
  m_nSpecies = 1;

  m_speciesNames.resize(m_nSpecies);
  m_volumeReactions.resize(m_nSpecies);
  m_surfaceReactions.resize(m_nSpecies);
  m_initialConditions.resize(m_nSpecies);
  m_diffusivities.resize(m_nSpecies);
  m_isMobile.resize(m_nSpecies);
}

void ChemicalSystem::loadFromFile(std::string filename)
{
  PetscMPIInt size;
  PetscMPIInt rank;
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

  std::ifstream fin(filename.c_str());
  std::string word;

  // First line is number of species
  fin >> word;
  int nSpecies = std::atoi(word.c_str());
  m_nSpecies = nSpecies;

  // First load all species names, ICs, and Ds
  m_speciesNames.resize(nSpecies);
  m_volumeReactions.resize(nSpecies);
  m_surfaceReactions.resize(nSpecies);
  m_initialConditions.resize(nSpecies);
  m_diffusivities.resize(nSpecies);
  m_isMobile.resize(nSpecies);
  int idx = 0;
  while (fin >> word)
  {
    if (word[0] == '!') // Species name
    {
      m_speciesNames[idx] = word.substr(1, word.length());
      fin >> word;
      m_isMobile[idx] = !(std::atoi(word.c_str()) == 0);
      fin >> word;
      m_initialConditions[idx] = std::atof(word.c_str());
      fin >> word;
      m_diffusivities[idx] = std::atof(word.c_str());
      idx++;
    }
  }
  if (idx != nSpecies)
  {
    if (rank == 0)
    {
      std::cout << "Number of species described in file: " << idx
                << " does not match number in file header: "
                << nSpecies << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }

  // Reset file
  fin.clear();
  fin.seekg(0, std::ios::beg);

  // Load volume reactions
  // First line is number of species
  fin >> word;

  // Load volume reactions
  for (int i = 0; i < nSpecies; i++)
  {
    // Load species name, isMobile, IC, and D into dummy variable
    fin >> word >> word >> word >> word;

    // Load number of reactions
    fin >> word;
    int nReactions = std::atoi(word.c_str());

    m_volumeReactions[i].m_reactions.resize(nReactions);

    int nSpecies = 0;
    int nReactionRates = 0;
    for (int j = 0; j < nReactions; j++)
    {
      ReactionTerm &currentReaction =
                       m_volumeReactions[i].m_reactions[j];

      // Load reaction type
      fin >> word;
      currentReaction.m_reactionType = std::atoi(word.c_str());

      if (currentReaction.m_reactionType >= 0
          && currentReaction.m_reactionType < 3)
      {
        nSpecies = currentReaction.m_reactionType; // Only true for 0-2
        nReactionRates = 1;
      }
      else if (currentReaction.m_reactionType == 3)
      {
        nSpecies = 2; // Only true for 0-2
        nReactionRates = 2;
      }
      else if (currentReaction.m_reactionType == 4)
      {
        nSpecies = 3; // Only true for 0-2
        nReactionRates = 2;
      }
      else
      {
        if (rank == 0)
        {
          std::cout << "Reaction type " << currentReaction.m_reactionType
                    << " not implemented." << std::endl;
        }
        MPI_Abort(PETSC_COMM_WORLD, 1);
      }

      // Load reaction rates
      for (int k = 0; k < nReactionRates; k++)
      {
        fin >> word;
        currentReaction.m_rateConstant[0] = std::atof(word.c_str());
      }

      // Load species
      for (int k = 0; k < nSpecies; k++)
      {
        fin >> word;
        // Find species index
        bool speciesFound = false;
        for (int m = 0; m < m_speciesNames.size(); m++)
        {
          if (m_speciesNames[m] == word)
          {
            currentReaction.m_speciesIdx[k] = m;
            speciesFound = true;
            break;
          }
        }
        if (!speciesFound)
        {
          if (rank == 0)
          {
            std::cout << "Species " << word << " not found." << std::endl;
          }
          MPI_Abort(PETSC_COMM_WORLD, 1);
        }
      }
    }
  }

  // Load surface reactions
  while (fin >> word)
  {
    if (word[0] == '@') // Species name
    {
      std::string name = word.substr(1, word.length());
      int speciesIdx = -1;
      for (int i = 0; i < m_speciesNames.size(); i++)
      {
        if (m_speciesNames[i] == name)
        {
          speciesIdx = i;
          break;
        }
      }
      if (speciesIdx == -1)
      {
        if (rank == 0)
        {
          std::cout << "Species " << name << " not found." << std::endl;
        }
        MPI_Abort(PETSC_COMM_WORLD, 1);
      }


      // Load number of reactions
      fin >> word;
      int nReactions = std::atoi(word.c_str());

      m_surfaceReactions[speciesIdx].m_reactions.resize(nReactions);

      int nReactionRates = 0;
      int nSpecies = 0;

      for (int j = 0; j < nReactions; j++)
      {
        ReactionTerm &currentReaction =
                                 m_surfaceReactions[speciesIdx].m_reactions[j];

        // Load reaction type
        fin >> word;
        currentReaction.m_reactionType = std::atoi(word.c_str());

        if (currentReaction.m_reactionType >= 0
            && currentReaction.m_reactionType < 3)
        {
          nSpecies = currentReaction.m_reactionType; // Only true for 0-2
          nReactionRates = 1;
        }
        else if (currentReaction.m_reactionType == 3)
        {
          nSpecies = 2; // Only true for 0-2
          nReactionRates = 2;
        }
        else if (currentReaction.m_reactionType == 4)
        {
          nSpecies = 3; // Only true for 0-2
          nReactionRates = 2;
        }
        else
        {
          if (rank == 0)
          {
            std::cout << "Reaction type " << currentReaction.m_reactionType
                      << " not implemented." << std::endl;
          }
          MPI_Abort(PETSC_COMM_WORLD, 1);
        }

        // Load reaction rates
        for (int k = 0; k < nReactionRates; k++)
        {
          fin >> word;
          currentReaction.m_rateConstant[0] = std::atof(word.c_str());
        }

        // Load species
        for (int k = 0; k < nSpecies; k++)
        {
          fin >> word;
          // Find species index
          bool speciesFound = false;
          for (int m = 0; m < m_speciesNames.size(); m++)
          {
            if (m_speciesNames[m] == word)
            {
              currentReaction.m_speciesIdx[k] = m;
              speciesFound = true;
              break;
            }
          }
          if (!speciesFound)
          {
            if (rank == 0)
            {
              std::cout << "Species " << word << " not found." << std::endl;
            }
            MPI_Abort(PETSC_COMM_WORLD, 1);
          }
        }
      }
    }
  }

  fin.close();
}



void ChemicalSystem::outputSystem()
{
  PetscMPIInt size;
  PetscMPIInt rank;
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

  if (rank == 0)
  {
    std::cout << std::endl;

    for (int i = 0; i < m_initialConditions.size(); i++)
    {
      std::cout << "["<< m_speciesNames[i] << "]_0 = "
                << m_initialConditions[i] << std::endl;
    }

    std::cout << std::endl;

    for (int i = 0; i < m_diffusivities.size(); i++)
    {
      std::cout << "D_"<< m_speciesNames[i] << " = "
                << m_diffusivities[i] << std::endl;
    }

    std::cout << std::endl;

    for (int i = 0; i < m_volumeReactions.size(); i++)
    {
      // Output reaction
      // Only implemented for types 0-3 so far
      std::cout << "R_" << m_speciesNames[i] << " = ";

      int nReactions =
                   m_volumeReactions[i].m_reactions.size();
      if (nReactions == 0)
      {
        std::cout << "0.0";
      }
      else
      {
        for (int j = 0; j < nReactions; j++)
        {
          ReactionTerm &currentReaction =
                       m_volumeReactions[i].m_reactions[j];
          if (currentReaction.m_reactionType >= 0
              && currentReaction.m_reactionType < 3)
          {
            std::string plusSign;
            if (j == 0)
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                           " " : " -";
            }
            else
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                            " + " : " - ";
            }
            std::cout << plusSign
                      << std::abs(currentReaction.m_rateConstant[0]);
            for (int k = 0; k < currentReaction.m_reactionType; k++)
            {
              std::cout << "*["
                        << m_speciesNames[currentReaction.m_speciesIdx[k]]
                        << "]";
            }
          }
          else if (currentReaction.m_reactionType == 3)
          {
            std::string plusSign;
            if (j == 0)
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                           " " : " -";
            }
            else
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                            " + " : " - ";
            }
            std::cout << plusSign
                      << std::abs(currentReaction.m_rateConstant[0])
                      << "[*"
                      << m_speciesNames[currentReaction.m_speciesIdx[0]]
                      << "]*["
                      << m_speciesNames[currentReaction.m_speciesIdx[1]]
                      << "]/("
                      << currentReaction.m_rateConstant[0]
                      << " +["
                      << m_speciesNames[currentReaction.m_speciesIdx[2]]
                      << "])";
          }
          else if (currentReaction.m_reactionType == 4)
          {
            std::string plusSign;
            if (j == 0)
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                           " " : " -";
            }
            else
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                            " + " : " - ";
            }
            std::cout << plusSign
                      << std::abs(currentReaction.m_rateConstant[0])
                      << "[*"
                      << m_speciesNames[currentReaction.m_speciesIdx[0]]
                      << "]*["
                      << m_speciesNames[currentReaction.m_speciesIdx[1]]
                      << "]*["
                      << m_speciesNames[currentReaction.m_speciesIdx[2]]
                      << "]/("
                      << currentReaction.m_rateConstant[0]
                      << " +["
                      << m_speciesNames[currentReaction.m_speciesIdx[3]]
                      << "])";
          }
          else
          {
            std::cout << "Output not implemented for reaction type: "
                      << currentReaction.m_reactionType
                      << std::endl;
            MPI_Abort(PETSC_COMM_WORLD, 1);
          }
        }
        std::cout << std::endl;
      }
    }

    std::cout << std::endl;

    for (int i = 0; i < m_surfaceReactions.size(); i++)
    {
      // Output reaction
      // Only implemented for types 0-2 so far
      int nReactions = m_surfaceReactions[i].m_reactions.size();
      if (nReactions == 0)
      {
        // Do nothing
      }
      else
      {
        std::cout << "d[" << m_speciesNames[i] << "]/dn = ";

        for (int j = 0; j < nReactions; j++)
        {
          ReactionTerm &currentReaction = m_surfaceReactions[i].m_reactions[j];
          if (currentReaction.m_reactionType >= 0
              && currentReaction.m_reactionType < 3)
          {
            std::string plusSign;
            if (j == 0)
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                           " " : " -";
            }
            else
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                            " + " : " - ";
            }
            std::cout << plusSign
                      << std::abs(currentReaction.m_rateConstant[0]);
            for (int k = 0; k < currentReaction.m_reactionType; k++)
            {
              std::cout << "*["
                        << m_speciesNames[currentReaction.m_speciesIdx[k]]
                        << "]";
            }
          }
          else if (currentReaction.m_reactionType == 3)
          {
            std::string plusSign;
            if (j == 0)
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                           " " : " -";
            }
            else
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                            " + " : " - ";
            }
            std::cout << plusSign
                      << std::abs(currentReaction.m_rateConstant[0])
                      << "[*"
                      << m_speciesNames[currentReaction.m_speciesIdx[0]]
                      << "]*["
                      << m_speciesNames[currentReaction.m_speciesIdx[1]]
                      << "]/("
                      << currentReaction.m_rateConstant[0]
                      << " +["
                      << m_speciesNames[currentReaction.m_speciesIdx[2]]
                      << "])";
          }
          else if (currentReaction.m_reactionType == 4)
          {
            std::string plusSign;
            if (j == 0)
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                           " " : " -";
            }
            else
            {
              plusSign = (currentReaction.m_rateConstant[0] >= 0.) ?
                            " + " : " - ";
            }
            std::cout << plusSign
                      << std::abs(currentReaction.m_rateConstant[0])
                      << "[*"
                      << m_speciesNames[currentReaction.m_speciesIdx[0]]
                      << "]*["
                      << m_speciesNames[currentReaction.m_speciesIdx[1]]
                      << "]*["
                      << m_speciesNames[currentReaction.m_speciesIdx[2]]
                      << "]/("
                      << currentReaction.m_rateConstant[0]
                      << " +["
                      << m_speciesNames[currentReaction.m_speciesIdx[3]]
                      << "])";
          }
          else
          {
            std::cout << "Output not implemented for reaction type: "
                      << currentReaction.m_reactionType
                      << std::endl;
            MPI_Abort(PETSC_COMM_WORLD, 1);
          }
        }
        std::cout << std::endl;
      }
    }
  }
}
