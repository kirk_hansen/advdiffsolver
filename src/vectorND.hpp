#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <string>
#include "petscsys.h"

#include "array_types.hpp"

#ifndef DIM
#define DIM 3
#endif


class Vector
{
public:
  Vector& operator=(const Vector &rhs);

  PetscReal operator[](size_t index) const
  {return m_coords[index];}

  PetscReal & operator[](size_t index)
  {return m_coords[index];}

  const Vector operator-() const;

  Vector & operator+=(const Vector &rhs);

  Vector & operator-=(const Vector &rhs);

  const Vector operator+(const Vector &rhs) const;

  const Vector operator-(const Vector &rhs) const;

  const Vector operator*(PetscReal scalar) const;

  const PetscReal dot(const Vector &rhs) const;

  const Vector cross(const Vector &rhs) const;

  const PetscReal magnitude() const;

private:
  PetscReal m_coords[DIM];
};


#endif
