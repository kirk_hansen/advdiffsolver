#include <iostream>
#include "petscksp.h"
#include "reaction_solver.hpp"

ReactionSolver::ReactionSolver(std::vector<Vec> &solution) :
  m_solution(solution), m_initialized(false)
{
}

void ReactionSolver::initialize(std::vector<ReactionEquation> &system)
{
  m_system = &system;
  int nSpecies = m_solution.size();

  m_stage1.resize(nSpecies);
  m_stage2.resize(nSpecies);

  for (int i = 0; i < nSpecies; i++)
  {
    VecDuplicate(m_solution[i], &(m_stage1[i]));
    VecDuplicate(m_solution[i], &(m_stage2[i]));
  }
  m_initialized = true;
}

ReactionSolver::~ReactionSolver()
{
  for (int i = 0; i < m_stage1.size(); i++)
  {
    VecDestroy(&(m_stage1[i]));
  }
  for (int i = 0; i < m_stage2.size(); i++)
  {
    VecDestroy(&(m_stage2[i]));
  }
}


void ReactionSolver::computeReactions(PetscReal dt)
{

  if (!m_initialized)
  {
    PetscMPIInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
    if (rank == 0)
    {
      std::cout << "Must initialize ReactionSolver before use." << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  // Compute stage 1
  for (int i = 0; i < m_solution.size(); i++)
  {
    (*m_system)[i].computeRate(m_solution, m_stage1[i]);
    VecAYPX(m_stage1[i], .5 * dt, m_solution[i]);
  }
  // Compute stage 2
  for (int i = 0; i < m_solution.size(); i++)
  {
    (*m_system)[i].computeRate(m_stage1, m_stage2[i]);
  }

  // Update solution
  for (int i = 0; i < m_solution.size(); i++)
  {
    VecAXPY(m_solution[i], dt, m_stage2[i]);
  }
}


