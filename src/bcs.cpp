#ifndef DIM
#define DIM 3
#endif

#include <fstream>
#include <string>
#include <stdint.h>
#include <vector>

#include "petscksp.h"

#include "bcs.hpp"
#include "mesh.hpp"
#include "array_types.hpp"
#include "vectorND.hpp"
#include "basis.hpp"



void DirichletBC::loadNodes(std::string nodeListFilename)
{
  std::fstream fin;
  fin.open(nodeListFilename.c_str(), std::ios::in | std::ios::binary);
  // Expects nNodes and node IDs to be 32 bit (unsigned) integers
  uint32_t nNodes;
  fin.read((char *) &nNodes, sizeof(uint32_t));

  Array1d<uint32_t> tmpNodes(nNodes);
  fin.read((char*) tmpNodes.front(),
           nNodes * sizeof(uint32_t));
  fin.close();

  // Find nodes which belong to this process
  // This could maybe be improved if slow
  m_nCornerNodes = 0;

  for (PetscInt i = 0; i < nNodes; i++)
  {
    for (PetscInt j = 0; j < m_mesh->m_nTotalNodes; j++)
    {
      if (tmpNodes(i) == m_mesh->m_parallelToSerialMap(j))
      {
        m_nCornerNodes++;
        break;
      }
    }
  }
  m_nTotalNodes = m_nCornerNodes;

  m_nodeIds.resize(m_nCornerNodes);
  m_nodeIdsGlobal.resize(m_nCornerNodes);
  for (int i = 0; i < m_nSpecies; i++)
  {
    m_nodeValues[i].resize(m_nCornerNodes);
  }

  PetscInt idx = 0;
  for (PetscInt i = 0; i < nNodes; i++)
  {
    for (PetscInt j = 0; j < m_mesh->m_nTotalNodes; j++)
    {
      if (tmpNodes(i) == m_mesh->m_parallelToSerialMap(j))
      {
        m_nodeIds(idx) = j;
        m_nodeIdsGlobal(idx) = m_mesh->m_localToGlobalMap(j);
        idx++;
        break;
      }
    }
  }
  if (m_mesh->m_enon > m_mesh->m_order1Enon)
  {
    make2ndOrder();
  }
}


void DirichletBC::setValues(PetscReal uniformValue, int species)
{
  m_nodeValues[species] = uniformValue;
}


void DirichletBC::make2ndOrder()
{
  // This could probably be optimized if it's slow

  // Find number of new nodes
  PetscInt nEdges = m_mesh->m_order2nodeNeighbors.m_dim1;
  for (PetscInt i = 0; i < nEdges; i++)
  {
    PetscInt n1 = m_mesh->m_order2nodeNeighbors(i,0);
    PetscInt n2 = m_mesh->m_order2nodeNeighbors(i,1);

    int nMatches = 0;
    for (PetscInt j = 0; j < m_nCornerNodes; j++)
    {
      if (m_nodeIds(j) == n1 || m_nodeIds(j) == n2)
      {
        nMatches++;
      }
      if (nMatches == 2)
      {
        m_nTotalNodes++;
        break;
      }
    }
  }

  // Fill array with new nodes
  Array1d<PetscInt> newNodes(m_nTotalNodes - m_nCornerNodes);
  m_order2Neighbors.resize(m_nTotalNodes - m_nCornerNodes, 2);
  PetscInt idx = 0;
  for (PetscInt i = 0; i < nEdges; i++)
  {
    PetscInt n1 = m_mesh->m_order2nodeNeighbors(i,0);
    PetscInt n2 = m_mesh->m_order2nodeNeighbors(i,1);
    PetscInt matches[2] = {-1, -1};
    for (PetscInt j = 0; j < m_nCornerNodes; j++)
    {
      if (m_nodeIds(j) == n1 || m_nodeIds(j) == n2)
      {
        if (matches[0] == -1)
        {
          matches[0] = j;
        }
        else
        {
          matches[1] = j;
          break;
        }
      }
    }
    if (matches[0] != -1 && matches[1] != -1)
    {
      newNodes(idx) = m_mesh->m_order2nodeNeighbors(i,2);
      m_order2Neighbors(idx, 0) = matches[0];
      m_order2Neighbors(idx, 1) = matches[1];
      idx++;
    }
  }

  // Copy old arrays
  Array1d<PetscInt> nodeIdsCopy(m_nCornerNodes);
  Array1d<PetscInt> nodeIdsGlobalCopy(m_nCornerNodes);
  std::vector<Array1d<PetscInt> > nodeValuesCopy(m_nSpecies);
  for (int i = 0; i < m_nSpecies; i++)
  {
    nodeValuesCopy[i].resize(m_nCornerNodes);
  }
  for (PetscInt i = 0; i < m_nCornerNodes; i++)
  {
    nodeIdsCopy(i) = m_nodeIds(i);
    nodeIdsGlobalCopy(i) = m_nodeIdsGlobal(i);
    for (int j = 0; j < m_nSpecies; j++)
    {
      nodeValuesCopy[j](i) = m_nodeValues[j](i);
    }
  }

  m_nodeIds.resize(m_nTotalNodes);
  m_nodeIdsGlobal.resize(m_nTotalNodes);
  for (int i = 0; i < m_nSpecies; i++)
  {
    m_nodeValues[i].resize(m_nTotalNodes);
  }
  for (PetscInt i = 0; i < m_nCornerNodes; i++)
  {
    m_nodeIds(i) = nodeIdsCopy(i);
    m_nodeIdsGlobal(i) = nodeIdsGlobalCopy(i);
    for (int j = 0; j < m_nSpecies; j++)
    {
      m_nodeValues[j](i) = nodeValuesCopy[j](i);
    }
  }
  for (PetscInt i = 0; i < m_nTotalNodes - m_nCornerNodes; i++)
  {
    m_nodeIds(i + m_nCornerNodes) = newNodes(i);
    m_nodeIdsGlobal(i + m_nCornerNodes) =
                                       m_mesh->m_localToGlobalMap(newNodes(i));
    // Set order 2 node's value to average of neighbors
    for (int j = 0; j < m_nSpecies; j++)
    {
      m_nodeValues[j](i + m_nCornerNodes) =
                             .5 * (m_nodeValues[j](m_order2Neighbors(i, 0))
                                   + m_nodeValues[j](m_order2Neighbors(i, 1)));
    }
  }
}


OutletBC::OutletBC(Mesh &mesh, int nSpecies) :
  DirichletBC(mesh, nSpecies)
{
  // Initialize m_normals to zero.
  m_normals.resize(m_nTotalNodes, DIM);
  for (PetscInt i = 0; i < m_nTotalNodes; i++)
  {
    for (PetscInt j = 0; j < DIM; j++)
    {
      m_normals(i, j) = 0.;
    }
  }
}


void OutletBC::setNormals(PetscReal normal[DIM])
{
  // Initialize m_normals to zero.
  m_normals.resize(m_nTotalNodes, DIM);
  for (PetscInt i = 0; i < m_nTotalNodes; i++)
  {
    for (PetscInt j = 0; j < DIM; j++)
    {
      m_normals(i, j) = normal[j];
    }
  }
}

NeumannBC::NeumannBC(Mesh &mesh,
                     int basisOrder,
                     int integrationOrder,
                     int nSpecies) :
  m_mesh(&mesh), m_nFaces(0), m_nCornerNodes(0), m_nTotalNodes(0),
  m_intScheme(integrationOrder, DIM - 1), m_order(basisOrder),
  m_nSpecies(nSpecies)
{
  // Remember this basis is only good for calculating phi, not dPhidX etc.
  Basis basis(mesh, basisOrder, DIM - 1);

  basis.calc_phi(m_intScheme, m_phi);

  m_nodeValues.resize(m_nSpecies);
  m_speciesGetsBC.resize(m_nSpecies);
  for (int i = 0; i < m_nSpecies; i++)
  {
    m_speciesGetsBC[i] = false;
  }

  switch(DIM)
  {
    case 1:
      m_enon = 1;
      break;
    case 2:
      switch(basisOrder)
      {
        case 1:
          m_enon = 2;
          break;
        case 2:
          m_enon = 3;
          break;
      }
      break;
    case 3:
      switch(basisOrder)
      {
        case 1:
          m_enon = 3;
          break;
        case 2:
          m_enon = 6;
          break;
      }
      break;
  }
}


void NeumannBC::loadNodes(std::string nodeListFilename)
{
  PetscErrorCode ierr;
  PetscMPIInt size;
  PetscMPIInt rank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  std::fstream fin;
  fin.open(nodeListFilename.c_str(), std::ios::in | std::ios::binary);
  // Expects nNodes and node IDs to be 32 bit (unsigned) integers
  uint32_t nNodes;
  fin.read((char *) &nNodes, sizeof(uint32_t));

  Array1d<uint32_t> tmpNodes(nNodes);
  fin.read((char*) tmpNodes.front(),
           nNodes * sizeof(uint32_t));
  fin.close();

  // Find faces which belong to this BC
  // Could possibly be sped up by adding faces the first time through rather
  // than searching once, preallocating, and searching again. Should be
  // changed if this is really slow.
  for (PetscInt el = 0; el < m_mesh->m_nElements; el++)
  {
    int count = 0;
    bool isMatch = false;
    for (PetscInt i = 0; i < nNodes; i++)
    {
      for (int j = 0; j < m_mesh->m_order1Enon; j++)
      {
        if (m_mesh->m_parallelToSerialMap(m_mesh->m_connectivity(el, j))
                                                                == tmpNodes(i))
        {
          count++;
        }
      }
      if (count == DIM)
      {
        isMatch = true;
        break;
      }
    }
    if (isMatch == true)
    {
      m_nFaces++;
    }
  }

  // int faces3d[4][6] = {{1, 2, 3, 5, 8, 9},
  //                      {0, 2, 3, 6, 7, 9},
  //                      {0, 1, 3, 4, 7, 8},
  //                      {0, 1, 2, 4, 5, 6}};
  // int faces2d[3][3] = {{1, 2, 4}, {0, 2, 5}, {0, 1, 6}};
  int faces3d[4][6] = {{1, 2, 3, 9, 8, 5},
                       {0, 3, 2, 9, 6, 7},
                       {0, 1, 3, 8, 7, 4},
                       {0, 2, 1, 5, 4, 6}};
  int faces2d[3][3] = {{1, 2, 4}, {2, 0, 5}, {0, 1, 3}};
  int faces1d[2] = {1, 0};

  Array2d<PetscInt> meshConnectivity(m_nFaces, m_enon);
  PetscInt matches[DIM + 1];
  PetscInt idx = 0;
  for (PetscInt el = 0; el < m_mesh->m_nElements; el++)
  {
    for (int i = 0; i < DIM + 1; i++)
    {
      matches[i] = -1;
    }
    int count = 0;
    bool isMatch = false;
    for (PetscInt i = 0; i < nNodes; i++)
    {
      for (int j = 0; j < m_mesh->m_order1Enon; j++)
      {
        if (m_mesh->m_parallelToSerialMap(m_mesh->m_connectivity(el, j))
                                                                == tmpNodes(i))
        {
          matches[j] = el;
          count++;
        }
      }
      if (count == DIM)
      {
        isMatch = true;
        break;
      }
    }
    if (isMatch == true)
    {
      // Find node opposite face
      int oppositeNode = 0;
      for (int i = 0; i < m_mesh->m_order1Enon; i++)
      {
        if (matches[i] != el)
        {
          oppositeNode = i;
          break;
        }
      }

      // Add face to meshConnectivity
      switch(DIM)
      {
        case 1:
          meshConnectivity(idx, 0) =
                             m_mesh->m_connectivity(el, faces1d[oppositeNode]);
          break;
        case 2:
          for (PetscInt i = 0; i < m_enon; i++)
          {
            meshConnectivity(idx, i) =
                          m_mesh->m_connectivity(el, faces2d[oppositeNode][i]);
          }
          break;
        case 3:
          for (PetscInt i = 0; i < m_enon; i++)
          {
            meshConnectivity(idx, i) =
                          m_mesh->m_connectivity(el, faces3d[oppositeNode][i]);
          }
          break;
      }
      idx++;
    }
  }

  // Mark BC nodes
  Array1d<int> needsNode(m_mesh->m_nTotalNodes);
  needsNode = -1;
  for (PetscInt i = 0; i < m_nFaces; i++)
  {
    for (PetscInt j = 0; j < m_enon; j++)
    {
      needsNode(meshConnectivity(i, j)) = 1;
    }
  }

  // Count BC nodes
  m_nCornerNodes = 0;
  m_nTotalNodes = 0;
  for (PetscInt i = 0; i < m_mesh->m_nTotalNodes; i++)
  {
    if (needsNode(i) == 1)
    {
      m_nTotalNodes++;
      if (i < m_mesh->m_nCornerNodes)
      {
        m_nCornerNodes++;
      }
    }
  }

  // Populate m_nodeIds and inverse mapping
  Array1d<PetscInt> meshToBcMap(m_mesh->m_nTotalNodes);
  meshToBcMap = -1;
  idx = 0;
  m_nodeIds.resize(m_nTotalNodes);
  for (PetscInt i = 0; i < m_mesh->m_nTotalNodes; i++)
  {
    if (needsNode(i) == 1)
    {
      m_nodeIds(idx) = i;
      meshToBcMap(i) = idx;
      idx++;
    }
  }

  // Populate m_connectivity and m_connectivityGlobal
  m_connectivity.resize(m_nFaces, m_enon);
  m_connectivityGlobal.resize(m_nFaces, m_enon);
  for (PetscInt i = 0; i < m_nFaces; i++)
  {
    for (int j = 0; j < m_enon; j++)
    {
      m_connectivity(i, j) = meshToBcMap(meshConnectivity(i, j));
      m_connectivityGlobal(i, j) =
                            m_mesh->m_localToGlobalMap(meshConnectivity(i, j));
    }
  }

  // Calculate Jacobians
  calculateJacobians();

  for (int i = 0; i < m_nSpecies; i++)
  {
    m_nodeValues[i].resize(m_nTotalNodes);
  }
}


void NeumannBC::setValues(PetscReal uniformValue, int species)
{
  for (int i = 0; i < m_nSpecies; i++)
  {
    m_nodeValues[species] = uniformValue;
  }
}


void NeumannBC::calculateJacobians()
{
  m_jacobians.resize(m_nFaces);
  switch(DIM)
  {
    case 1:
    {
      m_jacobians = 1.;
      break;
    }
    case 2:
    {
      for (PetscInt i = 0; i < m_nFaces; i++)
      {
        Vector p1 = m_mesh->m_coordinates(m_nodeIds(m_connectivity(i, 0)));
        Vector p2 = m_mesh->m_coordinates(m_nodeIds(m_connectivity(i, 1)));
        m_jacobians(i) = .5 * getLength(p1, p2);
      }
      break;
    }
    case 3:
    {
      for (PetscInt i = 0; i < m_nFaces; i++)
      {
        Vector p1 = m_mesh->m_coordinates(m_nodeIds(m_connectivity(i, 0)));
        Vector p2 = m_mesh->m_coordinates(m_nodeIds(m_connectivity(i, 1)));
        Vector p3 = m_mesh->m_coordinates(m_nodeIds(m_connectivity(i, 2)));
        m_jacobians(i) = getArea(p1, p2, p3);
      }
      break;
    }
  }
}
