#ifndef CHEMICAL_SYSTEM_HPP
#define CHEMICAL_SYSTEM_HPP

#include <vector>
#include <string>
#include "petscksp.h"
#include "reaction_system.hpp"

class ChemicalSystem
{
public:
  ChemicalSystem() {}

  ChemicalSystem(std::string filename);

  void initSingleSpecies();

  void loadFromFile(std::string filename);

  void outputSystem();

  /** m_volumeReactions[i] contains the ReactionEquation for species i. */
  std::vector<ReactionEquation> m_volumeReactions;

  /** m_surfaceReactions[i] contains the ReactionEquation for species i. There
    * is an entry for each species, even if it is empty. */
  std::vector<ReactionEquation> m_surfaceReactions;

  /** m_speciesNames[i] contains the name of species i. */
  std::vector<std::string> m_speciesNames;

  /** m_initialConditions[i] contains the initial/inlet condition for species
    * i. */
  std::vector<PetscReal> m_initialConditions;

  /** m_diffusivites[i] contains the diffusivity of species i. */
  std::vector<PetscReal> m_diffusivities;

  /** m_isMobile[i] tells whether species i is mobile (subject to advection and
    * diffusion. */
  std::vector<bool> m_isMobile;

  /** Number of species. This is a little redundant as it is the same as the
    * size() of all the data vectors, but it allows for a cleaner interface. */
  int m_nSpecies;
};

#endif
