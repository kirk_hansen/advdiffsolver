#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

#include "mesh.hpp"
#include "petscksp.h"
#include "FEMSystem.hpp"
#include "basis.hpp"
#include "femadvdiffsolver.hpp"
#include "adv_diff_react_system.hpp"


std::string readNextLine(std::istream &fin)
{
  std::string line;
  while (getline(fin, line))
  {
    if (line.length() > 0 && line[0] != '#')
    {
      break;
    }
  }
  return line;
}


int solveSystem(std::string paramFilename)
{
  AdvDiffReactSystem solver(paramFilename);
  solver.solveSystem();

  return 0;
}


int main(int argc, char **argv)
{
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, (char*)0, (char*)0);
  CHKERRQ(ierr);

  PetscMPIInt size;
  PetscMPIInt rank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  CHKERRQ(ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
  CHKERRQ(ierr);

  if (argc == 1)
  {
    if (rank == 0)
    {
      std::cout << "Must give parameter filename as first argument."
                << std::endl;
    }
    exit(0);
  }

  std::string paramFilename = argv[1];

  solveSystem(paramFilename);

  PetscFinalize();
  return 0;
}


// int main(int argc, char **argv)
// {
//   PetscErrorCode ierr;
//   ierr = PetscInitialize(&argc, &argv, (char*)0, (char*)0);
//   CHKERRQ(ierr);
//
//   PetscMPIInt size;
//   PetscMPIInt rank;
//   ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
//   CHKERRQ(ierr);
//   ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
//   CHKERRQ(ierr);
//
//   if (argc == 1)
//   {
//     if (rank == 0)
//     {
//       std::cout << "Must give parameter filename as first argument."
//                 << std::endl;
//     }
//     exit(0);
//   }
//
//   std::string paramFilename = argv[1];
//   if (rank == 0)
//   {
//     std::cout << "Reading parameters from " << paramFilename << "."
//               << std::endl << std::endl;
//   }
//
//   // Read params from file
//   std::ifstream fin(paramFilename.c_str());
//
//   std::string::size_type sz;
//
//   bool isReactiveSystem = std::atoi(readNextLine(fin).c_str()) != 0;
//   std::string reactionFilename;
//   if (isReactiveSystem)
//   {
//     reactionFilename = readNextLine(fin);
//     if (rank == 0)
//     {
//       std::cout << "Reactive system will be loaded from "
//                 << reactionFilename << std::endl;
//     }
//   }
//   else
//   {
//     if (rank == 0)
//     {
//       std::cout << "Not a reactive system." << std::endl;
//     }
//   }
//
//   int basisOrder = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "FEM basis function order: " << basisOrder << std::endl;
//   }
//
//   PetscReal diffusivity = std::atof(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Diffusivity: " << diffusivity << std::endl;
//   }
//
//   PetscReal rhoInfinity = std::atof(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Rho infinity: " << rhoInfinity << std::endl;
//   }
//
//   bool stabilized = (std::atoi(readNextLine(fin).c_str()) != 0);
//   if (rank == 0)
//   {
//     std::cout << "Use SUPG stabilization?: " << stabilized << std::endl;
//   }
//
//   bool storedPhidX = (std::atoi(readNextLine(fin).c_str()) != 0);
//   if (rank == 0)
//   {
//     std::cout << "Precompute dPhidX?: " << storedPhidX << std::endl;
//   }
//
//   std::string connectivityFilename = readNextLine(fin);
//   if (rank == 0)
//   {
//     std::cout << "Connectivity filename: " << connectivityFilename << std::endl;
//   }
//
//   std::string coordinatesFilename = readNextLine(fin);
//   if (rank == 0)
//   {
//     std::cout << "Coordinates filename: " << coordinatesFilename << std::endl;
//   }
//
//   std::string outputFilenameRoot = readNextLine(fin);
//   if (rank == 0)
//   {
//     std::cout << "Output filename root: " << outputFilenameRoot << std::endl;
//   }
//
//   std::string mapppingFilename = readNextLine(fin);
//   if (rank == 0)
//   {
//     std::cout << "Mapping filename: " << mapppingFilename << std::endl;
//   }
//
//   int velocityBasisOrder = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Velocity field basis order: " << velocityBasisOrder << std::endl;
//   }
//
//   std::string velocityFilenameRoot = readNextLine(fin);
//   if (rank == 0)
//   {
//     std::cout << "Velocity field filename root: "
//             << velocityFilenameRoot << std::endl;
//   }
//
//   int velocityStartTstep = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Velocity field starting time step: "
//             << velocityStartTstep << std::endl;
//   }
//
//   int nVelocityFiles = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Number of velocity files: " << nVelocityFiles << std::endl;
//   }
//
//   int velocityFileInterval = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Velocity file interval: " << velocityFileInterval << std::endl;
//   }
//
//   PetscReal velocityFileDt = std::atof(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Time between velocity files: " << velocityFileDt << std::endl;
//   }
//
//   int nTsteps = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Number of time steps: " << nTsteps << std::endl;
//   }
//
//   PetscReal dt = std::atof(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "dt: " << dt << std::endl;
//   }
//
//   PetscReal startTime = std::atof(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Simulation start time: " << startTime << std::endl;
//   }
//
//   int saveInterval = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Save interval: " << saveInterval << std::endl;
//   }
//
//
//   Mesh mesh(connectivityFilename,
//             coordinatesFilename,
//             basisOrder);
//
//   mesh.writeMappingToFile(mapppingFilename, rank);
//
//   int nSpecies = 1;
//   if (isReactiveSystem)
//   {
//     std::ifstream fin(reactionFilename.c_str());
//     std::string word = readNextLine(fin);
//     nSpecies = std::atoi(word.c_str());
//     fin.close();
//     std::cout << "nSpecies:" << nSpecies << std::endl;
//   }
//
//   FemAdvDiffSolverGenAlpha solver(mesh,
//                                   basisOrder,
//                                   velocityBasisOrder,
//                                   rhoInfinity,
//                                   diffusivity,
//                                   stabilized,
//                                   storedPhidX,
//                                   false,
//                                   nSpecies);
//
//   int nDirichletBCs = std::atoi(readNextLine(fin).c_str());
//   for (int i = 0; i < nDirichletBCs; i++)
//   {
//     std::string bcFilename = readNextLine(fin);
//     if (rank == 0)
//     {
//       std::cout << "Loading Dirichlet BC from " << bcFilename << std::endl;
//     }
//     solver.m_dirichletBCs.push_back(DirichletBC(mesh));
//     solver.m_dirichletBCs[i].loadNodes(bcFilename);
//     solver.m_dirichletBCs[i].setValues(std::atof(readNextLine(fin).c_str()));
//   }
//
//   int nOutletBCs = std::atoi(readNextLine(fin).c_str());
//   for (int i = 0; i < nOutletBCs; i++)
//   {
//     std::string bcFilename = readNextLine(fin);
//     if (rank == 0)
//     {
//       std::cout << "Loading outlet BC from " << bcFilename << std::endl;
//     }
//     solver.m_outletBCs.push_back(OutletBC(mesh));
//     solver.m_outletBCs[i].loadNodes(bcFilename);
//     solver.m_outletBCs[i].setValues(std::atof(readNextLine(fin).c_str()));
//     std::string normalString = readNextLine(fin);
//     std::stringstream stream(normalString);
//     PetscReal normal[DIM];
//     for (int j = 0; j < DIM; j++)
//     {
//       stream >> normal[j];
//     }
//     solver.m_outletBCs[i].setNormals(normal);
//   }
//
//   int nNeumannBCs = std::atoi(readNextLine(fin).c_str());
//   for (int i = 0; i < nNeumannBCs; i++)
//   {
//     std::string bcFilename = readNextLine(fin);
//     if (rank == 0)
//     {
//       std::cout << "Loading Neumann BC from " << bcFilename << std::endl;
//     }
//     solver.m_neumannBCs.push_back(NeumannBC(mesh, basisOrder, 2 * basisOrder));
//     solver.m_neumannBCs[i].loadNodes(bcFilename);
//     solver.m_neumannBCs[i].setValues(std::atof(readNextLine(fin).c_str()));
//   }
//   if (rank == 0)
//   {
//     std::cout << "Boundary conditions loaded." << std::endl;
//   }
//
//   fin.close();
//
//   solver.m_t = startTime;
//   solver.m_currentTstep = 0;
//   solver.m_dt = dt;
//   solver.m_velFilenameRoot = velocityFilenameRoot;
//   solver.m_velFileStart = velocityStartTstep;
//   solver.m_nVelFiles = nVelocityFiles;
//   solver.m_velFileInterval = velocityFileInterval;
//   solver.m_velFileDt = velocityFileDt;
//   solver.m_saveInterval = saveInterval;
//   solver.m_outputFilenameRoot = outputFilenameRoot;
//
//   if (isReactiveSystem)
//   {
//     if (rank == 0)
//     {
//       std::cout << "Loading reactive system." << std::endl;
//     }
//     // Load reactive system
//     // ReactiveSystem reactions(solver.m_solution, reactionFilename);
//
//     // for (int i = 0; i < reactions.m_speciesICs.size(); i++)
//     // {
//     //   // Set ICs
//     //   solver.applyIC(reactions.m_speciesICs[i], i);
//
//     //   // Set Dirichlet and Outlet BC values
//     //   // NOTE: This now sets all Dirichlet and outlet BCs to the IC value. There
//     //   // should probably be some kind of flag for this.
//     //   for (int j = 0; j < solver.m_dirichletBCs.size(); j++)
//     //   {
//     //     solver.m_dirichletBCs[j].setValues(reactions.m_speciesICs[i], i);
//     //   }
//     //   for (int j = 0; j < solver.m_outletBCs.size(); j++)
//     //   {
//     //     solver.m_outletBCs[j].setValues(reactions.m_speciesICs[i], i);
//     //   }
//
//     //   solver.setSpeciesNames(reactions);
//     // }
//     solver.writeHeader();
//     for (int i = 0; i < nTsteps; i++)
//     {
//       // reactions.computeReactions(solver.m_dt);
//
//       PetscInt nIterations = solver.takeTimeStep();
//     }
//   }
//   else
//   {
//     solver.writeHeader();
//     for (int i = 0; i < nTsteps; i++)
//     {
//       PetscInt nIterations = solver.takeTimeStep();
//     }
//   }
//
//   PetscFinalize();
//   return 0;
// }


// int main(int argc, char **argv)
// {
//   PetscErrorCode ierr;
//   ierr = PetscInitialize(&argc, &argv, (char*)0, (char*)0);
//   CHKERRQ(ierr);
//
//   PetscMPIInt size;
//   PetscMPIInt rank;
//   ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
//   CHKERRQ(ierr);
//   ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
//   CHKERRQ(ierr);
//
//   if (argc == 1)
//   {
//     if (rank == 0)
//     {
//       std::cout << "Must give parameter filename as first argument."
//                 << std::endl;
//     }
//     exit(0);
//   }
//
//   std::string paramFilename = argv[1];
//   if (rank == 0)
//   {
//     std::cout << "Reading parameters from " << paramFilename << "."
//               << std::endl << std::endl;
//   }
//
//   // Read params from file
//   std::ifstream fin(paramFilename.c_str());
//
//   std::string::size_type sz;
//
//   bool isReactiveSystem = std::atoi(readNextLine(fin).c_str()) != 0;
//   std::string reactionFilename;
//   if (isReactiveSystem)
//   {
//     reactionFilename = readNextLine(fin);
//     if (rank == 0)
//     {
//       std::cout << "Reactive system will be loaded from "
//                 << reactionFilename << std::endl;
//     }
//   }
//   else
//   {
//     if (rank == 0)
//     {
//       std::cout << "Not a reactive system." << std::endl;
//     }
//   }
//
//   int basisOrder = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "FEM basis function order: " << basisOrder << std::endl;
//   }
//
//   PetscReal diffusivity = std::atof(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Diffusivity: " << diffusivity << std::endl;
//   }
//
//   PetscReal rhoInfinity = std::atof(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Rho infinity: " << rhoInfinity << std::endl;
//   }
//
//   bool stabilized = (std::atoi(readNextLine(fin).c_str()) != 0);
//   if (rank == 0)
//   {
//     std::cout << "Use SUPG stabilization?: " << stabilized << std::endl;
//   }
//
//   bool storedPhidX = (std::atoi(readNextLine(fin).c_str()) != 0);
//   if (rank == 0)
//   {
//     std::cout << "Precompute dPhidX?: " << storedPhidX << std::endl;
//   }
//
//   std::string connectivityFilename = readNextLine(fin);
//   if (rank == 0)
//   {
//     std::cout << "Connectivity filename: " << connectivityFilename << std::endl;
//   }
//
//   std::string coordinatesFilename = readNextLine(fin);
//   if (rank == 0)
//   {
//     std::cout << "Coordinates filename: " << coordinatesFilename << std::endl;
//   }
//
//   std::string outputFilenameRoot = readNextLine(fin);
//   if (rank == 0)
//   {
//     std::cout << "Output filename root: " << outputFilenameRoot << std::endl;
//   }
//
//   std::string mapppingFilename = readNextLine(fin);
//   if (rank == 0)
//   {
//     std::cout << "Mapping filename: " << mapppingFilename << std::endl;
//   }
//
//   int velocityBasisOrder = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Velocity field basis order: " << velocityBasisOrder << std::endl;
//   }
//
//   std::string velocityFilenameRoot = readNextLine(fin);
//   if (rank == 0)
//   {
//     std::cout << "Velocity field filename root: "
//             << velocityFilenameRoot << std::endl;
//   }
//
//   int velocityStartTstep = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Velocity field starting time step: "
//             << velocityStartTstep << std::endl;
//   }
//
//   int nVelocityFiles = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Number of velocity files: " << nVelocityFiles << std::endl;
//   }
//
//   int velocityFileInterval = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Velocity file interval: " << velocityFileInterval << std::endl;
//   }
//
//   PetscReal velocityFileDt = std::atof(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Time between velocity files: " << velocityFileDt << std::endl;
//   }
//
//   int nTsteps = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Number of time steps: " << nTsteps << std::endl;
//   }
//
//   PetscReal dt = std::atof(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "dt: " << dt << std::endl;
//   }
//
//   PetscReal startTime = std::atof(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Simulation start time: " << startTime << std::endl;
//   }
//
//   int saveInterval = std::atoi(readNextLine(fin).c_str());
//   if (rank == 0)
//   {
//     std::cout << "Save interval: " << saveInterval << std::endl;
//   }
//
//
//   Mesh mesh(connectivityFilename,
//             coordinatesFilename,
//             basisOrder);
//
//   mesh.writeMappingToFile(mapppingFilename, rank);
//
//   int nSpecies = 1;
//   if (isReactiveSystem)
//   {
//     std::ifstream fin(reactionFilename.c_str());
//     std::string word = readNextLine(fin);
//     nSpecies = std::atoi(word.c_str());
//     fin.close();
//     std::cout << "nSpecies:" << nSpecies << std::endl;
//   }
//
//   FemAdvDiffSolverGenAlpha solver(mesh,
//                                   basisOrder,
//                                   velocityBasisOrder,
//                                   rhoInfinity,
//                                   diffusivity,
//                                   stabilized,
//                                   storedPhidX,
//                                   false,
//                                   nSpecies);
//
//   int nDirichletBCs = std::atoi(readNextLine(fin).c_str());
//   for (int i = 0; i < nDirichletBCs; i++)
//   {
//     std::string bcFilename = readNextLine(fin);
//     if (rank == 0)
//     {
//       std::cout << "Loading Dirichlet BC from " << bcFilename << std::endl;
//     }
//     solver.m_dirichletBCs.push_back(DirichletBC(mesh));
//     solver.m_dirichletBCs[i].loadNodes(bcFilename);
//     solver.m_dirichletBCs[i].setValues(std::atof(readNextLine(fin).c_str()));
//   }
//
//   int nOutletBCs = std::atoi(readNextLine(fin).c_str());
//   for (int i = 0; i < nOutletBCs; i++)
//   {
//     std::string bcFilename = readNextLine(fin);
//     if (rank == 0)
//     {
//       std::cout << "Loading outlet BC from " << bcFilename << std::endl;
//     }
//     solver.m_outletBCs.push_back(OutletBC(mesh));
//     solver.m_outletBCs[i].loadNodes(bcFilename);
//     solver.m_outletBCs[i].setValues(std::atof(readNextLine(fin).c_str()));
//     std::string normalString = readNextLine(fin);
//     std::stringstream stream(normalString);
//     PetscReal normal[DIM];
//     for (int j = 0; j < DIM; j++)
//     {
//       stream >> normal[j];
//     }
//     solver.m_outletBCs[i].setNormals(normal);
//   }
//
//   int nNeumannBCs = std::atoi(readNextLine(fin).c_str());
//   for (int i = 0; i < nNeumannBCs; i++)
//   {
//     std::string bcFilename = readNextLine(fin);
//     if (rank == 0)
//     {
//       std::cout << "Loading Neumann BC from " << bcFilename << std::endl;
//     }
//     solver.m_neumannBCs.push_back(NeumannBC(mesh, basisOrder, 2 * basisOrder));
//     solver.m_neumannBCs[i].loadNodes(bcFilename);
//     solver.m_neumannBCs[i].setValues(std::atof(readNextLine(fin).c_str()));
//   }
//   if (rank == 0)
//   {
//     std::cout << "Boundary conditions loaded." << std::endl;
//   }
//
//   fin.close();
//
//   solver.m_t = startTime;
//   solver.m_currentTstep = 0;
//   solver.m_dt = dt;
//   solver.m_velFilenameRoot = velocityFilenameRoot;
//   solver.m_velFileStart = velocityStartTstep;
//   solver.m_nVelFiles = nVelocityFiles;
//   solver.m_velFileInterval = velocityFileInterval;
//   solver.m_velFileDt = velocityFileDt;
//   solver.m_saveInterval = saveInterval;
//   solver.m_outputFilenameRoot = outputFilenameRoot;
//
//   if (isReactiveSystem)
//   {
//     if (rank == 0)
//     {
//       std::cout << "Loading reactive system." << std::endl;
//     }
//     // Load reactive system
//     // ReactiveSystem reactions(solver.m_solution, reactionFilename);
//
//     // for (int i = 0; i < reactions.m_speciesICs.size(); i++)
//     // {
//     //   // Set ICs
//     //   solver.applyIC(reactions.m_speciesICs[i], i);
//
//     //   // Set Dirichlet and Outlet BC values
//     //   // NOTE: This now sets all Dirichlet and outlet BCs to the IC value. There
//     //   // should probably be some kind of flag for this.
//     //   for (int j = 0; j < solver.m_dirichletBCs.size(); j++)
//     //   {
//     //     solver.m_dirichletBCs[j].setValues(reactions.m_speciesICs[i], i);
//     //   }
//     //   for (int j = 0; j < solver.m_outletBCs.size(); j++)
//     //   {
//     //     solver.m_outletBCs[j].setValues(reactions.m_speciesICs[i], i);
//     //   }
//
//     //   solver.setSpeciesNames(reactions);
//     // }
//     solver.writeHeader();
//     for (int i = 0; i < nTsteps; i++)
//     {
//       // reactions.computeReactions(solver.m_dt);
//
//       PetscInt nIterations = solver.takeTimeStep();
//     }
//   }
//   else
//   {
//     solver.writeHeader();
//     for (int i = 0; i < nTsteps; i++)
//     {
//       PetscInt nIterations = solver.takeTimeStep();
//     }
//   }
//
//   PetscFinalize();
//   return 0;
// }
//
