#ifndef ADV_DIFF_REACT_SYSTEM_HPP
#define ADV_DIFF_REACT_SYSTEM_HPP

#include <vector>
#include <string>
#include "petscksp.h"
#include "chemical_system.hpp"
#include "reaction_solver.hpp"
#include "femadvdiffsolver.hpp"
#include "reactive_bc.hpp"

class AdvDiffReactSystem
{
public:
  /** Construcy empty AdvDiffReactSystem. If this constructor is used, user is
    * responsible for initializing all variables before use (either with
    * initFromFile or manually). */
  AdvDiffReactSystem();

  /** Construct AdvDiffReactSystem from input file. See params.inp for proper
    * format. */
  AdvDiffReactSystem(std::string filename);

  ~AdvDiffReactSystem();

  /** Initialize AdvDiffReactSystem from inpur file. See params.inp for proper
    * format. */
  void initFromFile(std::string filename);

  /** Solve AdvDiffReactSystem for all timesteps. */
  void solveSystem();

  /** Step forward one time step. */
  void takeTimeStep();

  /** Write all solution vectors to binary files. */
  void writeSolutionToBin(std::string filenameRoot,
                          PetscInt tstep,
                          bool order1Only=true);

  /** Load initial condition from binary file. */
  void loadInitialConditionFromBin(std::string filename, int species=0);

  /** Initialize member variable m_needsLHSRebuild. This should be called after
    * all other solver parameters are set up. */
  void initNeedsLHSRebuild();

  /** Write header for screen output. */
  void writeHeader();

  Mesh m_mesh;

  ChemicalSystem m_chemicalSystem;

  std::vector<Vec> m_solution;

  FemAdvDiffSolverGenAlpha m_femSolver;

  ReactionSolver m_reactionSolver;

  /** Vector of reactive BCs evaluated by the solver. */
  std::vector<ReactiveBC> m_reactiveBCs;

  /** The current simulation time. Defaults to 0.*/
  PetscReal m_t;

  /** Simulation time step size. */
  PetscReal m_dt;

  /** The current time step. */
  PetscInt m_currentTstep;

  /** Final time step. NOTE: This should be replaced with a
    * simulation end time if variable time stepping is implemented. */
  PetscInt m_finalTstep;

  /** Root for the output filename. Output files will be written to
    * "root.(speciesname).(timestep).bin.(processor)" */
  std::string m_outputFilenameRoot;

  /** Number of timesteps per output file. This output will only include 1st
    * order nodes. */
  PetscInt m_saveInterval;

  /** Number of timesteps per restart file. This output will include all nodes,
    * including 2nd order. Files will be written to same filename as output
    * files, but prepended with "restart.". No restarts will be written if
    * m_restartInterval is negative, and a restart will be written at the end
    * of the simulation if the value of m_restartFilename is 0. */
  PetscInt m_restartInterval;

  /** Rank of this process. */
  PetscMPIInt m_MPIrank;

  /** Number of MPI processes. */
  PetscMPIInt m_MPIsize;

  /** Relative error value for determination of whether diffusivity of current
    * species is different enough from previous species to require rebuild of
    * LHS matrix. Defaults to 1e-9. */
  PetscReal m_eps;

  /** Whether or not LHS matrix needs to be rebuilt for this species. Matrix
    * needs to be rebuilt for species 0 (new timestep), any species that has a
    * different diffusivity than the previous species, and any species that
    * follows one with a unique (i.e. non-inlet/outlet) Dirichlet BC. */
  std::vector<bool> m_needsLHSRebuild;


private:
  std::string readNextLine(std::istream &fin);

  template <typename T>
  std::string toString(T rhs)
  {
    std::stringstream ss;
    ss << rhs;
    return ss.str();
  }
};

#endif
