#ifndef GAUSS_LEGENDRE_HPP
#define GAUSS_LEGENDRE_HPP

#include "array_types.hpp"

class IntegrationScheme
// Works for m_dim = 1, 2, or 3
// For m_dim = 1: m_order <= 9
//     m_dim = 2: m_order <= 6
//     m_dim = 3: m_order <= 6
{
public:
  IntegrationScheme(int dim=DIM);
  IntegrationScheme(int order, int dim=DIM);

  /** Evaluate m_gaussPoints and m_gaussWeights. m_order must be set before
    * this function is used, and m_gaussPoints and m_gaussWeights are empty
    * before evaluation. */
  void evaluate();

  /** Order of integration. */
  int m_order;

  /** The number of integration points in each element. */
  int m_nIntegrationPoints;

  /** Number of dimensions (1, 2, or 3). */
  int m_dim;

  /** Gauss-Legendre integration points. m_gaussPoints(i, j) gives the jth
    * coordinate of the ith integration point. */
  Array2d<PetscReal> m_gaussPoints;

  /** Gauss-Legendre integration points. m_gaussPoints(i) gives the weight of
    * the ith integration point. */
  Array1d<PetscReal> m_gaussWeights;
};

#endif
