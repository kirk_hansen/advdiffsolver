#include "femadvdiffsolver.hpp"

#include <fstream>
#include <string>
#include <cmath>
#include "petscksp.h"
#include "array_types.hpp"
#include "bcs.hpp"
#include "basis.hpp"
#include "ioutilities.hpp"

template <typename T>
std::string toString(T rhs)
{
  std::stringstream ss;
  ss << rhs;
  return ss.str();
}

FemAdvDiffSolver::FemAdvDiffSolver(Mesh &mesh, std::vector<Vec> &solution) :
  m_mesh(mesh),
  m_solution(solution),
  m_basis(m_mesh),
  m_velocityBasis(m_mesh),
  m_diffusivity(-1.)
{
  MPI_Comm_rank(PETSC_COMM_WORLD,&m_MPIrank);
  MPI_Comm_size(PETSC_COMM_WORLD,&m_MPIsize);
}

FemAdvDiffSolver::FemAdvDiffSolver(Mesh &mesh,
                                   std::vector<Vec> &solution,
                                   int basisOrder,
                                   int velocityBasisOrder,
                                   bool stabilized,
                                   bool store_dPhidX,
                                   bool verbose) :
  m_mesh(mesh),
  m_solution(solution),
  m_basis(m_mesh),
  m_velocityBasis(m_mesh),
  m_diffusivity(-1.)
{
  MPI_Comm_rank(PETSC_COMM_WORLD,&m_MPIrank);
  MPI_Comm_size(PETSC_COMM_WORLD,&m_MPIsize);

  initialize(basisOrder,
             velocityBasisOrder,
             stabilized,
             store_dPhidX,
             verbose);
}

void FemAdvDiffSolver::initialize(int basisOrder,
                                  int velocityBasisOrder,
                                  bool stabilized,
                                  bool store_dPhidX,
                                  bool verbose)
{
  m_basis.setOrder(basisOrder);
  m_velocityBasis.setOrder(velocityBasisOrder);
  m_integrationScheme.m_order = 2 * basisOrder + velocityBasisOrder - 1;
  m_integrationScheme.evaluate();
  m_stabilized = stabilized;
  m_store_dPhidX = store_dPhidX;
  m_verbose = verbose;
  m_nSpecies = m_solution.size();

  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "Initializing advection-diffusion problem." << std::endl;
  }
  if (m_mesh.m_nTotalNodes == 0)
  {
    if (m_verbose && m_MPIrank == 0)
    {
      std::cout << "Must load mesh before setting basis. Aborting."
                << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }

  m_localDOF = m_mesh.m_nOwnedNodes;
  m_globalDOF = m_mesh.m_nTotalNodesGlobal;

  m_writeSpeciesToFile.resize(m_nSpecies);
  // Default to write all species
  for (int i = 0; i < m_nSpecies; i++)
  {
    m_writeSpeciesToFile[i] = true;
  }

  if (basisOrder == 1)
  {
    m_enon = DIM + 1;
  }
  else if (basisOrder == 2)
  {
    if (m_mesh.m_nTotalNodes - m_mesh.m_nCornerNodes == 0)
    {
      if (m_verbose && m_MPIrank == 0)
      {
        std::cout << "Must load order 2 mesh for order 2 system. Aborting."
                  << std::endl;
      }
      MPI_Abort(PETSC_COMM_WORLD, 1);
    }

    if (DIM == 1)
    {
      m_enon = 3;
    }
    if (DIM == 2)
    {
      m_enon = 6;
    }
    if (DIM == 3)
    {
      m_enon = 10;
    }
  }
  else
  {
    if (m_verbose && m_MPIrank == 0)
    {
      std::cout << "Order " << basisOrder << " not available." << std::endl;
      MPI_Abort(PETSC_COMM_WORLD, 1);
    }
  }

  m_basis.calc_phi(m_integrationScheme, m_phi);
  m_velocityBasis.calc_phi(m_integrationScheme, m_velocityPhi);
  if (m_store_dPhidX == true)
  {
    if (m_verbose && m_MPIrank == 0)
    {
      std::cout << "Precomputing basis function gradients." << std::endl;
    }
    m_dPhidX.resize(m_mesh.m_nElements);
    for (PetscInt i = 0; i < m_mesh.m_nElements; i++)
    {
      m_basis.calc_dPhidX(i, m_integrationScheme, m_dPhidX(i));
    }

    if (m_stabilized == true and m_basis.m_order == 2)
    {
      m_laplacePhi.resize(m_mesh.m_nElements);
      for (PetscInt i = 0; i < m_mesh.m_nElements; i++)
      {
        m_basis.calc_laplacePhi(i, m_laplacePhi(i));
      }
    }
  }

  // Set up vectors
  // m_solution.resize(m_nSpecies);
  // for (int i = 0; i < m_nSpecies; i++)
  // {
  //   VecCreate(PETSC_COMM_WORLD, &m_solution[i]);
  //   VecSetSizes(m_solution[i], m_localDOF, m_globalDOF);
  //   if (m_MPIsize == 1)
  //   {
  //     VecSetType(m_solution[i], VECSEQ);
  //   }
  //   else
  //   {
  //     VecSetType(m_solution[i], VECMPI);
  //   }
  //   VecSetFromOptions(m_solution[i]);
  //   VecSet(m_solution[i], 0.);
  // }

  VecDuplicate(m_solution[0], &m_rhs);
  // VecCreate(PETSC_COMM_WORLD, &m_rhs);
  // VecSetSizes(m_rhs, m_localDOF, m_globalDOF);
  // if (m_MPIsize == 1)
  // {
  //   VecSetType(m_rhs, VECSEQ);
  // }
  // else
  // {
  //   VecSetType(m_rhs, VECMPI);
  // }
  // VecSetFromOptions(m_rhs);

  //Set up matrices
  MatCreate(PETSC_COMM_WORLD, &m_LHS);
  MatSetSizes(m_LHS, m_localDOF, m_localDOF, m_globalDOF, m_globalDOF);

  if (m_MPIsize == 1)
  {
    MatSetType(m_LHS, MATSEQAIJ);

    MatSeqAIJSetPreallocation(m_LHS, 0, m_mesh.m_nFemDiagonalEntries.front());
  }
  else
  {
    MatSetType(m_LHS, MATMPIAIJ);
    MatMPIAIJSetPreallocation(m_LHS, 0, m_mesh.m_nFemDiagonalEntries.front(),
                              0, m_mesh.m_nFemOffDiagonalEntries.front());
  }
  MatSetOption(m_LHS,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);
  // This should really be on, but would require reworking the DirichletBC
  // class so that ghost nodes are not included.
  // MatSetOption(m_LHS, MAT_NO_OFF_PROC_ZERO_ROWS, PETSC_TRUE);

  KSPCreate(PETSC_COMM_WORLD, &m_ksp);
  KSPSetFromOptions(m_ksp);
  KSPSetOperators(m_ksp, m_LHS, m_LHS);
  KSPSetInitialGuessNonzero(m_ksp, PETSC_TRUE);
}


FemAdvDiffSolver::~FemAdvDiffSolver()
{
  // for (int i = 0; i < m_nSpecies; i++)
  // {
  //   VecDestroy(&m_solution[i]);
  // }
  VecDestroy(&m_rhs);
  MatDestroy(&m_LHS);
  KSPDestroy(&m_ksp);
}


void FemAdvDiffSolver::buildDiffusiveStiffnessMatrix(Mat &Kdiff,
                                                     PetscReal diffusivity,
                                                     bool zeroFirst)
{
  if (zeroFirst == true)
  {
    MatZeroEntries(Kdiff);
  }
  Array3d<PetscReal> *dPhidX;
  Array3d<PetscReal> dPhidXlocal;

  if (m_store_dPhidX == false)
  {
    dPhidXlocal.resize(m_integrationScheme.m_nIntegrationPoints,
                       m_basis.m_nBasisFunctions,
                       DIM);
  }

  PetscReal weight;
  Array2d<PetscReal> A_local;
  A_local.resize(m_enon, m_enon);

  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "Building diffusive stiffness matrix..." << std::endl;
  }
  int pct = 0;
  for (PetscInt el = 0; el < m_mesh.m_nElements; el++)
  {
    if (m_verbose && m_MPIrank == 0)
    {
      if (el * 100. / m_mesh.m_nElements > pct)
      {
        std::cout << "\r" << pct << "%" << std::flush;
        pct += 5;
      }
    }
    A_local = 0.;

    if (m_store_dPhidX == true)
    {
      dPhidX = &m_dPhidX(el);
    }
    else
    {
      m_basis.calc_dPhidX(el, m_integrationScheme, dPhidXlocal);
      dPhidX = &dPhidXlocal;
    }

    for (int i = 0;
         i < m_integrationScheme.m_nIntegrationPoints;
         i++)
    {
      weight = m_integrationScheme.m_gaussWeights(i);
      for (int j = 0; j < m_enon; j++)
      {
        for (int k = 0; k < m_enon; k++)
        {
          for (int m = 0; m < DIM; m++)
          {
            // This could be a little bit more efficient maybe if weight was
            // not multiplied by each dimension.
            A_local(j, k) += (*dPhidX)(i, j, m) * (*dPhidX)(i, k, m) * weight;
          }
        }
      }
    }

    // Multiply by Jacobian
    PetscReal jac = m_mesh.m_jacobian(el);
    for (int i = 0; i < m_enon; i++)
    {
      for (int j = 0; j < m_enon; j++)
      {
        A_local(i, j) *= jac;
      }
    }

    // Multiply by diffusivity
    // For efficiency this could be moved outside to multiply the whole matrix
    A_local *= diffusivity;

    // Send to global matrix
    MatSetValues(Kdiff, m_enon, &m_mesh.m_connectivityGlobal(el, 0),
                 m_enon, &m_mesh.m_connectivityGlobal(el, 0), A_local.front(),
                 ADD_VALUES);

  }
  MatAssemblyBegin(Kdiff, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(Kdiff, MAT_FINAL_ASSEMBLY);
  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "\rdone." << std::endl;
  }
}


void FemAdvDiffSolver::buildMassMatrix(Mat &M, bool zeroFirst)
{
  if (zeroFirst == true)
  {
    MatZeroEntries(M);
  }
  PetscReal weight;
  Array2d<PetscReal> A_local;
  A_local.resize(m_enon, m_enon);

  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "Building mass matrix..." << std::endl;
  }
  int pct = 0;
  for (PetscInt el = 0; el < m_mesh.m_nElements; el++)
  {
    if (m_verbose && m_MPIrank == 0)
    {
      if (el * 100. / m_mesh.m_nElements > pct)
      {
        std::cout << "\r" << pct << "%" << std::flush;
        pct += 5;
      }
    }
    A_local = 0.;

    for (int i = 0;
         i < m_integrationScheme.m_nIntegrationPoints;
         i++)
    {
      weight = m_integrationScheme.m_gaussWeights(i);
      for (int j = 0; j < m_enon; j++)
      {
        for (int k = 0; k < m_enon; k++)
        {
          // This could be a little bit more efficient maybe if weight was
          // not multiplied by each dimension.
          A_local(j, k) += m_phi(i, j) * m_phi(i, k) * weight;
        }
      }
    }

    // Multiply by Jacobian
    PetscReal jac = m_mesh.m_jacobian(el);
    for (int i = 0; i < m_enon; i++)
    {
      for (int j = 0; j < m_enon; j++)
      {
        A_local(i, j) *= jac;
      }
    }

    // Send to global matrix
    MatSetValues(M, m_enon, &m_mesh.m_connectivityGlobal(el, 0),
                 m_enon, &m_mesh.m_connectivityGlobal(el, 0), A_local.front(),
                 ADD_VALUES);

  }
  MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY);
  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "\rdone." << std::endl;
  }
}


void FemAdvDiffSolver::buildAdvectiveStiffnessMatrix(Mat &Kadv, bool zeroFirst)
{
  if (zeroFirst == true)
  {
    MatZeroEntries(Kadv);
  }
  Array3d<PetscReal> *dPhidX;
  Array3d<PetscReal> dPhidXlocal;

  if (m_store_dPhidX == false)
  {
    dPhidXlocal.resize(m_integrationScheme.m_nIntegrationPoints,
                       m_basis.m_nBasisFunctions,
                       DIM);
  }

  PetscReal weight;
  Array2d<PetscReal> A_local;
  A_local.resize(m_enon, m_enon);

  Array1d<PetscReal> velocity(3);

  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "Building advective stiffness matrix..." << std::endl;
  }
  int pct = 0;
  for (PetscInt el = 0; el < m_mesh.m_nElements; el++)
  {
    if (m_verbose && m_MPIrank == 0)
    {
      if (el * 100. / m_mesh.m_nElements > pct)
      {
        std::cout << "\r" << pct << "%" << std::flush;
        pct += 5;
      }
    }

    A_local = 0.;

    if (m_store_dPhidX == true)
    {
      dPhidX = &m_dPhidX(el);
    }
    else
    {
      m_basis.calc_dPhidX(el, m_integrationScheme, dPhidXlocal);
      dPhidX = &dPhidXlocal;
    }

    for (int i = 0;
         i < m_integrationScheme.m_nIntegrationPoints;
         i++)
    {
      // Calculate velocity
      velocity = 0.;
      for (PetscInt j = 0; j < m_velocityBasis.m_nBasisFunctions; j++)
      {
        for (PetscInt k = 0; k < DIM; k++)
        {
          PetscInt velocityIdx = m_mesh.m_connectivity(el, j);
          if (m_velocityBasis.m_order == 1 &&
              velocityIdx >= m_mesh.m_nOwnedCornerNodes)
          {
            velocityIdx -= m_mesh.m_nOwnedNodes - m_mesh.m_nOwnedCornerNodes;
          }
          velocity(k) += m_velocityPhi(i, j)
                         * m_currentVelocity(velocityIdx, k);
        }
      }

      weight = m_integrationScheme.m_gaussWeights(i);
      for (int j = 0; j < m_enon; j++)
      {
        for (int k = 0; k < m_enon; k++)
        {
          for (int m = 0; m < DIM; m++)
          {
            // This could be a little bit more efficient maybe if weight was
            // not multiplied by each dimension.
            A_local(j, k) += m_phi(i, j) * (*dPhidX)(i, k, m) * velocity(m)
                             * weight;
          }
        }
      }
    }

    // Multiply by Jacobian
    PetscReal jac = m_mesh.m_jacobian(el);
    for (int i = 0; i < m_enon; i++)
    {
      for (int j = 0; j < m_enon; j++)
      {
        A_local(i, j) *= jac;
      }
    }

    // Send to global matrix
    MatSetValues(Kadv, m_enon, &m_mesh.m_connectivityGlobal(el, 0),
                 m_enon, &m_mesh.m_connectivityGlobal(el, 0), A_local.front(),
                 ADD_VALUES);
  }
  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "\rdone." << std::endl;
  }
  MatAssemblyBegin(Kadv, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(Kadv, MAT_FINAL_ASSEMBLY);
}


void FemAdvDiffSolver::applyNeumannBCs(Vec &rhs, bool zeroFirst, int species)
{
  PetscErrorCode ierr;
  PetscMPIInt size;
  PetscMPIInt rank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  if (zeroFirst == true)
  {
    VecSet(rhs, 0.);
  }
  Array1d<PetscReal> b_local(m_enon);
  for (PetscInt bcidx = 0; bcidx < m_neumannBCs.size(); bcidx++)
  {
    if (m_neumannBCs[bcidx].m_speciesGetsBC[species])
    {
      if (m_verbose && m_MPIrank == 0)
      {
        std::cout << "Applying Neumann BC " << bcidx + 1 << " of "
                  << m_neumannBCs.size() << std::endl;
      }
      NeumannBC &bc = m_neumannBCs[bcidx];
      for (PetscInt face = 0; face < bc.m_nFaces; face++)
      {
        b_local = 0.;
        for (int i = 0; i < bc.m_intScheme.m_nIntegrationPoints; i++)
        {
          PetscReal &weight = bc.m_intScheme.m_gaussWeights(i);
          // Calculate value of Neumann BC at this integration point
          PetscReal q = 0.;
          for (int j = 0; j < bc.m_enon; j++)
          {
            q += bc.m_nodeValues[species](bc.m_connectivity(face, j))
                 * bc.m_phi(i, j);
          }

          // Add contribution
          for (int j = 0; j < bc.m_enon; j++)
          {
            b_local(j) -= bc.m_phi(i, j) * q * weight;
          }
        }

        b_local *= bc.m_jacobians(face);
        VecSetValues(rhs, bc.m_enon, &bc.m_connectivityGlobal(face, 0),
                     b_local.front(), ADD_VALUES);
      }
    }
  }
  VecAssemblyBegin(rhs);
  VecAssemblyEnd(rhs);
}


void FemAdvDiffSolver::applyDirichletBCs(int species)
{

  Vec solns;
  VecCreate(PETSC_COMM_WORLD, &solns);
  VecSetSizes(solns, m_localDOF, m_globalDOF);
  if (m_MPIsize == 1)
  {
    VecSetType(solns, VECSEQ);
  }
  else
  {
    VecSetType(solns, VECMPI);
  }
  VecSetFromOptions(solns);

  for (int i = 0; i < m_dirichletBCs.size(); i++)
  {
    if (m_dirichletBCs[i].m_speciesGetsBC[species])
    {
      VecSetValues(solns,
                   m_dirichletBCs[i].m_nodeValues[species].m_size,
                   m_dirichletBCs[i].m_nodeIdsGlobal.front(),
                   m_dirichletBCs[i].m_nodeValues[species].front(),
                   INSERT_VALUES);
    }
  }
  VecAssemblyBegin(solns);
  VecAssemblyEnd(solns);

  for (int i = 0; i < m_dirichletBCs.size(); i++)
  {
    if (m_dirichletBCs[i].m_speciesGetsBC[species])
    {
      if (m_verbose && m_MPIrank == 0)
      {
        std::cout << "Applying Dirichlet BC " << i + 1 << " of "
                  << m_dirichletBCs.size() << std::endl;
      }
      MatZeroRows(m_LHS,
                  m_dirichletBCs[i].m_nodeValues[species].m_size,
                  m_dirichletBCs[i].m_nodeIdsGlobal.front(),
                  1.,
                  solns,
                  m_rhs);
    }
  }

  MatAssemblyBegin(m_LHS, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_LHS, MAT_FINAL_ASSEMBLY);

  VecDestroy(&solns);
}


void FemAdvDiffSolver::applyOutletBCs(int species)
{
  for (int i = 0; i < m_outletBCs.size(); i++)
  {
    if (m_outletBCs[i].m_speciesGetsBC[species])
    {
      if (m_verbose && m_MPIrank == 0)
      {
        std::cout << "Applying outlet BC " << i + 1 << " of "
                  << m_outletBCs.size() << std::endl;
      }

      Array1d<uint8_t> flowIsIn(m_outletBCs[i].m_nTotalNodes);
      flowIsIn = 0;
      PetscInt nDirichletNodes = 0;
      PetscInt nCoincidentNodes;
      if (m_velocityBasis.m_order == m_basis.m_order)
      {
        nCoincidentNodes = m_outletBCs[i].m_nTotalNodes;
      }
      else
      {
        nCoincidentNodes = m_outletBCs[i].m_nCornerNodes;
      }
      for (PetscInt j = 0; j < nCoincidentNodes; j++)
      {
        PetscReal dot = 0.;
        for (PetscInt k = 0; k < DIM; k++)
        {
          PetscInt velocityIdx = m_outletBCs[i].m_nodeIds(j);
          if (m_velocityBasis.m_order == 1 &&
              velocityIdx >= m_mesh.m_nOwnedCornerNodes)
          {
            velocityIdx -= m_mesh.m_nOwnedNodes - m_mesh.m_nOwnedCornerNodes;
          }
          dot += m_currentVelocity(velocityIdx, k)
                 * m_outletBCs[i].m_normals(j, k);
        }
        if (dot <= 0.)
        {
          flowIsIn(j) = 1;
          nDirichletNodes++;
        }
      }
      if (m_basis.m_order == 2 && m_velocityBasis.m_order == 1)
      {
        for (PetscInt j = m_outletBCs[i].m_nCornerNodes;
             j < m_outletBCs[i].m_nTotalNodes;
             j++)
        {
          PetscReal dot = 0.;
          PetscInt neighIdx = j - m_outletBCs[i].m_nCornerNodes;
          PetscInt velocityIdx1 = m_outletBCs[i].m_order2Neighbors(neighIdx, 0);
          PetscInt velocityIdx2 = m_outletBCs[i].m_order2Neighbors(neighIdx, 1);
          if (m_velocityBasis.m_order == 1 &&
              velocityIdx1 >= m_mesh.m_nOwnedCornerNodes)
          {
            velocityIdx1 -= m_mesh.m_nOwnedNodes - m_mesh.m_nOwnedCornerNodes;
          }
          if (m_velocityBasis.m_order == 1 &&
              velocityIdx2 >= m_mesh.m_nOwnedCornerNodes)
          {
            velocityIdx2 -= m_mesh.m_nOwnedNodes - m_mesh.m_nOwnedCornerNodes;
          }
          for (PetscInt k = 0; k < DIM; k++)
          {
            PetscReal vel = .5 * (m_currentVelocity(velocityIdx1, k)
                                  + m_currentVelocity(velocityIdx2, k));
            dot += vel * m_outletBCs[i].m_normals(j, k);
          }
          if (dot <= 0.)
          {
            flowIsIn(j) = 1;
            nDirichletNodes++;
          }
        }
      }

      Array1d<PetscInt> dirichletGlobalNodeIds(nDirichletNodes);
      PetscInt currentIdx = 0;
      for (PetscInt j = 0; j < m_outletBCs[i].m_nTotalNodes; j++)
      {
        if (flowIsIn(j) == 1)
        {
          dirichletGlobalNodeIds(currentIdx) = m_outletBCs[i].m_nodeIdsGlobal(j);
          currentIdx++;
        }
      }

      MatZeroRows(m_LHS,
                  nDirichletNodes,
                  dirichletGlobalNodeIds.front(),
                  1.,
                  m_solution[species],
                  m_rhs);
    }
  }

  MatAssemblyBegin(m_LHS, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_LHS, MAT_FINAL_ASSEMBLY);
}


void FemAdvDiffSolver::loadVelocityFromBin(Array2d<PetscReal> &velocity,
                                           std::string filename)
{
  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "Loading velocity field from " << filename << std::endl;
  }
  if (velocity.m_dim1 != m_mesh.m_nTotalNodes
      || velocity.m_dim2 != DIM)
  {
    velocity.resize(m_mesh.m_nTotalNodes, DIM);
  }
  uint32_t nEntries;
  PetscInt nEntriesMPI; // To ensure types are right
  Array2d<PetscReal> serialVelocity;

  if (m_MPIrank == 0)
  {
    std::fstream fin;
    fin.open(filename.c_str(), std::ios::in | std::ios::binary);

    // Read number of entries from header
    fin.read((char *) &nEntries, sizeof(uint32_t));
    if (m_velocityBasis.m_order == 1)
    {
      if (nEntries != m_mesh.m_nCornerNodesGlobal)
      {
        std::cout << "Mesh has " << m_mesh.m_nCornerNodesGlobal
                  << " nodes but " << filename << " has "
                  << nEntries << " entries." << std::endl;
        MPI_Abort(PETSC_COMM_WORLD, 1);
      }
    }
    else if (m_velocityBasis.m_order == 2)
    {
      if (nEntries != m_mesh.m_nTotalNodesGlobal)
      {
        std::cout << "Mesh has " << m_mesh.m_nTotalNodesGlobal
                  << " nodes but " << filename << " has "
                  << nEntries << " entries." << std::endl;
        MPI_Abort(PETSC_COMM_WORLD, 1);
      }
    }
    nEntriesMPI = nEntries;

    if (serialVelocity.m_dim1 != nEntries || serialVelocity.m_dim2 != DIM)
    {
      serialVelocity.resize(nEntries, DIM);
    }

    if (sizeof(PetscReal) == sizeof(double))
    {
      fin.read((char *) serialVelocity.front(),
               nEntries * DIM * sizeof(double));
      fin.close();
    }
    else // Need to convert to doubles before writing
    {
      Array2d<double> arrayDoubles(nEntries, DIM);
      fin.read((char *) arrayDoubles.front(), nEntries * DIM * sizeof(double));
      for (PetscInt i = 0; i < nEntries; i++)
      {
        for (int j = 0; j < DIM; j++)
        {
          serialVelocity(i, j) = arrayDoubles(i, j);
        }
      }
    }
    fin.close();
  }
  MPI_Bcast(&nEntriesMPI, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  nEntries = nEntriesMPI;
  if (m_MPIrank > 0)
  {
    if (serialVelocity.m_dim1 != nEntries || serialVelocity.m_dim2 != DIM)
    {
      serialVelocity.resize(nEntries, DIM);
    }
  }
  MPI_Bcast(serialVelocity.front(), nEntriesMPI * DIM, MPIU_REAL, 0,
            PETSC_COMM_WORLD);

  // Map serial values to local
  PetscInt localVelocitySize;
  if (m_velocityBasis.m_order == 1)
  {
    localVelocitySize = m_mesh.m_nCornerNodes;
  }
  else if (m_velocityBasis.m_order == 2)
  {
    localVelocitySize = m_mesh.m_nTotalNodes;
  }
  if (velocity.m_dim1 != localVelocitySize || velocity.m_dim2 != DIM)
  {
    velocity.resize(localVelocitySize, DIM);
  }

  PetscInt idx = 0;

  // Owned corner nodes
  for (PetscInt i = 0; i < m_mesh.m_nOwnedCornerNodes; i++)
  {
    PetscInt serialIdx = m_mesh.m_parallelToSerialMap(idx);
    for (int j = 0; j < DIM; j++)
    {
      velocity(idx, j) = serialVelocity(serialIdx, j);
    }
    idx++;
  }

  // If velocity field is order 2, get owned order 2 nodes else skip them
  PetscInt nOwnedOrder2Nodes = m_mesh.m_nOwnedNodes
                               - m_mesh.m_nOwnedCornerNodes;
  PetscInt order1Shift = 0;
  if (m_velocityBasis.m_order == 2)
  {
    for (PetscInt i = 0; i < nOwnedOrder2Nodes; i++)
    {
      PetscInt serialIdx = m_mesh.m_parallelToSerialMap(idx);
      for (int j = 0; j < DIM; j++)
      {
        velocity(idx, j) = serialVelocity(serialIdx, j);
      }
      idx++;
    }
  }
  else
  {
    order1Shift = nOwnedOrder2Nodes;
  }

  // Ghost corner nodes
  PetscInt nGhostCornerNodes = m_mesh.m_nCornerNodes
                               - m_mesh.m_nOwnedCornerNodes;
  for (PetscInt i = 0; i < nGhostCornerNodes; i++)
  {
    PetscInt serialIdx = m_mesh.m_parallelToSerialMap(idx + order1Shift);
    for (int j = 0; j < DIM; j++)
    {
      velocity(idx, j) = serialVelocity(serialIdx, j);
    }
    idx++;
  }

  // Ghost order 2 nodes
  if (m_velocityBasis.m_order == 2)
  {
    PetscInt nGhostOrder2Nodes = m_mesh.m_nTotalNodes
                                 - m_mesh.m_nOwnedNodes
                                 - m_mesh.m_nCornerNodes
                                 + m_mesh.m_nOwnedCornerNodes;
    for (PetscInt i = 0; i < nGhostOrder2Nodes; i++)
    {
      PetscInt serialIdx = m_mesh.m_parallelToSerialMap(idx);
      for (int j = 0; j < DIM; j++)
      {
        velocity(idx, j) = serialVelocity(serialIdx, j);
      }
      idx++;
    }
  }
}


void FemAdvDiffSolver::setUniformVelocity(Array2d<PetscReal> &velocity,
                                          PetscReal value[DIM])
{
  PetscInt localVelocitySize;
  if (m_velocityBasis.m_order == 1)
  {
    localVelocitySize = m_mesh.m_nCornerNodes;
  }
  else if (m_velocityBasis.m_order == 2)
  {
    localVelocitySize = m_mesh.m_nTotalNodes;
  }
  if (velocity.m_dim1 != localVelocitySize || velocity.m_dim2 != DIM)
  {
    velocity.resize(localVelocitySize, DIM);
  }

  for (PetscInt i = 0; i < localVelocitySize; i++)
  {
    for (int j = 0; j < DIM; j++)
    {
      velocity(i, j) = value[j];
    }
  }
}


FemAdvDiffSolverSteady::FemAdvDiffSolverSteady(Mesh &mesh,
                                               std::vector<Vec> &solution)
  : FemAdvDiffSolver(mesh, solution)
{
}


FemAdvDiffSolverSteady::FemAdvDiffSolverSteady(Mesh &mesh,
                                               std::vector<Vec> &solution,
                                               int basisOrder,
                                               int velocityBasisOrder,
                                               bool stabilized,
                                               bool store_dPhidX,
                                               bool verbose)
  : FemAdvDiffSolver(mesh,
                     solution,
                     basisOrder,
                     velocityBasisOrder,
                     stabilized,
                     store_dPhidX,
                     verbose)
{
}


void FemAdvDiffSolverSteady::buildLHS()
{
  if (m_diffusivity < 0.)
  {
    if (m_MPIrank == 0)
    {
      std::cout << "Must initialize diffusivity before building LHS."
                << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  buildDiffusiveStiffnessMatrix(m_LHS, m_diffusivity, true);

  Array3d<PetscReal> *dPhidX;
  Array3d<PetscReal> dPhidXlocal;
  Array1d<PetscReal> *laplacePhi;
  Array1d<PetscReal> laplacePhiLocal;

  if (m_store_dPhidX == false)
  {
    dPhidXlocal.resize(m_integrationScheme.m_nIntegrationPoints,
                       m_basis.m_nBasisFunctions,
                       DIM);
  }

  if (m_store_dPhidX == false && m_stabilized == true && m_basis.m_order == 2)
  {
    laplacePhiLocal.resize(m_basis.m_nBasisFunctions);
  }

  PetscReal weight;
  Array2d<PetscReal> K_local(m_enon, m_enon);

  Array1d<PetscReal> velocity(DIM);

  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "Building total stiffness and mass matrices..." << std::endl;
  }
  int pct = 0;
  for (PetscInt el = 0; el < m_mesh.m_nElements; el++)
  {
    if (m_verbose && m_MPIrank == 0)
    {
      if (el * 100. / m_mesh.m_nElements > pct)
      {
        std::cout << "\r" << pct << "%" << std::flush;
        pct += 10;
      }
    }

    K_local = 0.;

    if (m_store_dPhidX == true)
    {
      dPhidX = &m_dPhidX(el);

      if (m_stabilized == true && m_basis.m_order == 2)
      {
        laplacePhi = &m_laplacePhi(el);
      }
    }
    else
    {
      m_basis.calc_dPhidX(el, m_integrationScheme, dPhidXlocal);
      dPhidX = &dPhidXlocal;
      if (m_stabilized == true && m_basis.m_order == 2)
      {
        m_basis.calc_laplacePhi(el, laplacePhiLocal);
        laplacePhi = &laplacePhiLocal;
      }
    }


    for (int intPt = 0;
         intPt < m_integrationScheme.m_nIntegrationPoints;
         intPt++)
    {
      // Calculate velocity
      velocity = 0.;
      for (PetscInt j = 0; j < m_velocityBasis.m_nBasisFunctions; j++)
      {
        for (PetscInt k = 0; k < DIM; k++)
        {
          PetscInt velocityIdx = m_mesh.m_connectivity(el, j);
          if (m_velocityBasis.m_order == 1 &&
              velocityIdx >= m_mesh.m_nOwnedCornerNodes)
          {
            velocityIdx -= m_mesh.m_nOwnedNodes - m_mesh.m_nOwnedCornerNodes;
          }
          velocity(k) += m_velocityPhi(intPt, j)
                         * m_currentVelocity(velocityIdx, k);
        }
      }

      PetscReal u_dot_u = 0;
      for (int i = 0; i < DIM; i++)
      {
        u_dot_u += velocity(i) * velocity(i);
      }
      PetscReal u_dot_G_u = 0.;
      PetscReal G_dot_G = 0.; // This could be precomputed...
      if (m_stabilized == true)
      {
        for (int i = 0; i < DIM; i++)
        {
          for (int j = 0; j < DIM; j++)
          {
            u_dot_G_u += velocity(i) * velocity(j)
                         * m_mesh.m_metricTensor(el, i, j);
            G_dot_G += m_mesh.m_metricTensor(el, i, j)
                       * m_mesh.m_metricTensor(el, i, j);
          }
        }
      }
      PetscReal tau_m =
             1. / std::sqrt(u_dot_G_u
                            + DIM * DIM * std::pow(m_diffusivity, 2)
                              * std::pow(m_basis.m_order, 4)
                              * G_dot_G);

      weight = m_integrationScheme.m_gaussWeights(intPt);
      for (int i = 0; i < m_enon; i++)
      {
        for (int j = 0; j < m_enon; j++)
        {
          for (int k = 0; k < DIM; k++)
          {
            // K_advective
            K_local(i, j) += m_phi(intPt, i) * (*dPhidX)(intPt, j, k)
                             * velocity(k) * weight;
          }

          if (m_stabilized == true)
          {
            PetscReal uDotGradPhiI = 0.;
            PetscReal uDotGradPhiJ = 0.;
            for (int k = 0; k < DIM; k++)
            {
              uDotGradPhiI += velocity(k) * (*dPhidX)(intPt, i, k);
              uDotGradPhiJ += velocity(k) * (*dPhidX)(intPt, j, k);
            }

            // K_advective_stabilized
            K_local(i, j) += uDotGradPhiI * uDotGradPhiJ * tau_m * weight;

            if (m_basis.m_order == 2)
            {
              // K_diffusive_stabilized
              K_local(i, j) -= uDotGradPhiI * tau_m * m_diffusivity
                               * (*laplacePhi)(j);
            }
          }
        }
      }
    }

    // Multiply by Jacobian
    PetscReal jac = m_mesh.m_jacobian(el);
    for (int i = 0; i < m_enon; i++)
    {
      for (int j = 0; j < m_enon; j++)
      {
        K_local(i, j) *= jac;
      }
    }

    // Send to global matrix
    MatSetValues(m_LHS, m_enon, &m_mesh.m_connectivityGlobal(el, 0),
                 m_enon, &m_mesh.m_connectivityGlobal(el, 0), K_local.front(),
                 ADD_VALUES);

  }
  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "\rdone." << std::endl;
  }

  MatAssemblyBegin(m_LHS, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_LHS, MAT_FINAL_ASSEMBLY);
}


void FemAdvDiffSolverSteady::buildRhs()
{
  applyNeumannBCs(m_rhs, true);
  VecAssemblyBegin(m_rhs);
  VecAssemblyEnd(m_rhs);
}


void FemAdvDiffSolverSteady::solve()
{
  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "Solving system." << std::endl;
  }
  KSPSetOperators(m_ksp, m_LHS, m_LHS);

  // This is hacky, but multiple solutions don't really make sense for steady.
  KSPSolve(m_ksp, m_rhs, m_solution[0]);

  // Get diagnostics
  KSPConvergedReason reason;
  PetscInt nIterations;
  KSPGetConvergedReason(m_ksp, &reason);
  KSPGetIterationNumber(m_ksp, &nIterations);
  if (reason < 0)
  {
    if (m_MPIrank == 0)
    {
      std::cout << "Iterative solver failed to converge. Aborting."
                << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  else
  {
    if (m_verbose && m_MPIrank == 0)
    {
      std::cout << "Iterative solver converged in " << nIterations
                << " iterations." << std::endl;
    }
  }
}


FemAdvDiffSolverUnsteady::FemAdvDiffSolverUnsteady(Mesh &mesh,
                                                   std::vector<Vec> &solution)
  : FemAdvDiffSolver(mesh, solution)
{
}


FemAdvDiffSolverUnsteady::FemAdvDiffSolverUnsteady(Mesh &mesh,
                                                   std::vector<Vec> &solution,
                                                   int basisOrder,
                                                   int velocityBasisOrder,
                                                   bool stabilized,
                                                   bool store_dPhidX,
                                                   bool verbose)
  : FemAdvDiffSolver(mesh,
                     solution,
                     basisOrder,
                     velocityBasisOrder,
                     stabilized,
                     store_dPhidX,
                     verbose),
    m_leftVelIdx(-1), m_rightVelIdx(-1)
{
}


void FemAdvDiffSolverUnsteady::updateVelocity(PetscReal time)
{
  PetscReal locInVelFiles = std::fmod(time, m_velFileDt * m_nVelFiles);
  PetscInt newLeftIdx = std::floor(locInVelFiles / m_velFileDt)
                        * m_velFileInterval
                        + m_velFileStart;
  PetscInt newRightIdx = newLeftIdx + m_velFileInterval;
  if (newRightIdx > m_velFileStart + m_velFileInterval * (m_nVelFiles - 1))
  {
    newRightIdx = m_velFileStart;
  }

  if (newLeftIdx != m_leftVelIdx || newRightIdx != m_rightVelIdx)
  {
    if (newLeftIdx == m_rightVelIdx)
    {
      m_leftVel.swap(m_rightVel);
    }
    else
    {
      loadVelocityFromBin(m_leftVel,
                          m_velFilenameRoot + "."
                          + toString(newLeftIdx)
                          + ".bin");
    }
    loadVelocityFromBin(m_rightVel,
                        m_velFilenameRoot + "."
                        + toString(newRightIdx)
                        + ".bin");
    m_leftVelIdx = newLeftIdx;
    m_rightVelIdx = newRightIdx;
  }
  PetscReal alpha = std::fmod(locInVelFiles, m_velFileDt) / m_velFileDt;
  if (m_currentVelocity.m_dim1 != m_mesh.m_nTotalNodes
      || m_currentVelocity.m_dim2 != DIM)
  {
    m_currentVelocity.resize(m_mesh.m_nTotalNodes, DIM);
  }

  PetscInt localVelocitySize;
  if (m_velocityBasis.m_order == 1)
  {
    localVelocitySize = m_mesh.m_nCornerNodes;
  }
  else if (m_velocityBasis.m_order == 2)
  {
    localVelocitySize = m_mesh.m_nTotalNodes;
  }

  for (PetscInt i = 0; i < localVelocitySize; i++)
  {
    for (PetscInt j = 0; j < DIM; j++)
    {
      m_currentVelocity(i,j) = m_leftVel(i,j) * (1. - alpha)
                               + m_rightVel(i,j) * alpha;
    }
  }
}


FemAdvDiffSolverGenAlpha::FemAdvDiffSolverGenAlpha(Mesh &mesh,
                                                   std::vector<Vec> &solution)
    : FemAdvDiffSolverUnsteady(mesh, solution)
{
}


FemAdvDiffSolverGenAlpha::FemAdvDiffSolverGenAlpha(Mesh &mesh,
                                                   std::vector<Vec> &solution,
                                                   int basisOrder,
                                                   int velocityBasisOrder,
                                                   bool stabilized,
                                                   bool store_dPhidX,
                                                   bool verbose) :
  FemAdvDiffSolverUnsteady(mesh, solution)
{
  initialize(basisOrder,
             velocityBasisOrder,
             stabilized,
             store_dPhidX,
             verbose);
}


void FemAdvDiffSolverGenAlpha::initialize(int basisOrder,
                                          int velocityBasisOrder,
                                          bool stabilized,
                                          bool store_dPhidX,
                                          bool verbose)
{
  FemAdvDiffSolver::initialize(basisOrder,
                               velocityBasisOrder,
                               stabilized,
                               store_dPhidX,
                               verbose);
  // Set up vectors
  m_cdot.resize(m_nSpecies);
  m_cprev.resize(m_nSpecies);
  for (int i = 0; i < m_nSpecies; i++)
  {
    VecDuplicate(m_solution[i], &m_cdot[i]);
    VecDuplicate(m_solution[i], &m_cprev[i]);
    // VecCreate(PETSC_COMM_WORLD, &m_cdot[i]);
    // VecSetSizes(m_cdot[i], m_localDOF, m_globalDOF);
    // if (m_MPIsize == 1)
    // {
    //   VecSetType(m_cdot[i], VECSEQ);
    // }
    // else
    // {
    //   VecSetType(m_cdot[i], VECMPI);
    // }
    // VecSetFromOptions(m_cdot[i]);

    VecSet(m_cdot[i], 0.);
    //
    // VecCreate(PETSC_COMM_WORLD, &m_cprev[i]);
    // VecSetSizes(m_cprev[i], m_localDOF, m_globalDOF);
    // if (m_MPIsize == 1)
    // {
    //   VecSetType(m_cprev[i], VECSEQ);
    // }
    // else
    // {
    //   VecSetType(m_cprev[i], VECMPI);
    // }
    // VecSetFromOptions(m_cprev[i]);
    VecCopy(m_solution[i], m_cprev[i]);
  }

  //Set up matrices
  // MatDuplicate(m_LHS, MAT_SHARE_NONZERO_PATTERN, &m_M);
  // MatDuplicate(m_LHS, MAT_SHARE_NONZERO_PATTERN, &m_Kdiff);
  // MatDuplicate(m_LHS, MAT_SHARE_NONZERO_PATTERN, &m_M_total);
  // MatDuplicate(m_LHS, MAT_SHARE_NONZERO_PATTERN, &m_K_total);


  MatCreate(PETSC_COMM_WORLD, &m_M);
  MatSetSizes(m_M, m_localDOF, m_localDOF, m_globalDOF, m_globalDOF);
  MatCreate(PETSC_COMM_WORLD, &m_Kdiff);
  MatSetSizes(m_Kdiff, m_localDOF, m_localDOF, m_globalDOF, m_globalDOF);
  MatCreate(PETSC_COMM_WORLD, &m_M_total);
  MatSetSizes(m_M_total, m_localDOF, m_localDOF, m_globalDOF, m_globalDOF);
  MatCreate(PETSC_COMM_WORLD, &m_K_total);
  MatSetSizes(m_K_total, m_localDOF, m_localDOF, m_globalDOF, m_globalDOF);

  if (m_MPIsize == 1)
  {
    MatSetType(m_M, MATSEQAIJ);
    MatSeqAIJSetPreallocation(m_M, 0,
                              m_mesh.m_nFemDiagonalEntries.front());
    MatSetType(m_Kdiff, MATSEQAIJ);
    MatSeqAIJSetPreallocation(m_Kdiff, 0,
                              m_mesh.m_nFemDiagonalEntries.front());
    MatSetType(m_M_total, MATSEQAIJ);
    MatSeqAIJSetPreallocation(m_M_total, 0,
                              m_mesh.m_nFemDiagonalEntries.front());
    MatSetType(m_K_total, MATSEQAIJ);
    MatSeqAIJSetPreallocation(m_K_total, 0,
                              m_mesh.m_nFemDiagonalEntries.front());
  }
  else
  {
    MatSetType(m_M, MATMPIAIJ);
    MatMPIAIJSetPreallocation(m_M, 0, m_mesh.m_nFemDiagonalEntries.front(),
                              0, m_mesh.m_nFemOffDiagonalEntries.front());
    MatSetType(m_Kdiff, MATMPIAIJ);
    MatMPIAIJSetPreallocation(m_Kdiff, 0, m_mesh.m_nFemDiagonalEntries.front(),
                              0, m_mesh.m_nFemOffDiagonalEntries.front());
    MatSetType(m_M_total, MATMPIAIJ);
    MatMPIAIJSetPreallocation(m_M_total, 0,
                              m_mesh.m_nFemDiagonalEntries.front(),
                              0, m_mesh.m_nFemOffDiagonalEntries.front());
    MatSetType(m_K_total, MATMPIAIJ);
    MatMPIAIJSetPreallocation(m_K_total, 0,
                              m_mesh.m_nFemDiagonalEntries.front(),
                              0, m_mesh.m_nFemOffDiagonalEntries.front());
  }

  buildDiffusiveStiffnessMatrix(m_Kdiff, 1., true);
  buildMassMatrix(m_M, true);
  MatAssemblyBegin(m_Kdiff, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_Kdiff, MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(m_M, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_M, MAT_FINAL_ASSEMBLY);
  // if (basisOrder >= 2)
  // {
  //   // computeLaplacePhi();
  // }


  // Set m_LHS to the proper size
  // MatDuplicate(m_M, MAT_SHARE_NONZERO_PATTERN, &m_M_total);
  // MatDuplicate(m_M, MAT_SHARE_NONZERO_PATTERN, &m_K_total);
  // MatDuplicate(m_M, MAT_SHARE_NONZERO_PATTERN, &m_LHS);
  buildMassMatrix(m_M_total, true);
  MatAssemblyBegin(m_M_total, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_M_total, MAT_FINAL_ASSEMBLY);
  buildMassMatrix(m_K_total, true);
  MatAssemblyBegin(m_K_total, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_K_total, MAT_FINAL_ASSEMBLY);
  buildMassMatrix(m_LHS, true);
  MatAssemblyBegin(m_LHS, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_LHS, MAT_FINAL_ASSEMBLY);

}


FemAdvDiffSolverGenAlpha::~FemAdvDiffSolverGenAlpha()
{
  MatDestroy(&m_M);
  MatDestroy(&m_Kdiff);
  MatDestroy(&m_M_total);
  MatDestroy(&m_K_total);
  for (int i = 0; i < m_nSpecies; i++)
  {
    VecDestroy(&m_cdot[i]);
    VecDestroy(&m_cprev[i]);
  }
}


PetscInt FemAdvDiffSolverGenAlpha::takeTimeStep(int species)
{
  PetscInt nIterations;

  buildRHS(species);
  applyBCs(species);
  nIterations = solve(species);

  return nIterations;
}


// PetscInt FemAdvDiffSolverGenAlpha::takeTimeStep()
// {
//   if (m_verbose && m_MPIrank == 0)
//   {
//     std::cout << "Updating velocity field." << std::endl;
//   }
//   updateVelocity();
//
//   buildLHS();
//
//   PetscInt nIterations;
//   for (int i = 0; i < m_nSpecies; i++)
//   {
//     buildRHS(i);
//     applyBCs(i);
//     nIterations = solve(i);
//
//     PetscReal maxval;
//     PetscReal minval;
//     VecMax(m_solution[i], NULL, &maxval);
//     VecMin(m_solution[i], NULL, &minval);
//
//     if (m_MPIrank == 0)
//     {
//       std::cout << m_currentTstep << " "
//                 << m_t << " "
//                 << i << " "
//                 << nIterations << " "
//                 << maxval << " "
//                 << minval << " "
//                 << std::endl;
//
//     }
//   }
//
//   m_t += m_dt;
//   m_currentTstep++;
//
//   if (m_currentTstep % m_saveInterval == 0)
//   {
//     writeSolutionToBin(m_outputFilenameRoot, m_currentTstep);
//   }
//
//   return nIterations;
// }


void FemAdvDiffSolverGenAlpha::buildLHS()
{
  if (m_rhoInfinity < 0.)
  {
    if (m_MPIrank == 0)
    {
      std::cout << "Must initialize rho_infinity before using."
                << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  PetscReal alpha_m = .5 * (3. - m_rhoInfinity) / (1. + m_rhoInfinity);
  PetscReal alpha_f = 1. / (1. + m_rhoInfinity);
  PetscReal gamma = .5 + alpha_m - alpha_f;

  buildMatrices(m_M_total, m_K_total);

  MatZeroEntries(m_LHS);
  MatAXPY(m_LHS, 1., m_M_total, SAME_NONZERO_PATTERN);
  // MatDuplicate(m_M_total, MAT_COPY_VALUES, &m_LHS);
  MatScale(m_LHS, alpha_m / gamma / m_dt);
  MatAXPY(m_LHS, alpha_f, m_K_total, SAME_NONZERO_PATTERN);
}


void FemAdvDiffSolverGenAlpha::buildRHS(int species)
{
  if (m_rhoInfinity < 0.)
  {
    if (m_MPIrank == 0)
    {
      std::cout << "Must initialize rho_infinity before using."
                << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  VecSet(m_rhs, 0.);
  PetscReal alpha_m = .5 * (3. - m_rhoInfinity) / (1. + m_rhoInfinity);
  PetscReal alpha_f = 1. / (1. + m_rhoInfinity);
  PetscReal gamma = .5 + alpha_m - alpha_f;

  Vec tmp;
  VecCreate(PETSC_COMM_WORLD, &tmp);
  VecSetSizes(tmp, m_localDOF, m_globalDOF);
  if (m_MPIsize == 1)
  {
    VecSetType(tmp, VECSEQ);
  }
  else
  {
    VecSetType(tmp, VECMPI);
  }
  VecSetFromOptions(tmp);

  VecCopy(m_solution[species], tmp);
  VecScale(tmp, alpha_f - 1.);
  MatMultAdd(m_K_total, tmp, m_rhs, m_rhs);

  VecCopy(m_solution[species], tmp);
  VecScale(tmp, alpha_m / gamma / m_dt);
  MatMultAdd(m_M_total, tmp, m_rhs, m_rhs);

  VecCopy(m_cdot[species], tmp);
  VecScale(tmp, alpha_m / gamma - 1.);
  MatMultAdd(m_M_total, tmp, m_rhs, m_rhs);

  VecDestroy(&tmp);
}


void FemAdvDiffSolverGenAlpha::applyBCs(int species)
{
  applyNeumannBCs(m_rhs, false, species);
  VecAssemblyBegin(m_rhs);
  VecAssemblyEnd(m_rhs);

  applyDirichletBCs(species);

  applyOutletBCs(species);
}


PetscInt FemAdvDiffSolverGenAlpha::solve(int species)
{
  if (m_rhoInfinity < 0.)
  {
    if (m_MPIrank == 0)
    {
      std::cout << "Must initialize rho_infinity before using."
                << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  PetscReal alpha_m = .5 * (3. - m_rhoInfinity) / (1. + m_rhoInfinity);
  PetscReal alpha_f = 1. / (1. + m_rhoInfinity);
  PetscReal gamma = .5 + alpha_m - alpha_f;

  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "Solving for species " << species << "." << std::endl;
  }

  VecCopy(m_solution[species], m_cprev[species]);

  KSPSolve(m_ksp, m_rhs, m_solution[species]);

  // Get diagnostics
  KSPConvergedReason reason;
  PetscInt nIterations;
  KSPGetConvergedReason(m_ksp, &reason);
  KSPGetIterationNumber(m_ksp, &nIterations);
  if (reason < 0)
  {
    if (m_MPIrank == 0)
    {
      std::cout << "Iterative solver failed to converge. Aborting."
                << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }
  else
  {
    if (m_verbose && m_MPIrank == 0)
    {
      std::cout << "Iterative solver converged in " << nIterations
                << " iterations." << std::endl;
    }
  }

  // Calculate new values
  VecScale(m_cdot[species], 1. - 1. / gamma);
  VecAXPY(m_cdot[species], 1. / gamma / m_dt, m_solution[species]);
  VecAXPY(m_cdot[species], -1. / gamma / m_dt, m_cprev[species]);

  return nIterations;
}


void FemAdvDiffSolverGenAlpha::buildMatrices(Mat &M_total, Mat &K_total)
{
  if (m_diffusivity < 0.)
  {
    if (m_MPIrank == 0)
    {
      std::cout << "Must initialize diffusivity before building LHS."
                << std::endl;
    }
    MPI_Abort(PETSC_COMM_WORLD, 1);
  }

  // Initialize with values from constant mass and diffusive stiffness matrices
  MatCopy(m_M, M_total, SAME_NONZERO_PATTERN);
  MatZeroEntries(K_total);
  MatAXPY(K_total, m_diffusivity, m_Kdiff, SAME_NONZERO_PATTERN);

  Array3d<PetscReal> *dPhidX;
  Array3d<PetscReal> dPhidXlocal;
  Array1d<PetscReal> *laplacePhi;
  Array1d<PetscReal> laplacePhiLocal;

  if (m_store_dPhidX == false)
  {
    dPhidXlocal.resize(m_integrationScheme.m_nIntegrationPoints,
                       m_basis.m_nBasisFunctions,
                       DIM);
  }

  if (m_store_dPhidX == false && m_stabilized == true && m_basis.m_order == 2)
  {
    laplacePhiLocal.resize(m_basis.m_nBasisFunctions);
  }

  PetscReal weight;
  Array2d<PetscReal> K_local(m_enon, m_enon);
  Array2d<PetscReal> M_local(m_enon, m_enon);

  Array1d<PetscReal> velocity(DIM);

  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "Building stiffness and mass matrices..." << std::endl;
  }
  int pct = 0;
  for (PetscInt el = 0; el < m_mesh.m_nElements; el++)
  {
    if (m_verbose && m_MPIrank == 0)
    {
      if (el * 100. / m_mesh.m_nElements >= pct)
      {
        std::cout << "\r" << pct << "%" << std::flush;
        pct += 10;
      }
    }

    K_local = 0.;
    M_local = 0.;

    if (m_store_dPhidX == true)
    {
      dPhidX = &m_dPhidX(el);

      if (m_stabilized == true && m_basis.m_order == 2)
      {
        laplacePhi = &m_laplacePhi(el);
      }
    }
    else
    {
      m_basis.calc_dPhidX(el, m_integrationScheme, dPhidXlocal);
      dPhidX = &dPhidXlocal;
      if (m_stabilized == true && m_basis.m_order == 2)
      {
        m_basis.calc_laplacePhi(el, laplacePhiLocal);
        laplacePhi = &laplacePhiLocal;
      }
    }


    for (int intPt = 0;
         intPt < m_integrationScheme.m_nIntegrationPoints;
         intPt++)
    {
      // Calculate velocity
      velocity = 0.;
      for (PetscInt j = 0; j < m_velocityBasis.m_nBasisFunctions; j++)
      {
        for (PetscInt k = 0; k < DIM; k++)
        {
          PetscInt velocityIdx = m_mesh.m_connectivity(el, j);
          if (m_velocityBasis.m_order == 1 &&
              velocityIdx >= m_mesh.m_nOwnedCornerNodes)
          {
            velocityIdx -= m_mesh.m_nOwnedNodes - m_mesh.m_nOwnedCornerNodes;
          }
          velocity(k) += m_velocityPhi(intPt, j)
                         * m_currentVelocity(velocityIdx, k);
        }
      }

      PetscReal u_dot_u = 0;
      for (int i = 0; i < DIM; i++)
      {
        u_dot_u += velocity(i) * velocity(i);
      }
      PetscReal u_dot_G_u = 0.;
      PetscReal G_dot_G = 0.; // This could be precomputed...
      PetscReal tau_m = 0.;
      if (m_stabilized == true)
      {
        for (int i = 0; i < DIM; i++)
        {
          for (int j = 0; j < DIM; j++)
          {
            u_dot_G_u += velocity(i) * velocity(j)
                         * m_mesh.m_metricTensor(el, i, j);
            G_dot_G += m_mesh.m_metricTensor(el, i, j)
                       * m_mesh.m_metricTensor(el, i, j);
          }
        }
        tau_m = 1. / std::sqrt(4. / (m_dt * m_dt)
                               + u_dot_G_u
                               + DIM * DIM * std::pow(m_diffusivity, 2)
                                 * std::pow(m_basis.m_order, 4)
                                 * G_dot_G);
      }

      weight = m_integrationScheme.m_gaussWeights(intPt);
      for (int i = 0; i < m_enon; i++)
      {
        for (int j = 0; j < m_enon; j++)
        {
          for (int k = 0; k < DIM; k++)
          {
            // K_advective
            K_local(i, j) += m_phi(intPt, i) * (*dPhidX)(intPt, j, k)
                             * velocity(k) * weight;
          }

          if (m_stabilized == true)
          {
            PetscReal uDotGradPhiI = 0.;
            PetscReal uDotGradPhiJ = 0.;
            for (int k = 0; k < DIM; k++)
            {
              uDotGradPhiI += velocity(k) * (*dPhidX)(intPt, i, k);
              uDotGradPhiJ += velocity(k) * (*dPhidX)(intPt, j, k);
            }

            // K_advective_stabilized
            K_local(i, j) += uDotGradPhiI * uDotGradPhiJ * tau_m * weight;

            // M_stabilized
            M_local(i, j) += uDotGradPhiI * m_phi(intPt, j) * tau_m * weight;

            if (m_basis.m_order == 2)
            {
              // K_diffusive_stabilized
              K_local(i, j) -= uDotGradPhiI * tau_m * m_diffusivity
                               * (*laplacePhi)(j) * weight;
            }
          }
        }
      }
    }

    // Multiply by Jacobian
    PetscReal jac = m_mesh.m_jacobian(el);
    for (int i = 0; i < m_enon; i++)
    {
      for (int j = 0; j < m_enon; j++)
      {
        K_local(i, j) *= jac;
        if (m_stabilized == true)
        {
          M_local(i, j) *= jac;
        }
      }
    }

    // Send to global matrix
    MatSetValues(K_total, m_enon, &m_mesh.m_connectivityGlobal(el, 0),
                 m_enon, &m_mesh.m_connectivityGlobal(el, 0), K_local.front(),
                 ADD_VALUES);

    if (m_stabilized == true)
    {
      // Send to global matrix
      MatSetValues(M_total, m_enon, &m_mesh.m_connectivityGlobal(el, 0),
                   m_enon, &m_mesh.m_connectivityGlobal(el, 0), M_local.front(),
                   ADD_VALUES);
    }
  }
  if (m_verbose && m_MPIrank == 0)
  {
    std::cout << "\rdone." << std::endl;
  }

  MatAssemblyBegin(K_total, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(K_total, MAT_FINAL_ASSEMBLY);

  MatAssemblyBegin(M_total, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(M_total, MAT_FINAL_ASSEMBLY);
}

void FemAdvDiffSolverGenAlpha::applyIC(int species)
{
  VecCopy(m_solution[species], m_cprev[species]);
}


