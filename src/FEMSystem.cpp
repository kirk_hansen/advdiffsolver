#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>
#include <stdint.h>
#include <string>

#include "petscksp.h"

#include "mesh.hpp"
#include "vectorND.hpp"
#include "FEMSystem.hpp"
#include "array_types.hpp"
#include "basis.hpp"
#include "ioutilities.hpp"

#ifndef DIM
#define DIM 3
#endif

FEMSystem::FEMSystem(Mesh &mesh) : m_mesh(mesh)
{
  PetscErrorCode ierr;

  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&m_MPIrank);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&m_MPIsize);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
}



PoissonSolverSteady::~PoissonSolverSteady()
{
  MatDestroy(&m_A);
  VecDestroy(&m_x);
  VecDestroy(&m_b);
}


PoissonSolverSteady::PoissonSolverSteady(Mesh &mesh, int order) :
  FEMSystem(mesh), m_basis(mesh, order), m_integrationScheme(order)
{
  if (m_mesh.m_nTotalNodes == 0)
  {
    std::cout << "Must load mesh before setting basis. Aborting."
              << std::endl;
    exit(0);
  }
  m_localDOF = m_mesh.m_nOwnedNodes;
  m_globalDOF = m_mesh.m_nTotalNodesGlobal;

  if (order == 1)
  {
    m_enon = DIM + 1;
  }
  else if (order == 2)
  {
    if (m_mesh.m_nTotalNodes - m_mesh.m_nCornerNodes == 0)
    {
      std::cout << "Must load order 2 mesh for order 2 system. Aborting."
                << std::endl;
      exit(0);
    }

    if (DIM == 1)
    {
      m_enon = 3;
    }
    if (DIM == 2)
    {
      m_enon = 6;
    }
    if (DIM == 3)
    {
      m_enon = 10;
    }
  }
  else
  {
    std::cout << "Order " << order << " not available." << std::endl;
    exit(0);
  }
}


void PoissonSolverSteady::setUpSolver()
{
  PetscErrorCode ierr;

  PetscMPIInt size;
  PetscMPIInt rank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  VecCreate(PETSC_COMM_WORLD, &m_x);
  VecSetSizes(m_x, m_localDOF, m_globalDOF);
  if (size == 1)
  {
    VecSetType(m_x, VECSEQ);
  }
  else
  {
    VecSetType(m_x, VECMPI);
  }
  VecSetFromOptions(m_x);
  VecDuplicate(m_x, &m_b);
  VecSet(m_b, 0.);

  // Array1d<PetscInt> nConnectedLocalNodes(m_mesh.m_nOwnedNodes);
  // Array1d<PetscInt> nConnectedNonLocalNodes(m_mesh.m_nOwnedNodes);
  // getNodeConnectivity(nConnectedLocalNodes, nConnectedNonLocalNodes);


  MatCreate(PETSC_COMM_WORLD, &m_A);
  MatSetSizes(m_A, m_localDOF, m_localDOF, m_globalDOF, m_globalDOF);

  if (size == 1)
  {
    MatSetType(m_A, MATSEQAIJ);

    MatSeqAIJSetPreallocation(m_A, 0,
                              m_mesh.m_nFemDiagonalEntries.front());

  //   MatCreateSeqAIJ(PETSC_COMM_WORLD, m_localDOF, m_localDOF, 0,
  //                   &nConnectedLocalNodes(0), &m_A);
  }
  else
  {
    MatSetType(m_A, MATMPIAIJ);
    MatMPIAIJSetPreallocation(m_A, 0, m_mesh.m_nFemDiagonalEntries.front(),
                              0, m_mesh.m_nFemOffDiagonalEntries.front());
    // MatCreateMPIAIJ(PETSC_COMM_WORLD, m_localDOF, m_localDOF, PETSC_DETERMINE,
    //                 PETSC_DETERMINE, 0, nConnectedLocalNodes.front(), 0,
    //                 nConnectedNonLocalNodes.front(), &m_A);
  }
  // MatSetUp(m_A);
  // Should be fixed to preallocate memory
  // MatSetOption(m_A, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);

  MatZeroEntries(m_A);

  PetscInt matStart, matStop;
  MatGetOwnershipRange(m_A, &matStart, &matStop);

  if (m_mesh.m_jacobian.m_size == 0)
  {
    std::cout << "Need to calculate element jacobians in mesh before setting "
              << "up FEM problem." << std::endl;
    exit(0);
  }
}


void PoissonSolverSteady::buildLHS()
{
  Array3d<PetscReal> dPhidX;

  PetscReal weight;
  Array2d<PetscReal> A_local;
  A_local.resize(m_enon, m_enon);

  PetscInt m = m_enon;
  PetscInt n = m_enon;

  for (PetscInt el = 0; el < m_mesh.m_nElements; el++)
  {
    if (m_MPIrank == 0)
    {
      if (el % 1000 == 0) std::cout << "Element " << el << " of "
                                    << m_mesh.m_nElements << std::endl;
    }
    A_local = 0.;
    // // Zero out local matrix
    // for (int i = 0; i < m_enon; i++)
    // {
    //   for (int j = 0; j < m_enon; j++)
    //   {
    //     A_local(i, j) = 0.;
    //   }
    // }

    m_basis.calc_dPhidX(el, m_integrationScheme, dPhidX);

    for (int i = 0;
         i < m_integrationScheme.m_nIntegrationPoints;
         i++)
    {
      weight = m_integrationScheme.m_gaussWeights(i);
      for (int j = 0; j < m_enon; j++)
      {
        for (int k = 0; k < m_enon; k++)
        {
          for (int m = 0; m < DIM; m++)
          {
            // This could be a little bit more efficient maybe if weight was
            // not multiplied by each dimension.
            A_local(j, k) += dPhidX(i, j, m) * dPhidX(i, k, m) * weight;
          }
        }
      }
    }

    // Multiply by Jacobian
    PetscReal jac = m_mesh.m_jacobian(el);
    for (int i = 0; i < m_enon; i++)
    {
      for (int j = 0; j < m_enon; j++)
      {
        A_local(i, j) *= jac;
      }
    }

    // Send to global matrix
    MatSetValues(m_A, m, &m_mesh.m_connectivityGlobal(el, 0),
                 n, &m_mesh.m_connectivityGlobal(el, 0), A_local.front(),
                 ADD_VALUES);

  }

  // Assemble
  MatAssemblyBegin(m_A, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_A, MAT_FINAL_ASSEMBLY);
}


void PoissonSolverSteady::setRHS(PetscReal source)
{
  Array2d<PetscReal> phi;
  m_basis.calc_phi(m_integrationScheme, phi);

  Array1d<PetscReal> b_local(m_enon);

  for (PetscInt el = 0; el < m_mesh.m_nElements; el++)
  {
    PetscReal jac = m_mesh.m_jacobian(el);
    // Zero out local matrix
    for (int i = 0; i < m_enon; i++)
    {
      b_local(i) = 0.;
    }
    for (int i = 0;
         i < m_integrationScheme.m_nIntegrationPoints;
         i++)
    {
      PetscReal weight = m_integrationScheme.m_gaussWeights(i);
      for (int j = 0; j < m_enon; j++)
      {
        b_local(j) += phi(i, j) * source * jac * weight;
      }
    }
    VecSetValues(m_b, m_enon, &m_mesh.m_connectivityGlobal(el, 0),
                 b_local.front(), ADD_VALUES);
  }
}


void PoissonSolverSteady::applyDirichletBCs()
{
  Vec solns;
  VecCreate(PETSC_COMM_WORLD, &solns);
  VecSetSizes(solns, m_localDOF, m_globalDOF);
  if (m_MPIsize == 1)
  {
    VecSetType(solns, VECSEQ);
  }
  else
  {
    VecSetType(solns, VECMPI);
  }
  VecSetFromOptions(solns);

  for (int i = 0; i < m_dirichletBCs.size(); i++)
  {
    VecSetValues(solns,
                 m_dirichletBCs[i].m_nodeValues.m_size,
                 m_dirichletBCs[i].m_nodeIdsGlobal.front(),
                 m_dirichletBCs[i].m_nodeValues.front(),
                 INSERT_VALUES);
  }
  VecAssemblyBegin(solns);
  VecAssemblyEnd(solns);

  for (int i = 0; i < m_dirichletBCs.size(); i++)
  {
    MatZeroRows(m_A,
                m_dirichletBCs[i].m_nodeValues.m_size,
                m_dirichletBCs[i].m_nodeIdsGlobal.front(),
                1.,
                solns,
                m_b);
  }
}


void PoissonSolverSteady::applyNeumannBCs()
{
  Array1d<PetscReal> b_local(m_enon);
  for (PetscInt bcidx = 0; bcidx < m_neumannBCs.size(); bcidx++)
  {
    NeumannBC &bc = m_neumannBCs[bcidx];
    for (PetscInt face = 0; face < bc.m_nFaces; face++)
    {
      b_local = 0.;
      for (int i = 0; i < bc.m_intScheme.m_nIntegrationPoints; i++)
      {
        PetscReal &weight = bc.m_intScheme.m_gaussWeights(i);

        // Calculate value of Neumann BC at this integration point
        PetscReal q = 0.;
        for (int j = 0; j < bc.m_enon; j++)
        {
          q += bc.m_nodeValues(bc.m_connectivity(face, j)) * bc.m_phi(i, j);
        }

        // Add contribution
        for (int j = 0; j < bc.m_enon; j++)
        {
          b_local(j) -= bc.m_phi(i, j) * q * weight;
        }
      }

      b_local *= bc.m_jacobians(face);
      VecSetValues(m_b, bc.m_enon, &bc.m_connectivityGlobal(face, 0),
                   b_local.front(), ADD_VALUES);
    }
  }
}


void PoissonSolverSteady::solve()
{
  VecAssemblyBegin(m_b);
  VecAssemblyEnd(m_b);

  KSPCreate(PETSC_COMM_WORLD, &m_ksp);
  KSPSetFromOptions(m_ksp);
  KSPSetOperators(m_ksp, m_A, m_A);
  KSPSetUp(m_ksp);
  KSPSolve(m_ksp, m_b, m_x);
  KSPDestroy(&m_ksp);
}


void PoissonSolverSteady::writeSolutionToBin(std::string filename,
                                             bool order1Only)
{
  // PetscViewer viewer;
  // PetscViewerBinaryOpen(PETSC_COMM_WORLD,
  //                       filename.c_str(),
  //                       FILE_MODE_WRITE,
  //                       &viewer);
  // VecView(m_x, viewer);
  // PetscViewerDestroy(&viewer);

  std::stringstream ss;
  ss << m_MPIrank;
  filename += "." + ss.str();

  uint32_t nEntries;
  if (order1Only == true)
  {
    nEntries = m_mesh.m_nOwnedCornerNodes;
  }
  else
  {
    nEntries = m_mesh.m_nOwnedNodes;
  }

  IoUtilities::writePetscVectorToBin(m_x, filename, nEntries);
}


// void PoissonSolverSteady::getNodeConnectivity(
//                                     Array1d<PetscInt> &nConnectedLocalNodes,
//                                     Array1d<PetscInt> &nConnectedNonLocalNodes)
// {
//   nConnectedLocalNodes.resize(m_mesh.m_nOwnedNodes);
//   nConnectedNonLocalNodes.resize(m_mesh.m_nOwnedNodes);
//   std::vector<std::vector<PetscInt >> connections(m_mesh.m_nOwnedNodes);
//
//   for (int i = 0; i < m_mesh.m_nElements; i++)
//   {
//     for (int j = 0; j < m_mesh.m_enon; j++)
//     {
//       PetscInt n1 = m_mesh.m_connectivity(i, j);
//       if (n1 >= m_mesh.m_nOwnedNodes) break;
//       for (int k = 0; k < m_mesh.m_enon; k++)
//       {
//         if (j != k)
//         {
//           PetscInt n2 = m_mesh.m_connectivity(i, k);
//           bool isDuplicate = false;
//           for (int m = 0; m < connections[n1].size(); m++)
//           {
//             if (n2 == connections[n1][m])
//             {
//               isDuplicate = true;
//               break;
//             }
//           }
//           if (isDuplicate == false)
//           {
//             connections[n1].push_back(n2);
//           }
//         }
//       }
//     }
//   }
//
//   for (int i = 0; i < m_mesh.m_nOwnedNodes; i++)
//   {
//     // +1 to include self
//     nConnectedLocalNodes(i) = 1;
//     nConnectedNonLocalNodes(i) = 0;
//     for (int j = 0; j < connections[i].size(); j++)
//     {
//       if (connections[i][j] < m_mesh.m_nOwnedNodes)
//       {
//         nConnectedLocalNodes(i)++;
//       }
//       else
//       {
//         nConnectedNonLocalNodes(i)++;
//       }
//     }
//   }
// //   nConnectedNodes.resize(m_localDOF);
// //   std::vector<std::vector<PetscInt >> connections(m_localDOF);
// //
// //   for (int i = 0; i < m_mesh.m_nElements; i++)
// //   {
// //     for (int j = 0; j < m_enon; j++)
// //     {
// //       PetscInt n1 = m_mesh.m_connectivity(i, j);
// //       for (int k = 0; k < m_enon; k++)
// //       {
// //         if (j != k)
// //         {
// //           PetscInt n2 = m_mesh.m_connectivity(i, k);
// //           bool isDuplicate = false;
// //           for (int m = 0; m < connections[n1].size(); m++)
// //           {
// //             if (n2 == connections[n1][m])
// //             {
// //               isDuplicate = true;
// //               break;
// //             }
// //           }
// //           if (isDuplicate == false)
// //           {
// //             connections[n1].push_back(n2);
// //           }
// //         }
// //       }
// //     }
// //   }
// //
// //   for (int i = 0; i < m_localDOF; i++)
// //   {
// //     // +1 to include self
// //     nConnectedNodes(i) = connections[i].size() + 1;
// //   }
//
// }
