#include "reactive_bc.hpp"

ReactiveBC::ReactiveBC()
{
}


ReactiveBC::ReactiveBC(std::vector<Vec> *solution,
                       NeumannBC *bc,
                       std::vector<ReactionEquation> *surfaceReactions)
     : m_solution(solution), m_bc(bc), m_surfaceReactions(surfaceReactions)
{
  initialize(solution, bc, surfaceReactions);
}

void ReactiveBC::initialize(std::vector<Vec> *solution,
                            NeumannBC *bc,
                            std::vector<ReactionEquation> *surfaceReactions)
{
  m_solution = solution;
  m_bc = bc;
  m_surfaceReactions = surfaceReactions;

  PetscMPIInt size;
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

  // Allocate m_rate vector
  PetscInt nOwnedNodes = m_bc->m_mesh->m_nOwnedNodes;
  PetscInt nGhostNodes = m_bc->m_mesh->m_nTotalNodes - nOwnedNodes;
  PetscInt nGlobalNodes = m_bc->m_mesh->m_nTotalNodesGlobal;

  if (size == 1)
  {
    VecCreate(PETSC_COMM_WORLD, &m_rate);
    VecSetSizes(m_rate, nOwnedNodes, nOwnedNodes);
    VecSetType(m_rate, VECSEQ);
    VecSetFromOptions(m_rate);
  }
  else
  {
    std::vector<PetscInt> ghosts(nGhostNodes);
    for (PetscInt i = 0; i < nGhostNodes; i++)
    {
      ghosts[i] = m_bc->m_mesh->m_localToGlobalMap(nOwnedNodes + i);
    }

    VecCreateGhost(PETSC_COMM_WORLD,
                   nOwnedNodes,
                   nGlobalNodes,
                   nGhostNodes,
                   &ghosts[0],
                   &m_rate);
  }
  VecAssemblyBegin(m_rate);
  VecAssemblyEnd(m_rate);
  m_initialized = true;
}


ReactiveBC::~ReactiveBC()
{
  VecDestroy(&m_rate);
}


void ReactiveBC::updateValues()
{
  PetscMPIInt size;
  MPI_Comm_size(PETSC_COMM_WORLD,&size);

  Vec localRate;
  PetscScalar *data;

  for (int i = 0; i < m_surfaceReactions->size(); i++)
  {
    if ((*m_surfaceReactions)[i].m_reactions.empty())
    {
      // TODO: This can be optimized...only needs to be done once
      m_bc->m_nodeValues[i] = 0.;
    }
    else
    {

      (*m_surfaceReactions)[i].computeRate(*m_solution, m_rate);

      if (size == 1)
      {
        PetscScalar *data;
        VecGetArray(m_rate, &data);
        for (PetscInt j = 0; j < m_bc->m_nodeValues[i].m_size; j++)
        {
          m_bc->m_nodeValues[i](j) = data[m_bc->m_nodeIds(j)];
        }
      }
      else
      {

        VecGhostUpdateBegin(m_rate, INSERT_VALUES, SCATTER_FORWARD);
        VecGhostUpdateEnd(m_rate, INSERT_VALUES, SCATTER_FORWARD);
        VecGhostGetLocalForm(m_rate, &localRate);

        VecGetArray(localRate, &data);
        for (PetscInt j = 0; j < m_bc->m_nodeValues[i].m_size; j++)
        {
          m_bc->m_nodeValues[i](j) = data[m_bc->m_nodeIds(j)];
        }

        VecRestoreArray(localRate,&data);
        VecGhostRestoreLocalForm(m_rate,&localRate);
      }
    }
  }
}
