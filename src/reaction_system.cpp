#include <iostream>
#include <fstream>
#include <cstdlib>
#include "petscksp.h"
#include "reaction_system.hpp"


void ReactionEquation::computeRate(const std::vector<Vec> &c, Vec &rate)
{
  VecSet(rate, 0.);
  if (m_reactions.empty()) // Do nothing if empty
  {
  }
  else
  {
    for (int i = 0; i < m_reactions.size(); i++)
    {
      if (m_reactions[i].m_reactionType == 0) // 0th order
      {
        VecSet(rate, m_reactions[i].m_rateConstant[0]);
      }
      else if (m_reactions[i].m_reactionType == 1) // 1st order
      {
        VecAXPY(rate,
                m_reactions[i].m_rateConstant[0],
                c[m_reactions[i].m_speciesIdx[0]]);
      }
      else if (m_reactions[i].m_reactionType == 2) // 2nd order
      {
        VecAXPBYPCZ(rate,
                    1,
                    m_reactions[i].m_rateConstant[0],
                    0.0,
                    c[m_reactions[i].m_speciesIdx[0]],
                    c[m_reactions[i].m_speciesIdx[1]]);
      }
      else if (m_reactions[i].m_reactionType == 3) // Michaelis-Menten
      {
        // NOTE: This could be made faster by preallocating a tmp vector
        // somewhere if it is a problem.
        Vec tmp;
        VecDuplicate(c[m_reactions[i].m_speciesIdx[0]], &tmp);
        VecSet(tmp, m_reactions[i].m_rateConstant[1]);
        VecAXPY(tmp, 1, c[m_reactions[i].m_speciesIdx[1]]);
        VecReciprocal(tmp);
        VecScale(tmp, m_reactions[i].m_rateConstant[0]);
        VecPointwiseMult(tmp, tmp, c[m_reactions[i].m_speciesIdx[0]]);
        VecPointwiseMult(tmp, tmp, c[m_reactions[i].m_speciesIdx[1]]);
        VecAXPY(rate, 1, tmp);
        VecDestroy(&tmp);
      }
      else if (m_reactions[i].m_reactionType == 4) // 3 spec Michaelis-Menten
      {
        // NOTE: This could be made faster by preallocating a tmp vector
        // somewhere if it is a problem.
        Vec tmp;
        VecDuplicate(c[m_reactions[i].m_speciesIdx[0]], &tmp);
        VecSet(tmp, m_reactions[i].m_rateConstant[1]);
        VecAXPY(tmp, 1, c[m_reactions[i].m_speciesIdx[2]]);
        VecReciprocal(tmp);
        VecScale(tmp, m_reactions[i].m_rateConstant[0]);
        VecPointwiseMult(tmp, tmp, c[m_reactions[i].m_speciesIdx[0]]);
        VecPointwiseMult(tmp, tmp, c[m_reactions[i].m_speciesIdx[1]]);
        VecPointwiseMult(tmp, tmp, c[m_reactions[i].m_speciesIdx[2]]);
        VecAXPY(rate, 1, tmp);
        VecDestroy(&tmp);
      }
      else // Unknown
      {
        std::cout << "Invalid reaction type: "
                  << m_reactions[i].m_reactionType
                  << std::endl;
        MPI_Abort(PETSC_COMM_WORLD, 1);
      }
    }
  }
}
