import numpy as np

def unit_line_mesh(n_elements, output_filename_root):
    coordinates = np.linspace(0, 1, n_elements + 1)
    connectivity = np.zeros(2 * n_elements, dtype = np.int32)
    for i in xrange(n_elements):
        connectivity[2 * i] = i
        connectivity[2 * i + 1] = i + 1

    fout = open(output_filename_root + '_coordinates.bin', 'wb')
    ((n_elements + 1) * np.ones(1, dtype=np.int32)).tofile(fout)
    coordinates.tofile(fout)
    fout.close()

    fout = open(output_filename_root + '_connectivity.bin', 'wb')
    (n_elements * np.ones(1, dtype=np.int32)).tofile(fout)
    connectivity.tofile(fout)
    fout.close()


def unit_square_mesh(n_x, n_y, output_filename_root):
    x = np.linspace(0, 1, n_x + 1)
    y = np.linspace(0, 1, n_y + 1)
    coordinates = np.zeros(((n_x + 1) * (n_y + 1), 2))
    idx = 0
    for j in xrange(n_y + 1):
        for i in xrange(n_x + 1):
            coordinates[idx, 0] = x[i]
            coordinates[idx, 1] = y[j]
            idx += 1
    connectivity = np.zeros((n_x * n_y * 2, 3), dtype=np.int32)
    for i in xrange(n_y):
        for j in xrange(n_x):
            connectivity[2 * j + 2 * i * n_x, :] = \
                                                [j + i * (n_x + 1), \
                                                 j + i * (n_x + 1) + 1, \
                                                 j + (i + 1) * (n_x + 1) + 1]
            connectivity[2 * j + 2 * i * n_x + 1, :] = \
                                                [j + i * (n_x + 1), \
                                                 j + (i + 1) * (n_x + 1) + 1, \
                                                 j + (i + 1) * (n_x + 1)]

    fout = open(output_filename_root + '_coordinates.bin', 'wb')
    ((n_x + 1) * (n_y + 1) * np.ones(1, dtype=np.int32)).tofile(fout)
    coordinates.tofile(fout)
    fout.close()

    fout = open(output_filename_root + '_connectivity.bin', 'wb')
    (n_x * n_y * 2 * np.ones(1, dtype=np.int32)).tofile(fout)
    connectivity.tofile(fout)
    fout.close()




if __name__ == "__main__":
#     unit_line_mesh(8,
#                    "/Users/kirkhansen/Desktop/TestHansport/line_n8")
    unit_square_mesh(8, 8,
                     "/Users/kirkhansen/Desktop/TestHansport/square_n8")
