#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include "mesh.hpp"
#include "petscksp.h"
#include "FEMSystem.hpp"
#include "basis.hpp"
#include "femadvdiffsolver.hpp"
#include "reaction_solver.hpp"
#include "chemical_system.hpp"

int testPoisson1d();
int testPoisson2d();
int testPoisson3d();
int testPartition();
int testAdvDiff();
int testAdvDiffUnsteady();
int testAdvDiffGenAlpha();
int testIterateDiffusion();
int testLoadIC();
int testReactionFile();

template <typename T>
std::string toString(T rhs)
{
  std::stringstream ss;
  ss << rhs;
  return ss.str();
}

int main(int argc, char **argv)
{
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, (char*)0, (char*)0);
  CHKERRQ(ierr);

  PetscMPIInt m_MPIsize;
  PetscMPIInt m_MPIrank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&m_MPIrank);
  CHKERRQ(ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&m_MPIsize);
  CHKERRQ(ierr);

  // testPartition();
  // testAdvDiffGenAlpha();
  // testAdvDiff();
  // testLoadIC();
  // testIterateDiffusion();
  testReactionFile();

  PetscFinalize();
  return 0;
}


int testReactionFile()
{
  std::string filename = "/Users/kirkhansen/Desktop/TestReactionFile/pap.inp";
  std::vector<Vec> sol(4);
  int localDOF=10;
  int globalDOF=40;
  PetscMPIInt size;
  MPI_Comm_size(PETSC_COMM_WORLD,&size);
  for (int i = 0; i < 4; i++)
  {
    VecCreate(PETSC_COMM_WORLD, &sol[i]);
    VecSetSizes(sol[i], localDOF, globalDOF);
    if (size == 1)
    {
      VecSetType(sol[i], VECSEQ);
    }
    else
    {
      VecSetType(sol[i], VECMPI);
    }
    VecSetFromOptions(sol[i]);
  }

  ChemicalSystem tmp(filename);

  tmp.outputSystem();
}


int testLoadIC()
{
  PetscErrorCode ierr;
  PetscMPIInt size;
  PetscMPIInt rank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  int order = 1;
  bool stabilized = true;

  std::string rootDir =
    "/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/";
  Mesh mesh(rootDir + "Geometries/Mesh1/cylinder_connectivity.bin",
            rootDir + "Geometries/Mesh1/cylinder_coordinates.bin",
            order);

  std::string outputRootDir = "/Users/kirkhansen/Desktop/TestICFile/";
  mesh.writeMappingToFile(outputRootDir + "mapping_o1.bin", rank);
  FemAdvDiffSolverSteady solver(mesh, order, 1, stabilized);
  solver.loadInitialConditionFromBin(outputRootDir + "ic_o2.bin");
  solver.writeSolutionToBin(outputRootDir + "sol", 0, solver.m_solution[0]);
}


int testIterateDiffusion()
{
  PetscErrorCode ierr;
  PetscMPIInt size;
  PetscMPIInt rank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  std::vector<std::string> meshes;
  meshes.push_back("Mesh1/");
  meshes.push_back("Mesh2/");
  meshes.push_back("Mesh3/");
  meshes.push_back("Mesh4/");
  meshes.push_back("Mesh5/");
  meshes.push_back("Mesh6/");
  meshes.push_back("Mesh7/");

  std::vector<std::string> dir1;
  dir1.push_back("Pe1e6/");
  dir1.push_back("Pe1e8/");
  dir1.push_back("Pe1e10/");

  std::vector<std::string> dir2;
  dir2.push_back("01_Coarse/");
  dir2.push_back("02_Medium/");
  dir2.push_back("03_Fine/");

  double diffusivity[3] = {1e-6, 1e-8, 1e-10};//{1e-4, 1e-6, 1e-8};

  for (int outer = 0; outer < 3; outer++)
  {
    if (rank == 0)
    {
      std::cout << "Diffusivity: " << diffusivity[outer] << std::endl;
    }
    std::string outerDir = dir1[outer];

    for (int inner = 0; inner < 3; inner++)
    {
      std::string innerDir = dir2[inner];
      std::string meshDir = meshes[2 * outer + inner];
      for (int order = 1; order < 3; order++)
      {
        for (int isStab = 1; isStab < 2; isStab++)
        {
          bool stabilized = (isStab != 0);
          std::string rootDir = "/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/Geometries/" + meshDir;
          std::string outputRootDir = "/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/Results/Steady/" + outerDir + innerDir;
          std::string output_filename = "order" + toString(order)
            + (stabilized ? "_stab.bin" : "_nostab.bin");
          if (rank == 0)
          {
            std::cout << "Loading mesh from " << rootDir << std::endl;
          }
          Mesh mesh(rootDir + "cylinder_connectivity.bin",
                    rootDir + "cylinder_coordinates.bin",
                    order);
          mesh.writeMappingToFile(outputRootDir + "mapping_order" + toString(order)  + (stabilized ? "_stab.bin" : "_nostab.bin"), rank);

          FemAdvDiffSolverSteady solver(mesh, order, 1, stabilized);


          solver.m_dirichletBCs.push_back(DirichletBC(mesh));
          solver.m_dirichletBCs[0].loadNodes(rootDir + "inlet.bin");
          solver.m_dirichletBCs[0].setValues(0.);
          // solver.m_dirichletBCs.push_back(DirichletBC(mesh));
          //  solver.m_dirichletBCs[1].loadNodes(rootDir + "reactive.bin");
          //  solver.m_dirichletBCs[1].setValues(2.);

          if (rank == 0)
          {
            std::cout << "Loading velocity..." << std::endl;
          }
          solver.loadVelocityFromBin(solver.m_currentVelocity, rootDir + "velocity.1.bin");

          if (rank == 0)
          {
            std::cout << "Loading Neumann bcs..." << std::endl;
          }
          solver.m_neumannBCs.push_back(NeumannBC(mesh, order, 2 * order));
          solver.m_neumannBCs[0].loadNodes(rootDir + "reactive.bin");
          solver.m_neumannBCs[0].setValues(-diffusivity[outer]);

          bool converged = false;
          solver.m_diffusivity = diffusivity[outer];
          int nIters = 10;
          PetscReal multiplier = .1;
          // for (int i = -nIters * std::log10(diffusivity[outer]); i >= 0; i--)

          Vec sol_prev;
          VecCreate(PETSC_COMM_WORLD, &sol_prev);
          VecSetSizes(sol_prev, solver.m_localDOF, solver.m_globalDOF);
          if (solver.m_MPIsize == 1)
          {
            VecSetType(sol_prev, VECSEQ);
          }
          else
          {
            VecSetType(sol_prev, VECMPI);
          }
          VecSetFromOptions(sol_prev);


          while (!converged)
          {
            VecCopy(solver.m_solution[0], sol_prev);
            // solver.m_diffusivity = diffusivity[outer]
            //                        * std::pow(10., (i * 1.) / nIters);
            if (rank == 0)
            {
              std::cout << "Diffusivity: " << solver.m_diffusivity << std::endl;
            }

            if (rank == 0)
            {
              std::cout << "Building LHS..." << std::endl;
            }
            solver.buildLHS();
            if (rank == 0)
            {
              std::cout << "Building RHS..." << std::endl;
            }
            solver.buildRhs();
            if (rank == 0)
            {
              std::cout << "Applying BCs..." << std::endl;
            }
            solver.applyDirichletBCs();
            // MatView(solver.m_LHS,	PETSC_VIEWER_STDOUT_WORLD);
            // VecView(solver.m_rhs,	PETSC_VIEWER_STDOUT_WORLD);
            if (rank == 0)
            {
              std::cout << "Solving..." << std::endl;
            }

            KSPSetOperators(solver.m_ksp, solver.m_LHS, solver.m_LHS);
            KSPSolve(solver.m_ksp, solver.m_rhs, solver.m_solution[0]);

            // Get diagnostics
            KSPConvergedReason reason;
            PetscInt nIterations;
            KSPGetConvergedReason(solver.m_ksp, &reason);
            KSPGetIterationNumber(solver.m_ksp, &nIterations);

            if (reason >= 0) // converged
            {
              if (solver.m_diffusivity == diffusivity[outer])
              {
                converged = true;
              }
              else
              {
                solver.m_diffusivity *= multiplier;
                if (solver.m_diffusivity <= diffusivity[outer])
                {
                  solver.m_diffusivity = diffusivity[outer];
                }
              }
            }
            else
            {
              solver.m_diffusivity /= multiplier;
              multiplier = std::pow(multiplier, .5);
              VecCopy(sol_prev, solver.m_solution[0]);
            }
            if (rank == 0)
            {
              std::cout << "Convergence: " << reason << std::endl;
              std::cout << "Number of iterations: " << nIterations << std::endl;
            }
            if (rank == 0)
            {
              std::cout << "New multiplier: " << multiplier << std::endl;
            }
            PetscInt p;
            PetscReal maxval;
            PetscReal minval;
            VecMax(solver.m_solution[0], &p, &maxval);
            VecMin(solver.m_solution[0], &p, &minval);
            if (rank == 0)
            {
              std::cout << "Max: " << maxval << std::endl;
              std::cout << "Min: " << minval << std::endl << std::endl;
            }
          }


          if (rank == 0)
          {
            std::cout << "Writing solution to "
                      << outputRootDir + output_filename
                      << std::endl << std::endl << std::endl << std::endl;
          }
          solver.writeSolutionToBin(outputRootDir + output_filename, 0, solver.m_solution[0]);

          VecDestroy(&sol_prev);
        }
      }
    }
  }
}



int testAdvDiffGenAlpha()
{
  PetscErrorCode ierr;
  PetscMPIInt size;
  PetscMPIInt rank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  PetscReal rho_infinity = 1.;
  PetscReal convergence_epsilon = 1. + 1e-6;

  std::vector<std::string> meshes;
  meshes.push_back("Mesh1/");
  meshes.push_back("Mesh2/");
  meshes.push_back("Mesh3/");
  meshes.push_back("Mesh4/");
  meshes.push_back("Mesh5/");
  meshes.push_back("Mesh6/");
  meshes.push_back("Mesh7/");
  meshes.push_back("Mesh8/");
  meshes.push_back("Mesh9/");

  std::vector<std::string> dir1;
  dir1.push_back("Pe1e6/");
  dir1.push_back("Pe1e8/");
  dir1.push_back("Pe1e10/");

  std::vector<std::string> dir2;
  dir2.push_back("01_Coarse/");
  dir2.push_back("02_Medium/");
  dir2.push_back("03_Fine/");
  dir2.push_back("04_VeryFine/");
  dir2.push_back("05_UltraFine/");

  PetscReal diffusivity[3] = {1e-6, 1e-8, 1e-10};//{1e-4, 1e-6, 1e-8};

  for (int outer = 0; outer < 2; outer++)
  {
    if (rank == 0)
    {
      std::cout << "Diffusivity: " << diffusivity[outer] << std::endl;
    }
    std::string outerDir = dir1[outer];

    for (int inner = 0; inner < 5; inner++)
    {
      std::string innerDir = dir2[inner];
      std::string meshDir = meshes[2 * outer + inner];
      for (int order = 1; order < 3; order++)
      {
        for (int isStab = 0; isStab < 2; isStab++)
        {
          bool stabilized = (isStab != 0);
          std::string rootDir = "/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/2D/BLMesh/Geometries/" + meshDir;
          std::string outputRootDir = "/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/2D/BLMesh/Results/" + outerDir + innerDir;
          // std::string outputRootDir = "/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/2D/Results/TestVsFenics/";
          std::string output_filename = "order" + toString(order)
            + "_new_tau" + (stabilized ? "_stab.bin" : "_nostab.bin");
          if (rank == 0)
          {
            std::cout << "Loading mesh from " << rootDir << std::endl;
          }
          Mesh mesh(rootDir + "cylinder_connectivity.bin",
                    rootDir + "cylinder_coordinates.bin",
                    order);
          mesh.writeMappingToFile(outputRootDir + "mapping_order" + toString(order)  + (stabilized ? "_stab.bin" : "_nostab.bin"), rank);

          // FemAdvDiffSolverSteady solver(mesh, order, 1, stabilized);

          FemAdvDiffSolverGenAlpha solver(mesh,
                                          order,
                                          1,
                                          rho_infinity,
                                          diffusivity[outer],
                                          stabilized);

          if (rank == 0)
          {
            std::cout << "Basis function order: " << solver.m_basis.m_order
                      << std::endl
                      << "Stabilization is "
                      << (solver.m_stabilized ? "on." : "off.")
                      << std::endl;
          }
          // solver.m_diffusivity = diffusivity[outer];

          solver.m_dirichletBCs.push_back(DirichletBC(mesh));
          solver.m_dirichletBCs[0].loadNodes(rootDir + "inlet.bin");
          solver.m_dirichletBCs[0].setValues(0.);
          // solver.m_dirichletBCs.push_back(DirichletBC(mesh));
          // solver.m_dirichletBCs[1].loadNodes(rootDir + "reactive.bin");
          // solver.m_dirichletBCs[1].setValues(1.);

          if (rank == 0)
          {
            std::cout << "Loading Neumann bcs..." << std::endl;
          }
          solver.m_neumannBCs.push_back(NeumannBC(mesh, order, 2*order));
          solver.m_neumannBCs[0].loadNodes(rootDir + "reactive.bin");
          solver.m_neumannBCs[0].setValues(-solver.m_diffusivity);
          // if (rank == 0)
          // {
          //   std::cout << "Loading velocity..." << std::endl;
          // }
          // solver.loadVelocityFromBin(solver.m_currentVelocity, rootDir + "velocity.bin");

          solver.m_t = 0.;
          solver.m_currentTstep = 0;
          solver.m_dt = .025;
          int nsteps = 8000000;
          solver.m_velFilenameRoot = rootDir + "velocity";
          solver.m_velFileStart = 1;
          solver.m_nVelFiles = 2;
          solver.m_velFileInterval = 1;
          solver.m_velFileDt = 1000.;
          solver.m_saveInterval = 800000000;
          solver.m_outputFilenameRoot = outputRootDir
                                        + "advdiff_test_vel_order2_nodiffstab";

          //solver.calculateConstantMatrices();

          std::time_t start = std::time(NULL);
          // PetscLogDouble mallocSpace, memory;

          PetscReal oldMax = 0.;
          PetscReal newMax = 0.;
          for (int i = 0; i < nsteps; i++)
          {
            // PetscMallocGetCurrentUsage(&mallocSpace);
            // PetscMemoryGetCurrentUsage(&memory);
            if (rank == 0)
            {
            //             << "PetscMallocGetCurrentUsage(): "
            //             << mallocSpace << std::endl
            //             << "PetscMemoryGetCurrentUsage(): "
            //             << memory << std::endl;
            }
            // solver.setUpSystem();
            // solver.solve();
            PetscInt nIterations = solver.takeTimeStep();

            // VecMax(solver.m_solution, NULL, &newMax);
            VecNorm(solver.m_solution[0], NORM_2, &newMax);
            if (rank == 0)
            {
              std::cout << "Case: " << outer << inner << order << isStab
                        << " Tstep: " << i + 1 << " Norm: " << newMax
                        << " Iterations: " << nIterations
                        << std::endl;
            }
            if (newMax > convergence_epsilon * oldMax)
            {
              oldMax = newMax;
            }
            else if (i > 100)
            {
              break;
            }
          }
          solver.writeSolutionToBin(outputRootDir + output_filename,
                                    0,
                                    solver.m_solution[0]);
        }
      }
    }
  }



//   std::string rootDir = "/Users/kirkhansen/Data/Hansport_Validation/01_Geometries/03_Cylinder/02_Medium/";
//   std::string outputRootDir = "/Users/kirkhansen/Data/Hansport_Validation/02_Results/03_Cylinder/02_Medium/";
//   int order = 2;
//   if (rank == 0)
//   {
//     std::cout << "Loading mesh..." << std::endl;
//   }
//   Mesh mesh(rootDir + "cylinder_connectivity.bin",
//             rootDir + "cylinder_coordinates.bin",
//             order);
//   mesh.writeMappingToFile(outputRootDir + "mapping_parabola.bin", rank);
//
//   FemAdvDiffSolverGenAlpha solver(mesh, order, 1, .2, 1e-5, true);
//
//   // solver.m_dirichletBCs.push_back(DirichletBC(mesh));
//   // solver.m_dirichletBCs[0].loadNodes(rootDir + "outlet.bin");
//   // solver.m_dirichletBCs[0].setValues(0.);
//   // solver.m_dirichletBCs.push_back(DirichletBC(mesh));
//   // solver.m_dirichletBCs[1].loadNodes(rootDir + "inlet.bin");
//   // solver.m_dirichletBCs[1].setValues(1.);
//   solver.m_outletBCs.push_back(OutletBC(mesh));
//   solver.m_outletBCs[0].loadNodes(rootDir + "outlet.bin");
//   solver.m_outletBCs[0].setValues(0.);
//   PetscReal norm[DIM] = {0., 1., 0.};
//   solver.m_outletBCs[0].setNormals(norm);
//   solver.m_outletBCs.push_back(OutletBC(mesh));
//   solver.m_outletBCs[1].loadNodes(rootDir + "inlet.bin");
//   solver.m_outletBCs[1].setValues(1.);
//   PetscReal norm2[DIM] = {0., -1., 0.};
//   solver.m_outletBCs[1].setNormals(norm2);
//
//
//   // solver.m_neumannBCs.push_back(NeumannBC(mesh, 2, 4));
//   // solver.m_neumannBCs[0].loadNodes(rootDir + "outlet.bin");
//   // solver.m_neumannBCs[0].setValues(2.);
//
//
//   // PetscReal vel[3] = {0., 1., 0.};
//
//   // solver.setUniformVelocity(solver.m_currentVelocity, vel);
//
//   solver.m_t = 0.;
//   solver.m_currentTstep = 0;
//   solver.m_dt = .01;
//   int nsteps = 400;
//   solver.m_velFilenameRoot = rootDir + "velocity";
//   solver.m_velFileStart = 1;
//   solver.m_nVelFiles = 2;
//   solver.m_velFileInterval = 1;
//   solver.m_velFileDt = .01;
//   solver.m_saveInterval = 1;
//   solver.m_outputFilenameRoot = outputRootDir + "advdiff_parabola";
//
//   //solver.calculateConstantMatrices();
//
//   std::time_t start = std::time(NULL);
//   // PetscLogDouble mallocSpace, memory;
//
//   PetscReal oldMax = 0.;
//   PetscReal newMax = 0.;
//   for (int i = 0; i < nsteps; i++)
//   {
//     // PetscMallocGetCurrentUsage(&mallocSpace);
//     // PetscMemoryGetCurrentUsage(&memory);
//     //             << "PetscMallocGetCurrentUsage(): "
//     //             << mallocSpace << std::endl
//     //             << "PetscMemoryGetCurrentUsage(): "
//     //             << memory << std::endl;
//     // solver.setUpSystem();
//     // solver.solve();
//     solver.takeTimeStep();
//
//     VecMax(solver.m_solution, NULL, &newMax);
//     if (rank == 0)
//     {
//       std::cout << "Tstep: " << i + 1 << ", Max: " << newMax << std::endl;
//     }
//     // if (newMax > oldMax)
//     // {
//     //   oldMax = newMax;
//     // }
//     // else
//     // {
//     //   break;
//     // }
//   }
//   std::cout << "Time in time step loop: "
//             << std::difftime(std::time(NULL), start) << std::endl;


}


// int testAdvDiffUnsteady()
// {
//   PetscErrorCode ierr;
//   PetscMPIInt size;
//   PetscMPIInt rank;
//   ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
//   CHKERRABORT(PETSC_COMM_WORLD, ierr);
//   ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
//   CHKERRABORT(PETSC_COMM_WORLD, ierr);
//
//   std::string rootDir = "/Users/kirkhansen/Data/Hansport_Validation/01_Geometries/03_Cylinder/04_VeryFine/";
//   std::string outputRootDir = "/Users/kirkhansen/Data/Hansport_Validation/02_Results/03_Cylinder/04_VeryFine/";
//   int order = 2;
//   if (rank == 0)
//   {
//     std::cout << "Loading mesh..." << std::endl;
//   }
//   Mesh mesh(rootDir + "cylinder_connectivity.bin",
//             rootDir + "cylinder_coordinates.bin",
//             order);
//   mesh.writeMappingToFile(outputRootDir + "mapping.bin", rank);
//
//   FemAdvDiffSolverCN solver(mesh, order, 1, false);
//
//   solver.m_diffusivity = 1e-5;
//
//   DirichletBC bc(mesh);
//   solver.m_dirichletBCs.push_back(bc);
//   solver.m_dirichletBCs.push_back(bc);
//   solver.m_dirichletBCs[0].loadNodes(rootDir + "inlet.bin");
//   solver.m_dirichletBCs[0].setValues(1.);
//   solver.m_dirichletBCs[1].loadNodes(rootDir + "outlet.bin");
//   solver.m_dirichletBCs[1].setValues(0.);
//
//   // solver.m_neumannBCs.push_back(NeumannBC(mesh, 2, 4));
//   // solver.m_neumannBCs[0].loadNodes(rootDir + "outlet.bin");
//   // solver.m_neumannBCs[0].setValues(2.);
//
//
//   PetscReal vel[3] = {0., 1., 0.};
//
//   solver.setUniformVelocity(solver.m_currentVelocity, vel);
//
//   std::cout << "Velocity: " << solver.m_currentVelocity(10, 1) << std::endl;
//
//   solver.m_t = 0.;
//   solver.m_dt = .05;
//   solver.calculateConstantMatrices();
//   for (int i = 0; i < 20; i++)
//   {
//     solver.setUpSystem();
//     solver.solve();
//     solver.writeSolutionToBin(outputRootDir + "advdiff_unsteady." + toString(i) + ".bin", solver.m_solution);
//   }
//
//
// }


int testAdvDiff()
{
  PetscErrorCode ierr;
  PetscMPIInt size;
  PetscMPIInt rank;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  std::vector<std::string> meshes;
  meshes.push_back("Mesh1/");
  meshes.push_back("Mesh2/");
  meshes.push_back("Mesh3/");
  meshes.push_back("Mesh4/");
  meshes.push_back("Mesh5/");
  meshes.push_back("Mesh6/");
  meshes.push_back("Mesh7/");

  std::vector<std::string> dir1;
  dir1.push_back("Pe1e6/");
  dir1.push_back("Pe1e8/");
  dir1.push_back("Pe1e10/");

  std::vector<std::string> dir2;
  dir2.push_back("01_Coarse/");
  dir2.push_back("02_Medium/");
  dir2.push_back("03_Fine/");

  double diffusivity[3] = {1e-6, 1e-8, 1e-10};//{1e-4, 1e-6, 1e-8};

  for (int outer = 0; outer < 3; outer++)
  {
    if (rank == 0)
    {
      std::cout << "Diffusivity: " << diffusivity[outer] << std::endl;
    }
    std::string outerDir = dir1[outer];

    for (int inner = 0; inner < 3; inner++)
    {
      std::string innerDir = dir2[inner];
      std::string meshDir = meshes[2 * outer + inner];
      for (int order = 1; order < 3; order++)
      {
        for (int isStab = 1; isStab < 2; isStab++)
        {
          bool stabilized = (isStab != 0);
          std::string rootDir = "/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/Geometries/" + meshDir;
          std::string outputRootDir = "/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/Results/" + outerDir + innerDir;
          std::string output_filename = "order" + toString(order)
            + (stabilized ? "_stab.bin" : "_nostab.bin");
          if (rank == 0)
          {
            std::cout << "Loading mesh from " << rootDir << std::endl;
          }
          Mesh mesh(rootDir + "cylinder_connectivity.bin",
                    rootDir + "cylinder_coordinates.bin",
                    order);
          mesh.writeMappingToFile(outputRootDir + "mapping_order" + toString(order)  + (stabilized ? "_stab.bin" : "_nostab.bin"), rank);

          FemAdvDiffSolverSteady solver(mesh, order, 1, stabilized);

          solver.m_diffusivity = diffusivity[outer];

          solver.m_dirichletBCs.push_back(DirichletBC(mesh));
          solver.m_dirichletBCs[0].loadNodes(rootDir + "inlet.bin");
          solver.m_dirichletBCs[0].setValues(0.);
          // solver.m_dirichletBCs.push_back(DirichletBC(mesh));
          //  solver.m_dirichletBCs[1].loadNodes(rootDir + "reactive.bin");
          //  solver.m_dirichletBCs[1].setValues(2.);

          if (rank == 0)
          {
            std::cout << "Loading Neumann bcs..." << std::endl;
          }
          solver.m_neumannBCs.push_back(NeumannBC(mesh, order, order));
          solver.m_neumannBCs[0].loadNodes(rootDir + "reactive.bin");
          solver.m_neumannBCs[0].setValues(-solver.m_diffusivity);

          if (rank == 0)
          {
            std::cout << "Loading velocity..." << std::endl;
          }
          solver.loadVelocityFromBin(solver.m_currentVelocity, rootDir + "velocity.1.bin");

          if (rank == 0)
          {
            std::cout << "Building LHS..." << std::endl;
          }
          solver.buildLHS();
          if (rank == 0)
          {
            std::cout << "Building RHS..." << std::endl;
          }
          solver.buildRhs();
          if (rank == 0)
          {
            std::cout << "Applying BCs..." << std::endl;
          }
          solver.applyDirichletBCs();
          // MatView(solver.m_LHS,	PETSC_VIEWER_STDOUT_WORLD);
          // VecView(solver.m_rhs,	PETSC_VIEWER_STDOUT_WORLD);
          if (rank == 0)
          {
            std::cout << "Solving..." << std::endl;
          }
          solver.solve();

          // Get diagnostics
          KSPConvergedReason reason;
          PetscInt nIterations;
          KSPGetConvergedReason(solver.m_ksp, &reason);
          KSPGetIterationNumber(solver.m_ksp, &nIterations);

          if (reason >= 0) // converged
          {
            if (rank == 0)
            {
              std::cout << "Converged." << std::endl;
            }
            if (rank == 0)
            {
              std::cout << "Writing solution to "
                        << outputRootDir + output_filename
                        << std::endl << std::endl << std::endl << std::endl;
            }
            solver.writeSolutionToBin(outputRootDir + output_filename,
                                      0,
                                      solver.m_solution[0]);
          }
          else
          {
            if (rank == 0)
            {
              std::cout << "Not converged." << std::endl;
            }
          }
          // PetscInt p;
          // PetscReal val;
          // VecMax(solver.m_solution, &p, &val);
          // std::cout << "Max: " << val << std::endl;
          // VecMin(solver.m_solution, &p, &val);
          // std::cout << "Min: " << val << std::endl;

        }
      }
    }
  }
}


int testPartition()
{
  std::string rootDir = "/Users/kirkhansen/Data/Hansport_Validation/01_Geometries/03_Cylinder/01_Coarse/";
  std::string outputRootDir = "/Users/kirkhansen/Data/Hansport_Validation/02_Results/03_Cylinder/01_Coarse/";
  int order = 1;
  // std::cout << "Loading mesh..." << std::endl;
  Mesh mesh(rootDir + "cylinder_connectivity.bin",
            rootDir + "cylinder_coordinates.bin",
            order);
  // mesh.printConnectivity();
  // mesh.printCoordinates();
}


// int testPoisson3d()
// {
//   PetscErrorCode ierr;
//
//   PetscMPIInt size;
//   PetscMPIInt rank;
//   ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
//   CHKERRABORT(PETSC_COMM_WORLD, ierr);
//   ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
//   CHKERRABORT(PETSC_COMM_WORLD, ierr);
//
//   std::string rootDir = "/Users/kirkhansen/Data/Hansport_Validation/01_Geometries/03_Cylinder/04_VeryFine/";
//   std::string outputRootDir = "/Users/kirkhansen/Data/Hansport_Validation/02_Results/03_Cylinder/04_VeryFine/";
//   int order = 1;
//   if (rank == 0)
//   {
//     std::cout << "Loading mesh..." << std::endl;
//   }
//   Mesh mesh(rootDir + "cylinder_connectivity.bin",
//             rootDir + "cylinder_coordinates.bin",
//             order);
//   mesh.writeMappingToFile(outputRootDir + "mapping.bin", rank);
//   PoissonSolverSteady solver(mesh, order);
//
//   solver.m_integrationScheme.m_order = order;
//   solver.m_integrationScheme.evaluate();
//   if (rank == 0)
//   {
//     std::cout << "Integration points: "
//               << solver.m_integrationScheme.m_nIntegrationPoints << std::endl;
//   }
//   solver.setUpSolver();
//   if (rank == 0)
//   {
//     std::cout << "Building LHS..." << std::endl;
//   }
//   solver.buildLHS();
//
//   if (rank == 0)
//   {
//     std::cout << "Loading Dirichlet BCs..." << std::endl;
//   }
//   DirichletBC bc(mesh);
//   solver.m_dirichletBCs.push_back(bc);
//   solver.m_dirichletBCs.push_back(bc);
//   solver.m_dirichletBCs[0].loadNodes(rootDir + "inlet.bin");
//   solver.m_dirichletBCs[1].loadNodes(rootDir + "outlet.bin");
//   solver.m_dirichletBCs[0].setValues(1.);
//   solver.m_dirichletBCs[1].setValues(1.);
//   if (order == 2)
//   {
//     solver.m_dirichletBCs[0].make2ndOrder();
//     solver.m_dirichletBCs[1].make2ndOrder();
//   }
//
//   // if (rank == 0)
//   // {
//   //   std::cout << "Loading Neumann BCs..." << std::endl;
//   // }
//   // solver.m_neumannBCs.push_back(NeumannBC(mesh, 2, 4));
//   // solver.m_neumannBCs[0].loadNodes(rootDir + "outlet.bin");
//   // solver.m_neumannBCs[0].setValues(1.);
//
//   if (rank == 0)
//   {
//     std::cout << "Setting RHS..." << std::endl;
//   }
//   solver.setRHS(1.);
//   if (rank == 0)
//   {
//     std::cout << "Applying BCs..." << std::endl;
//   }
//   solver.applyDirichletBCs();
//   VecAssemblyBegin(solver.m_b);
//   VecAssemblyEnd(solver.m_b);
//   // VecView(solver.m_b,	PETSC_VIEWER_STDOUT_WORLD);
//
//
//   // MatAssemblyBegin(solver.m_A, MAT_FINAL_ASSEMBLY);
//   // MatAssemblyEnd(solver.m_A, MAT_FINAL_ASSEMBLY);
//   // MatView(solver.m_A,	PETSC_VIEWER_STDOUT_WORLD);
//
//
//   if (rank == 0)
//   {
//     std::cout << "Solving..." << std::endl;
//   }
//   solver.solve();
//   // VecView(solver.m_x, PETSC_VIEWER_STDOUT_WORLD);
//   PetscInt vecSize;
//   VecGetSize(solver.m_x, &vecSize);
//   PetscInt p;
//   PetscScalar max;
//   VecMax(solver.m_x, &p, &max);
//   if (rank == 0)
//   {
//     std::cout << "Max value: " << max << std::endl;
//   }
//   solver.writeSolutionToBin(outputRootDir + "soln_order" + toString(order) + ".bin");
//
//
// }
//
//
// int testPoisson2d()
// {
//   PetscErrorCode ierr;
//
//   PetscMPIInt size;
//   PetscMPIInt rank;
//   ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
//   CHKERRABORT(PETSC_COMM_WORLD, ierr);
//   ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
//   CHKERRABORT(PETSC_COMM_WORLD, ierr);
//
//   std::string rootDir = "/Users/kirkhansen/Data/Hansport_Validation/01_Geometries/02_Square/";
//   std::string outputRootDir = "/Users/kirkhansen/Data/Hansport_Validation/02_Results/02_Square/";
//   int order = 2;
//   if (rank == 0)
//   {
//     std::cout << "Loading mesh..." << std::endl;
//   }
//   Mesh mesh(rootDir + "square_connectivity.bin",
//             rootDir + "square_coordinates.bin",
//             order);
//   mesh.writeMappingToFile(outputRootDir + "mapping.bin", rank);
//
//
//   for (int i = 0; i < size; i++)
//   {
//     if (i == rank)
//     {
//       std::cout << "Rank: " << rank << std::endl;
//       mesh.printCoordinates();
//       mesh.printOrder2Connectivity();
//       for (int j = 0; j < mesh.m_nTotalNodes; j++)
//       {
//         std::cout << mesh.m_localToGlobalMap(j) << " "
//                   << mesh.m_parallelToSerialMap(j) << std::endl;
//       }
//       std::cout << std::endl;
//
//       for (int j = 0; j < (mesh.m_nTotalNodes-mesh.m_nCornerNodes); j++)
//       {
//         for (int k = 0; k < 3; k++)
//         {
//           std::cout << mesh.m_order2nodeNeighbors(j, k) << " ";
//         }
//         std::cout << std::endl;
//       }
//     }
//   }
//
//
//   PoissonSolverSteady solver(mesh, order);
//
//   solver.m_integrationScheme.m_order = order;
//   solver.m_integrationScheme.evaluate();
//   solver.setUpSolver();
//   std::cout << "Building LHS..." << std::endl;
//   solver.buildLHS();
//
//   std::cout << "Loading BCs..." << std::endl;
//   DirichletBC bc(mesh);
//   solver.m_dirichletBCs.push_back(bc);
//   solver.m_dirichletBCs.push_back(bc);
//   solver.m_dirichletBCs[0].loadNodes(rootDir + "inlet.bin");
//   solver.m_dirichletBCs[1].loadNodes(rootDir + "outlet.bin");
//   // for (int i = 0; i < size; i++)
//   // {
//   //   MPI_Barrier(PETSC_COMM_WORLD);
//   //   if (i == rank)
//   //   {
//   //     std::cout << "Rank: " << rank << std::endl;
//   //     for (int j = 0; j < 2; j++)
//   //     {
//   //       std::cout << "BC: " << j << std::endl;
//   //       for (int k = 0; k < solver.m_dirichletBCs[j].m_nodeIds.m_size; k++)
//   //       {
//   //         std::cout << solver.m_dirichletBCs[j].m_nodeIds(k) << " ";
//   //       }
//   //       std::cout << std::endl;
//   //     }
//   //   }
//   //   MPI_Barrier(PETSC_COMM_WORLD);
//   // }
//   solver.m_dirichletBCs[0].setValues(1.);
//   solver.m_dirichletBCs[1].setValues(1.);
//   if (order == 2)
//   {
//     solver.m_dirichletBCs[0].make2ndOrder();
//     solver.m_dirichletBCs[1].make2ndOrder();
//   }
//   for (int i = 0; i < size; i++)
//   {
//     if (i == rank)
//     {
//       std::cout << "Rank: " << rank;
//       for (int j = 0; j < 2; j++)
//       {
//         std::cout << " Bc: " << j
//                   << " Nodes: " << solver.m_dirichletBCs[j].m_nodeIds.m_size;
//       }
//       std::cout << std::endl;
//
//     }
//   }
//
//   std::cout << "Setting RHS..." << std::endl;
//   solver.setRHS(1.);
//   VecAssemblyBegin(solver.m_b);
//   VecAssemblyEnd(solver.m_b);
//   VecView(solver.m_b,	PETSC_VIEWER_STDOUT_WORLD);
//   std::cout << "Applying BCs..." << std::endl;
//   solver.applyDirichletBCs();
//
//
//   // MatAssemblyBegin(solver.m_A, MAT_FINAL_ASSEMBLY);
//   // MatAssemblyEnd(solver.m_A, MAT_FINAL_ASSEMBLY);
//   MatView(solver.m_A,	PETSC_VIEWER_STDOUT_WORLD);
//
//
//   std::cout << "Solving..." << std::endl;
//   solver.solve();
//   // VecView(solver.m_x, PETSC_VIEWER_STDOUT_WORLD);
//   PetscInt vecSize;
//   VecGetSize(solver.m_x, &vecSize);
//   std::cout << "Vector size: " << vecSize << std::endl;
//   PetscInt p;
//   PetscScalar max;
//   VecMax(solver.m_x, &p, &max);
//   std::cout << "Max: " << max << std::endl;
//   solver.writeSolutionToBin(outputRootDir + "soln_order" + toString(order) + ".bin");
//   VecView(solver.m_x,	PETSC_VIEWER_STDOUT_WORLD);
// }
//
//
// int testPoisson1d()
// {
//   PetscErrorCode ierr;
//
//   PetscMPIInt size;
//   PetscMPIInt rank;
//   ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
//   CHKERRABORT(PETSC_COMM_WORLD, ierr);
//   ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);
//   CHKERRABORT(PETSC_COMM_WORLD, ierr);
//
//   std::string rootDir = "/Users/kirkhansen/Data/Hansport_Validation/01_Geometries/01_Line/";
//   std::string outputRootDir = "/Users/kirkhansen/Data/Hansport_Validation/02_Results/01_Line/";
//   int order = 2;
//   if (rank == 0)
//   {
//     std::cout << "Loading mesh..." << std::endl;
//   }
//   Mesh mesh(rootDir + "line_connectivity.bin",
//             rootDir + "line_coordinates.bin",
//             order);
//   mesh.writeMappingToFile(outputRootDir + "mapping.bin", rank);
//
//   PoissonSolverSteady solver(mesh, order);
//
//   solver.m_integrationScheme.m_order = order;
//   solver.m_integrationScheme.evaluate();
//   solver.setUpSolver();
//   std::cout << "Building LHS..." << std::endl;
//   solver.buildLHS();
//
//   std::cout << "Loading BCs..." << std::endl;
//   DirichletBC bc(mesh);
//   solver.m_dirichletBCs.push_back(bc);
//   solver.m_dirichletBCs.push_back(bc);
//   solver.m_dirichletBCs[0].loadNodes(rootDir + "inlet.bin");
//   solver.m_dirichletBCs[1].loadNodes(rootDir + "outlet.bin");
//   solver.m_dirichletBCs[0].setValues(1.);
//   solver.m_dirichletBCs[1].setValues(1.);
//   if (order == 2)
//   {
//     solver.m_dirichletBCs[0].make2ndOrder();
//     solver.m_dirichletBCs[1].make2ndOrder();
//   }
//   for (int i = 0; i < size; i++)
//   {
//     if (i == rank)
//     {
//       std::cout << "Rank: " << rank;
//       for (int j = 0; j < 2; j++)
//       {
//         std::cout << " Bc: " << j
//                   << " Nodes: " << solver.m_dirichletBCs[j].m_nodeIds.m_size;
//       }
//       std::cout << std::endl;
//
//     }
//   }
//
//   std::cout << "Setting RHS..." << std::endl;
//   solver.setRHS(1.);
//   VecAssemblyBegin(solver.m_b);
//   VecAssemblyEnd(solver.m_b);
//   VecView(solver.m_b,	PETSC_VIEWER_STDOUT_WORLD);
//   std::cout << "Applying BCs..." << std::endl;
//   solver.applyDirichletBCs();
//
//
//   // MatAssemblyBegin(solver.m_A, MAT_FINAL_ASSEMBLY);
//   // MatAssemblyEnd(solver.m_A, MAT_FINAL_ASSEMBLY);
//   MatView(solver.m_A,	PETSC_VIEWER_STDOUT_WORLD);
//
//
//   std::cout << "Solving..." << std::endl;
//   solver.solve();
//   // VecView(solver.m_x, PETSC_VIEWER_STDOUT_WORLD);
//   PetscInt vecSize;
//   VecGetSize(solver.m_x, &vecSize);
//   std::cout << "Vector size: " << vecSize << std::endl;
//   PetscInt p;
//   PetscScalar max;
//   VecMax(solver.m_x, &p, &max);
//   std::cout << "Max: " << max << std::endl;
//   solver.writeSolutionToBin(outputRootDir + "soln_order" + toString(order) + ".bin");
//
//
// }
//
//
// // int testPoisson1d()
// // {
// //   Mesh mesh("/Users/kirkhansen/Desktop/TestHansport/line_n4_connectivity.bin",
// //             "/Users/kirkhansen/Desktop/TestHansport/line_n4_coordinates.bin",
// //             2);
// //   mesh.printConnectivity();
// //   mesh.printCoordinates();
// //   PoissonSolverSteady solver(mesh, 2);
// //   solver.m_integrationScheme.m_order = 2;
// //   solver.m_integrationScheme.evaluate();
// //   solver.setUpSolver();
// //   solver.buildLHS();
// //
// //   DirichletBC bc(mesh);
// //   bc.m_nodeIds.resize(2);
// //   bc.m_nodeIds(0) = 0;
// //   bc.m_nodeIds(1) = 4;
// //   bc.m_nodeValues.resize(2);
// //   bc.m_nodeValues(0) = 0.;
// //   bc.m_nodeValues(1) = 0.;
// //   solver.m_dirichletBCs.push_back(bc);
// //
// //   solver.setRHS(1.);
// //   solver.applyDirichletBCs();
// //   VecAssemblyBegin(solver.m_b);
// //   VecAssemblyEnd(solver.m_b);
// //   VecView(solver.m_b,	PETSC_VIEWER_STDOUT_WORLD);
// //
// //
// //   MatAssemblyBegin(solver.m_A, MAT_FINAL_ASSEMBLY);
// //   MatAssemblyEnd(solver.m_A, MAT_FINAL_ASSEMBLY);
// //   MatView(solver.m_A,	PETSC_VIEWER_STDOUT_WORLD);
// //
// //
// //   solver.solve();
// //   VecView(solver.m_x, PETSC_VIEWER_STDOUT_WORLD);
// //
// //
// // }
// //
// //
// // int testPoisson2d()
// // {
// //   Mesh mesh("/Users/kirkhansen/Desktop/TestHansport/square_n4_connectivity.bin",
// //             "/Users/kirkhansen/Desktop/TestHansport/square_n4_coordinates.bin",
// //             2);
// //   mesh.printConnectivity();
// //   mesh.printCoordinates();
// //   mesh.printOrder2Connectivity();
// //   PoissonSolverSteady solver(mesh, 2);
// //   solver.m_integrationScheme.m_order = 2;
// //   solver.m_integrationScheme.evaluate();
// //   solver.setUpSolver();
// //   solver.buildLHS();
// //
// //   DirichletBC bc(mesh);
// //   bc.m_nodeIds.resize(10);
// //   bc.m_nodeIds(0) = 0;
// //   bc.m_nodeIds(1) = 5;
// //   bc.m_nodeIds(2) = 10;
// //   bc.m_nodeIds(3) = 15;
// //   bc.m_nodeIds(4) = 20;
// //   bc.m_nodeIds(5) = 4;
// //   bc.m_nodeIds(6) = 9;
// //   bc.m_nodeIds(7) = 14;
// //   bc.m_nodeIds(8) = 19;
// //   bc.m_nodeIds(9) = 24;
// //   bc.m_nodeValues.resize(10);
// //   bc.m_nTotalNodes = 10;
// //   // bc.m_nodeIds.resize(18);
// //   // bc.m_nodeIds(0) = 0;
// //   // bc.m_nodeIds(1) = 9;
// //   // bc.m_nodeIds(2) = 18;
// //   // bc.m_nodeIds(3) = 27;
// //   // bc.m_nodeIds(4) = 36;
// //   // bc.m_nodeIds(5) = 45;
// //   // bc.m_nodeIds(6) = 54;
// //   // bc.m_nodeIds(7) = 63;
// //   // bc.m_nodeIds(8) = 72;
// //   // bc.m_nodeIds(9) = 8;
// //   // bc.m_nodeIds(10) = 17;
// //   // bc.m_nodeIds(11) = 26;
// //   // bc.m_nodeIds(12) = 35;
// //   // bc.m_nodeIds(13) = 44;
// //   // bc.m_nodeIds(14) = 53;
// //   // bc.m_nodeIds(15) = 62;
// //   // bc.m_nodeIds(16) = 71;
// //   // bc.m_nodeIds(17) = 80;
// //   // bc.m_nodeValues.resize(18);
// //   // bc.m_nNodes = 18;
// //   bc.make2ndOrder();
// //   bc.setValues(0.);
// //   solver.m_dirichletBCs.push_back(bc);
// //   solver.setRHS(1.);
// //   solver.applyDirichletBCs();
// //   VecAssemblyBegin(solver.m_b);
// //   VecAssemblyEnd(solver.m_b);
// // //   VecView(solver.m_b,	PETSC_VIEWER_STDOUT_WORLD);
// //
// //
// //   MatAssemblyBegin(solver.m_A, MAT_FINAL_ASSEMBLY);
// //   MatAssemblyEnd(solver.m_A, MAT_FINAL_ASSEMBLY);
// // //   MatView(solver.m_A,	PETSC_VIEWER_STDOUT_WORLD);
// //
// //
// //   solver.solve();
// //   // VecView(solver.m_x, PETSC_VIEWER_STDOUT_WORLD);
// //
// // }
// //
// //
// // int oldStuff()
// // {
// //   Mat A;
// //   MatCreateSeqAIJ(PETSC_COMM_WORLD, 3, 3, 0, PETSC_NULL, &A);
// // //  MatSetFromOptions(A);
// // //  MatSetOption(A, MAT_SYMMETRIC, PETSC_TRUE);
// //   MatSetUp(A);
// //   MatSetOption(A, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);
// //   MatSetValue(A, 0, 0, 1., INSERT_VALUES);
// //   MatSetValue(A, 0, 1, 2., INSERT_VALUES);
// //   MatSetValue(A, 0, 2, 3., INSERT_VALUES);
// //   MatSetValue(A, 1, 1, 4., INSERT_VALUES);
// //   MatSetValue(A, 1, 2, 5., INSERT_VALUES);
// //   MatSetValue(A, 2, 2, 6., INSERT_VALUES);
// //   MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
// //   MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
// //   MatView(A,	PETSC_VIEWER_STDOUT_WORLD);
// //
// //   Vec x, b;
// //   VecCreate(PETSC_COMM_WORLD, &b);
// //   VecSetSizes(b, PETSC_DECIDE, 3);
// //   VecSetType(b, VECSEQ);
// //   VecDuplicate(b, &x);
// //   MatGetColumnVector(A, x, 0);
// //   // VecSetValue(b, 0, 1., INSERT_VALUES);
// //   // VecSetValue(b, 1, 2., INSERT_VALUES);
// //   // VecSetValue(b, 2, 3., INSERT_VALUES);
// //
// //   // KSP ksp;
// //   // KSPCreate(PETSC_COMM_WORLD, &ksp);
// //   // KSPSetOperators(ksp, A, A);
// //   // KSPSetFromOptions(ksp);
// //   // KSPSolve(ksp, b, x);
// //   // KSPDestroy(&ksp);
// //   VecView(x,	PETSC_VIEWER_STDOUT_WORLD);
// //
// //
// //
// //
// //   Mesh mesh("/Users/kirkhansen/Desktop/TestHansport/cylinder_connectivity.bin",
// //             "/Users/kirkhansen/Desktop/TestHansport/cylinder_coordinates.bin",
// //             2);
// //   PoissonSolverSteady test(mesh, 1);
// //   mesh.printConnectivity();
// //   std::cout << std::endl << std::endl << std::endl;
// //   mesh.printOrder2Connectivity();
// //   mesh.printCoordinates();
// //   for (int i = 0; i < mesh.m_nElements; i++)
// //   {
// //     std::cout << i << ": "
// //               << getVolume(mesh.m_coordinates(mesh.m_connectivity(i,0)),
// //                            mesh.m_coordinates(mesh.m_connectivity(i,1)),
// //                            mesh.m_coordinates(mesh.m_connectivity(i,2)),
// //                            mesh.m_coordinates(mesh.m_connectivity(i,3)))
// //               << std::endl;
// //   }
// //
// //   Basis basis(mesh, 1);
// //
// //   //basis.calc_dXidX();
// //   basis.print_dXidX(1);
// //
// //   // std::cout << "Dim: " << DIM << std::endl;
// //   // Vector p1, p2, p3;
// //   // p1[0] = 0.; p1[1] = 0.;
// //   // p2[0] = 1.; p2[1] = 0.;
// //   // p3[0] = 1.; p3[1] = 1.;
// //   // std::cout << "Area: " << getArea(p1, p2, p3) << std::endl;
// //   // std::cout << p1[2] << " " << p2[2] << " " << p3[2] << std::endl;
// //
// //   PetscFinalize();
// //   return 0;
// //
// // }
// //
