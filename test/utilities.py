import vtk
import sys
import numpy as np
from vtk.util import numpy_support as VN

def unit_line_mesh(n_elements, output_filename_root):
    coordinates = np.linspace(0, 1, n_elements + 1)
    connectivity = np.zeros(2 * n_elements, dtype = np.int32)
    for i in xrange(n_elements):
        connectivity[2 * i] = i
        connectivity[2 * i + 1] = i + 1

    fout = open(output_filename_root + '_coordinates.bin', 'wb')
    ((n_elements + 1) * np.ones(1, dtype=np.int32)).tofile(fout)
    coordinates.tofile(fout)
    fout.close()

    fout = open(output_filename_root + '_connectivity.bin', 'wb')
    (n_elements * np.ones(1, dtype=np.int32)).tofile(fout)
    connectivity.tofile(fout)
    fout.close()


def unit_square_mesh(n_x, n_y, output_filename_root):
    x = np.linspace(0, 1, n_x + 1)
    y = np.linspace(0, 1, n_y + 1)
    coordinates = np.zeros(((n_x + 1) * (n_y + 1), 2))
    idx = 0
    for i in xrange(n_y + 1):
        for j in xrange(n_x + 1):
            coordinates[idx, 0] = x[j]
            coordinates[idx, 1] = y[i]
            idx += 1
    connectivity = np.zeros((n_x * n_y * 2, 3), dtype=np.int32)
    for i in xrange(n_y):
        for j in xrange(n_x):
            connectivity[2 * j + 2 * i * n_x, :] = \
                                                [j + i * (n_x + 1), \
                                                 j + i * (n_x + 1) + 1, \
                                                 j + (i + 1) * (n_x + 1)]
            connectivity[2 * j + 2 * i * n_x + 1, :] = \
                                                [j + i * (n_x + 1) + 1, \
                                                 j + i * (n_x + 1) + n_x + 2, \
                                                 j + i * (n_x + 1) + n_x + 1]
            # print connectivity[2 * j + 2 * i * n_x, :]
            # print connectivity[2 * j + 2 * i * n_x + 1, :]

    fout = open(output_filename_root + '_coordinates.bin', 'wb')
    n_nodes = (n_x + 1) * (n_y + 1)
    print 'Nodes:', n_nodes
    (n_nodes * np.ones(1, dtype=np.int32)).tofile(fout)
    coordinates.tofile(fout)
    fout.close()

    fout = open(output_filename_root + '_connectivity.bin', 'wb')
    n_elements = n_x * n_y * 2
    print 'Elements:', n_elements
    (n_elements * np.ones(1, dtype=np.int32)).tofile(fout)
    connectivity.tofile(fout)
    print connectivity
    fout.close()


def vtk_to_connectivity_and_coordinates(vtk_filename,
                                        output_root,
                                        # make_adjacency=False,
                                        offset=0,
                                        dim=3):
    # vtk_filename:   VTK file from which files will be created. Can be VTP,
    #                 VTU, or classic VTK file
    # output_root:    Filename root for output files. Underscores in filenames
    #                 are automatically added
    # make_adjacency: True to create adjacency file, False to skip
    # offset:         Offset to zero index for connectivity and adjacency
    #                 numbering, e.g. to start cell numbering at 1,
    #                 offset = 1

    extension = vtk_filename[-4:]
    if extension == '.vtp':
        reader = vtk.vtkXMLPolyDataReader()
    elif extension == '.vtu':
        reader = vtk.vtkXMLUnstructuredGridReader()
    elif extension == '.vtk':
        reader = vtk.vtkDataSetReader()
    else:
        print 'Unsupported file type:', extension
        sys.exit()
    reader.SetFileName(vtk_filename)
    reader.Update()
    data = reader.GetOutput()

    n_nodes = data.GetNumberOfPoints()
    n_elements = data.GetNumberOfCells()

    print vtk_filename, 'loaded.'
    print n_nodes, 'nodes,', n_elements, 'elements.'
    # Create coordinates file
    print 'Creating coordinates:',
    coordinates = np.zeros((n_nodes, dim))
    local_coords = np.zeros(3)
    progress = 0
    for i in xrange(n_nodes):
        if ((i * 100) / n_nodes) > progress:
            print progress,
            sys.stdout.flush()
            progress += 5
        data.GetPoint(i, local_coords)
        # Assumes 2D in x-y plane
        for j in xrange(dim):
            coordinates[i, j] = local_coords[j]
    print '100'

    fout = open(output_root + '_coordinates.bin', 'wb')
    (n_nodes * np.ones(1, dtype=np.int32)).tofile(fout)
    coordinates.tofile(fout)
    fout.close()

    # Create connectivity file
    print  'Creating connectivity:',
    connectivity = np.zeros((n_elements, dim + 1), dtype=np.int32)
    progress = 0
    for i in xrange(n_elements):
        if ((i * 100) / n_elements) > progress:
            print progress,
            sys.stdout.flush()
            progress += 5
        node_ids = vtk.vtkIdList()
        data.GetCellPoints(i, node_ids)
        for j in xrange(dim + 1):
            connectivity[i, j] = node_ids.GetId(j)

    print '100'
    fout = open(output_root + '_connectivity.bin', 'wb')
    (n_elements * np.ones(1, dtype=np.int32)).tofile(fout)
    (connectivity + offset).tofile(fout)
    fout.close()

#     if make_adjacency:
#         print 'Finding adjacency:',
#         # Create adjacency file
#         adjacency = -1 * np.ones((n_elements, 4), dtype=np.int32)
#
#         node_ids = vtk.vtkIdList()
#         node_ids.SetNumberOfIds = 4
#         face_ids = vtk.vtkIdList()
#         face_ids.SetNumberOfIds = 3
#         progress = 0
#         for i in xrange(n_elements):
#             if ((i * 100) / n_elements) > progress:
#                 print progress,
#                 sys.stdout.flush()
#                 progress += 5
#             data.GetCellPoints(i, node_ids)
#             for j in xrange(4):
#                 for k in xrange(3):
#                     face_ids.InsertId(k, node_ids.GetId((j + k + 2) % 4))
#                 shared_cells = vtk.vtkIdList()
#                 data.GetCellNeighbors(i, face_ids, shared_cells)
#                 if shared_cells.GetNumberOfIds() == 0:
#                     adjacency[i, j] = -1
#                 else:
#                     adjacency[i, j] = shared_cells.GetId(0)
#             if i < 100:
#                 print i, adjacency[i, :]
#         print '100'
#         fout = open(output_root + '_adjacency.bin', 'wb')
#         (n_elements * np.ones(1, dtype=np.int32)).tofile(fout)
#         (adjacency + offset).tofile(fout)
#         fout.close()


def vtp_to_boundary_condition_bin(vtk_filename,
                                  output_filename,
                                  fieldname='Ids',
                                  offset=0):

    reader = vtk.vtkXMLPolyDataReader()
    reader.SetFileName(vtk_filename)
    reader.Update()
    data = reader.GetOutput()

    n_nodes = data.GetNumberOfPoints()
    n_elements = data.GetNumberOfCells()

    print vtk_filename, 'loaded.'
    print n_nodes, 'nodes,', n_elements, 'elements.'


    if vtk.vtkIdTypeArray().GetDataTypeSize() == 4:
        node_ids = np.zeros(n_nodes, dtype=np.int32)
    elif vtk.vtkIdTypeArray().GetDataTypeSize() == 8:
        node_ids = np.zeros(n_nodes, dtype=np.int64)

    out_data = np.zeros(n_nodes, dtype=np.int32)

    values = data.GetPointData().GetArray(fieldname)

    for i in xrange(n_nodes):
        values.GetTuple(i, node_ids[i:i+1])
        out_data[i] = node_ids[i]

    out_data = np.sort(out_data)

    fout = open(output_filename, 'wb')
    (n_nodes * np.ones(1, dtype=np.int32)).tofile(fout)
    out_data.tofile(fout)
    fout.close()


def bin_to_vtk(mesh_vtk_filename,
               bin_filename,
               output_filename,
               field_type = 'point',
               data_type = 'f8',
               output_data_type = 'f8',
               header_data_type = 'i4',
               header_data_count = 1):
    extension = vtk_filename[-4:]
    if extension == '.vtp':
        reader = vtk.vtkXMLPolyDataReader()
    elif extension == '.vtu':
        reader = vtk.vtkXMLUnstructuredGridReader()
    elif extension == '.vtk':
        reader = vtk.vtkDataSetReader()
    else:
        print 'Unsupported file type:', extension
        sys.exit()
    reader.SetFileName(mesh_vtk_filename)
    reader.Update()
    data = reader.GetOutput()

    n_nodes = data.GetNumberOfPoints()
    n_elements = data.GetNumberOfCells()

    fin = open(bin_filename, 'rb')
    tmp = np.fromfile(fin,
                      dtype=np.dtype(header_data_type),
                      count=header_data_count)
    print tmp
    output_data = np.fromfile(fin, dtype=np.dtype(data_type))
    fin.close()
    print np.amax(output_data)
    print np.amin(output_data)
    # elimnate order 2 data
    if field_type == 'point':
        data_size = n_nodes
    elif field_type == 'cell':
        data_size = n_elements
    output_data_normal = np.zeros(data_size, dtype=np.dtype(output_data_type))
    print 'Nodes:', n_nodes
    print 'File size:', output_data.shape[0]
    for i in xrange(data_size):
        output_data_normal[i] = output_data[i]


    output_data_vtk = VN.numpy_to_vtk(output_data_normal)
    output_data_vtk.SetName('u')
    if field_type == 'point':
        data.GetPointData().AddArray(output_data_vtk)
    elif field_type == 'cell':
        data.GetCellData().AddArray(output_data_vtk)

    writer = vtk.vtkXMLUnstructuredGridWriter()
    writer.SetInput(data)
    writer.SetFileName(output_filename)
    writer.Write()



def bin_to_vtk_parallel(mesh_vtk_filename,
                        bin_filename_root,
                        mapping_filename_root,
                        number_of_processors,
                        output_filename):
    # Assumes all binary files are prefaced with a 32-bit int containing the
    # number of entries and all data is 32-bit int or double.
    extension = mesh_vtk_filename[-4:]
    if extension == '.vtp':
        reader = vtk.vtkXMLPolyDataReader()
    elif extension == '.vtu':
        reader = vtk.vtkXMLUnstructuredGridReader()
    elif extension == '.vtk':
        reader = vtk.vtkDataSetReader()
    else:
        print 'Unsupported file type:', extension
        sys.exit()
    reader.SetFileName(mesh_vtk_filename)
    reader.Update()
    data = reader.GetOutput()

    print 'Creating', output_filename
    n_nodes = data.GetNumberOfPoints()
    n_elements = data.GetNumberOfCells()

    mapping = []

    for i in xrange(number_of_processors):
        fin = open(mapping_filename_root + '.' + str(i), 'rb')
        tmp = np.fromfile(fin,
                          dtype=np.dtype('i4'),
                          count=1)
        mapping.append(np.fromfile(fin, dtype=np.dtype('i4')))
        fin.close()

    output_data = np.zeros(n_nodes, dtype=np.dtype('f8'))

    for i in xrange(number_of_processors):
        fin = open(bin_filename_root + '.' + str(i), 'rb')
        tmp = np.fromfile(fin, dtype=np.dtype('f4'), count=1)
        output_data_local = np.fromfile(fin, dtype=np.dtype('f8'))
        for j in xrange(mapping[i].shape[0]):
            if mapping[i][j] < n_nodes:
                output_data[mapping[i][j]] = output_data_local[j]
        fin.close()
    output_data_vtk = VN.numpy_to_vtk(output_data)
    output_data_vtk.SetName('c')
    data.GetPointData().AddArray(output_data_vtk)

    extension = output_filename[-4:]
    if extension == '.vtp':
        writer = vtk.vtkXMLPolyDataWriter()
    elif extension == '.vtu':
        writer = vtk.vtkXMLUnstructuredGridWriter()
    elif extension == '.vtk':
        writer = vtk.vtkDataSetWriter()
    else:
        print 'Unsupported file type:', extension
        sys.exit()
    writer.SetInputData(data)
    writer.SetFileName(output_filename)
    writer.Write()

def local_to_serial_bin_parallel(bin_filename_root,
                                 mapping_filename_root,
                                 number_of_processors,
                                 output_filename):
    mapping = []
    n_nodes = 0

    for i in xrange(number_of_processors):
        fin = open(mapping_filename_root + '.' + str(i), 'rb')
        n_local_nodes = np.fromfile(fin,
                                    dtype=np.dtype('i4'),
                                    count=1)
        n_nodes += n_local_nodes
        mapping.append(np.fromfile(fin, dtype=np.dtype('i4')))
        fin.close()

    output_data = np.zeros(n_nodes, dtype=np.dtype('f8'))

    for i in xrange(number_of_processors):
        fin = open(bin_filename_root + '.' + str(i), 'rb')
        tmp = np.fromfile(fin, dtype=np.dtype('i4'), count=1)
        output_data_local = np.fromfile(fin, dtype=np.dtype('f8'))
        for j in xrange(mapping[i].shape[0]):
            output_data[mapping[i][j]] = output_data_local[j]
        fin.close()

    fout = open(output_filename, 'wb')
    (n_nodes * np.ones(1, dtype=np.int32)).tofile(fout)
    output_data.tofile(fout)
    fout.close()



def make_test_velocity_field(vtk_filename,
                             output_root,
                             make_adjacency=False,
                             offset=0):
    # vtk_filename:   VTK file from which files will be created. Can be VTP,
    #                 VTU, or classic VTK file
    # output_root:    Filename root for output files. Underscores in filenames
    #                 are automatically added
    # make_adjacency: True to create adjacency file, False to skip
    # offset:         Offset to zero index for connectivity and adjacency
    #                 numbering, e.g. to start cell numbering at 1,
    #                 offset = 1

    extension = vtk_filename[-4:]
    if extension == '.vtp':
        reader = vtk.vtkXMLPolyDataReader()
    elif extension == '.vtu':
        reader = vtk.vtkXMLUnstructuredGridReader()
    elif extension == '.vtk':
        reader = vtk.vtkDataSetReader()
    else:
        print 'Unsupported file type:', extension
        sys.exit()
    reader.SetFileName(vtk_filename)
    reader.Update()
    data = reader.GetOutput()

    n_nodes = data.GetNumberOfPoints()

    print vtk_filename, 'loaded.'
    print n_nodes, 'nodes,'
    # Create coordinates file
    print 'Creating velocity field files:',
    velocity = np.zeros((n_nodes, 3))

    for i in xrange(n_nodes):
        velocity[i, 1] = 0.

    fout = open(output_root + '.1' + '.bin', 'wb')
    (n_nodes * np.ones(1, dtype=np.int32)).tofile(fout)
    velocity.tofile(fout)
    fout.close()

    for i in xrange(n_nodes):
        velocity[i, 1] = 1.

    fout = open(output_root + '.2' + '.bin', 'wb')
    (n_nodes * np.ones(1, dtype=np.int32)).tofile(fout)
    velocity.tofile(fout)
    fout.close()

    for i in xrange(n_nodes):
        velocity[i, 1] = 0.

    fout = open(output_root + '.3' + '.bin', 'wb')
    (n_nodes * np.ones(1, dtype=np.int32)).tofile(fout)
    velocity.tofile(fout)
    fout.close()

    for i in xrange(n_nodes):
        velocity[i, 1] = -1.

    fout = open(output_root + '.4' + '.bin', 'wb')
    (n_nodes * np.ones(1, dtype=np.int32)).tofile(fout)
    velocity.tofile(fout)
    fout.close()

    for i in xrange(n_nodes):
        velocity[i, 1] = 0.

    fout = open(output_root + '.5' + '.bin', 'wb')
    (n_nodes * np.ones(1, dtype=np.int32)).tofile(fout)
    velocity.tofile(fout)
    fout.close()


if __name__ == "__main__":

#     n_x = 10
#     n_y = 10
#
#     root_dir = '/Users/kirkhansen/Desktop/TestReaction/2D/'
#
#     unit_square_mesh(n_x, n_y, root_dir + 'square')

#     mesh_root_dir = '/Users/kirkhansen/Data/Hansport_Validation/01_Geometries/02_Square/'
#     output_root_dir = '/Users/kirkhansen/Data/Hansport_Validation/02_Results/02_Square/'
#     unit_square_mesh(2, 2, mesh_root_dir + 'square')
#
#     mesh_root_dir = '/Users/kirkhansen/Data/Hansport_Validation/01_Geometries/01_Line/'
#     output_root_dir = '/Users/kirkhansen/Data/Hansport_Validation/01_Geometries/01_Line/'
#     unit_line_mesh(8, mesh_root_dir + 'line')

#     mesh_root_dir = '/Users/kirkhansen/Data/Hansport_Validation/01_Geometries/03_Cylinder/02_Medium/'
#     output_root_dir = '/Users/kirkhansen/Data/Hansport_Validation/02_Results/03_Cylinder/02_Medium/'
#     # make_test_velocity_field(mesh_root_dir + 'cylinder.vtu',
#     #                          mesh_root_dir + 'velocity',
#     #                          make_adjacency=False,
#     #                          offset=0)
#
#     for i in xrange(1, 201):
#         bin_to_vtk_parallel(mesh_root_dir + 'cylinder.vtu',
#                             output_root_dir + 'advdiff_test_outletBC.0.' + str(i) + '.bin',
#                             output_root_dir + 'mapping.bin',
#                             4,
#                             output_root_dir + 'advdiff_test_outletBC.0.' + str(i) + '.vtu')


    # bin_to_vtk(mesh_root_dir + 'cylinder.vtu',
    #            output_root_dir + 'soln_order2.bin',
    #            output_root_dir + 'soln_order2.vtu')


#     root_dir = \
#         '/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/Results/' \
#         + 'Pe1e6/01_Coarse/'
#     mesh_root_dir = '/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/' \
#                     + 'Cylinder/Geometries/Mesh1/'
#     output_root_dir = '/Users/kirkhansen/Desktop/TestICFile/'
#     order = 'order2'
#     stab = '_stab'
#
#     # local_to_serial_bin_parallel(root_dir + order + stab + '.bin',
#     #                              root_dir + 'mapping_' + order + stab + '.bin',
#     #                              48,
#     #                              output_root_dir + 'ic_o2.bin')
#     bin_to_vtk_parallel(mesh_root_dir + 'cylinder.vtu',
#                         output_root_dir + 'sol4.bin',
#                         output_root_dir + 'mapping_o1.bin',
#                         4,
#                         output_root_dir + 'sol4.vtu')









    meshes = ['Mesh1/', 'Mesh2/', 'Mesh3/', 'Mesh4/', 'Mesh5/', 'Mesh6/', 'Mesh7/', 'Mesh8/', 'Mesh9/']
    dir1 = ['Pe1e6/', 'Pe1e8/', 'Pe1e10/']
    dir2 = ['00_VeryCoarse/', '01_Coarse/', '02_Medium/', '03_Fine/', '04_VeryFine/', '05_UltraFine/']

    stab = ['_stab.bin', '_nostab.bin']

    for outer in xrange(1, 2):
        for inner in xrange(4, 5):

            mesh_root_dir = \
                    '/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/Geometries/' + meshes[inner]
            output_root_dir = \
                    '/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/Results/AdjustedPeclet/Dover20/BigTstep/' + dir1[outer] + meshes[inner]
            for order in ['order1']:
                for stab in ['_stab']:
                    bin_to_vtk_parallel(mesh_root_dir + 'cylinder.vtu',
                                        output_root_dir + order + stab + '.bin',
                                        output_root_dir + 'mapping_' + order + '_stab' + '.bin',
                                        20,
                                        output_root_dir + order + stab + '.vtu')






#     for outer in xrange(0, 2):
#         for inner in xrange(0, 5):
#             mesh_root_dir = \
#                     '/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/Geometries/' + meshes[2*outer + inner]
#             output_root_dir = \
#                     '/Users/kirkhansen/Desktop/Mesh_Refinement_Tests/Cylinder/Results/AdjustedPeclet/Pe1e6/' + dir2[inner]
#             # for order in ['order2']:
#             for order in ['order1']:#, 'order2']:
#                 # for stab in ['_nostab', '_stab']:
#                 for stab in ['_stab']:#['_nostab', '_stab']:
#                     bin_to_vtk_parallel(mesh_root_dir + 'cylinder.vtp',
#                                         output_root_dir + order + '_new_tau' + stab + '.bin',
#                                         output_root_dir + 'mapping_' + order + stab + '.bin',
#                                         4,
#                                         output_root_dir + order + '_new_tau' + stab + '.vtp')


#     mesh_root_dir = '/Users/kirkhansen/Desktop/TestAdvDiffReactSolver/Geometry/'
#     output_root_dir = '/Users/kirkhansen/Desktop/TestAdvDiffReactSolver/Results/'
#     for i in xrange(5000, 5001, 20):
#         bin_to_vtk_parallel(mesh_root_dir + 'cylinder.vtu',
#                             output_root_dir + 'concentration.IIa.' + str(i) + '.bin',
#                             output_root_dir + 'mapping.bin',
#                             4,
#                             output_root_dir + 'result.' + str(i) + '.vtu')




        # vtk_to_connectivity_and_coordinates(root_dir + 'cylinder.vtu',
        #                                     root_dir + 'cylinder',
        #                                     dim=3);
# #     #     unit_line_mesh(8,
# #     #                    "/Users/kirkhansen/Desktop/TestHansport/line_n8")
# #     #     unit_square_mesh(4, 4,
# #     #                      "/Users/kirkhansen/Desktop/TestHansport/square_n4")
        # vtp_to_boundary_condition_bin(root_dir + 'inlet.vtp',
        #                               root_dir + 'inlet.bin',
        #                               fieldname='Ids',
        #                               offset=0)
        # vtp_to_boundary_condition_bin(root_dir + 'reactive.vtp',
        #                               root_dir + 'reactive.bin',
        #                               fieldname='Ids',
        #                               offset=0)
